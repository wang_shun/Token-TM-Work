//
//  NSString+URLEncoding.m
//  yuqing
//
//  Created by wangshun on 15/1/15.
//  Copyright (c) 2015年 wangshun. All rights reserved.
//

#import "NSString+URLEncoding.h"

@implementation NSString(URLEncoding)

- (NSString *)urlEncodingString
{
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                (CFStringRef)self, 
                NULL, 
                (CFStringRef)@";/?:@&=$+{}<>,",
                kCFStringEncodingUTF8));
    return result;
}

@end
