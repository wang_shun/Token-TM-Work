//
//  NSDate+TimeAgo.h
//  LuoJiFM-IOS
//
//  Created by sipengyu on 15/7/28.
//  Copyright (c) 2015年 luojilab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "DDGlobalDateFormatter.h"

@interface NSDate (TimeAgo)
- (NSString *) timeAgoSimple;
- (NSString *) timeAgo;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormat:(NSDateFormatterStyle)dFormatter andTimeFormat:(NSDateFormatterStyle)tFormatter;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormatter:(NSDateFormatter *)formatter;

+ (NSString*)secondToTimeString:(NSTimeInterval)time;
// this method only returns "{value} {unit} ago" strings and no "yesterday"/"last month" strings
- (NSString *)dateTimeAgo;

// this method gives when possible the date compared to the current calendar date: "this morning"/"yesterday"/"last week"/..
// when more precision is needed (= less than 6 hours ago) it returns the same output as dateTimeAgo
- (NSString *)dateTimeUntilNow;

+ (NSString *)stringToTimeKey:(NSString *)string;
+ (NSString *)stringToYearMonth:(NSString *)string;

- (NSString *)dateTimeAgoFeedsAfter;
- (NSString *)dateTimeAgoWithNoLine;
- (NSString *)dateTimeAgoWithOutTime;
- (NSString *)dateTimeAgoInStoryTellingMode;
- (NSString *)dateTimeAgoWithKnowledgeBookHistory;
- (NSString *)dateTimeAgoWithFreeColumnMode;
- (NSString *)dateTimeAgoWithOnlyMonthAndDay;


/**
 v3.1.0新增，获取全局的日期格式化

 @return 
 */
- (NSString *)getDDGlobalTimeAgo;

@end

