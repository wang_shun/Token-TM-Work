//
//  DDGlobalDateFormatter.h
//  LuoJiFM-IOS
//
//  Created by roc鹏 on 2017/12/20.
//  Copyright © 2017年 luojilab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDGlobalDateFormatter : NSObject

+ (DDGlobalDateFormatter *)shareInstance;

- (NSDateFormatter *)getDDFormatterMD;

- (NSDateFormatter *)getDDFormatterYMD;

- (NSDateFormatter *)getYMDDateOnlyMonthAndDayFormatter;

- (NSDateFormatter *)getYMDDateFormatter;

- (NSDateFormatter *)getYMDDateNoLineFormatter;

- (NSDateFormatter *)getHMSDateFormatter;

- (NSDateFormatter *)getFormatterOnlyDay;

- (NSDateFormatter *)getYMDDateOnlyMonthAndDayMHFormatter;

@end
