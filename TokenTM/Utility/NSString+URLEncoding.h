//
//  NSString+URLEncoding.h
//  yuqing
//
//  Created by wangshun on 15/1/15.
//  Copyright (c) 2015年 wangshun. All rights reserved.


#import <Foundation/Foundation.h>


@interface NSString (URLEncoding)

- (NSString *)urlEncodingString;

@end
