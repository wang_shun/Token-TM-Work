//
//  TMToast.m
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMToast.h"

#define loading_icon_width 37.f

#define TOASTTAG 15265

#ifndef DDFONT
#define DDFONT(x) [UIFont systemFontOfSize:x]
#endif

#ifndef RGBA
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#endif

#ifndef DDFONT_B
#define DDFONT_B(x) [UIFont boldSystemFontOfSize:x]
#endif

#ifndef ATINT
#define ATINT(x) (int)([UIScreen mainScreen].bounds.size.width/375.0*(x))
#endif

@interface TMToast ()

@property (nonatomic, retain) UILabel* textLabel;

- (void)showToast;
- (void)closeToast;


/**
 *  创建需要显示图片的样式,现在使用的场景是需要一个需要显示加载态的场景，waitingType
 *
 *  @param text 要显示的文字
 *
 *  @return TMToast对象
 */
- (id)initWaitingType:(NSString*)text;


///**
// *  封装设置text的方法，因为得处理两行的情况
// */
//- (void)setText:(NSString*)text;
@end

#pragma mark - TMNewToast

@interface TMNewToast:TMToast

@property (nonatomic, retain) UILabel* label_text;

@end


@implementation TMToast

- (id)initWaitingType:(NSString*)text
{
    CGRect frame = CGRectMake(0, 0, CGRectGetWidth([UIApplication sharedApplication].keyWindow.bounds), CGRectGetHeight([UIApplication sharedApplication].keyWindow.bounds));
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        //add _tosView
        
        _backMaskView = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-new_wait_toast_width)/2, ([UIScreen mainScreen].bounds.size.height-new_wait_toast_height)/2, new_wait_toast_width, new_wait_toast_height)];
        _backMaskView.alpha = 0;
        _backMaskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
        _backMaskView.layer.cornerRadius = 8.f;
        [self addSubview:_backMaskView];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake((new_wait_toast_width-loading_icon_width)/2, 21.f, loading_icon_width, loading_icon_width)];
        [_backMaskView addSubview:self.imageView];
        
        if(text && ![text isEqualToString:@""]) {
            _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 65.f, new_wait_toast_width, (new_wait_toast_height-65.f)/2)];
            _textLabel.textAlignment = 1;
            _textLabel.backgroundColor = [UIColor clearColor];
            _textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            _textLabel.textColor = [UIColor whiteColor];
            _textLabel.font = DDFONT(14);
            [_backMaskView addSubview:_textLabel];
        }else{
            self.imageView.frame = CGRectMake((new_wait_toast_width-loading_icon_width)/2, (new_wait_toast_height-loading_icon_width)/2, loading_icon_width, loading_icon_width);
            [_backMaskView addSubview:self.imageView];
        }
    }
    return self;
}

- (void)dealloc
{
}

#pragma mark -
#pragma mark in use functions

-(void)newShowToast{
    //qz:用于等待、加载等页面 140*98 带打转的图片36*36
    
    self.imageView.image = [UIImage imageNamed:@"loading-icon.png"];
    [NSTimer scheduledTimerWithTimeInterval:0.08 target:self selector:@selector(imageCircle) userInfo:nil repeats:YES];
    
    [UIView beginAnimations:@"show_toast" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    _backMaskView.alpha = 1;
    [UIView commitAnimations];
}

-(void)imageCircle{
    self.imageView.layer.transform = CATransform3DRotate(self.imageView.layer.transform, M_PI/10, 0.0f, 0.0f, 1.0f);
}

- (void)showToast
{
    _backMaskView.alpha = 0;
    
    [UIView beginAnimations:@"show_toast" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    _backMaskView.alpha = 1;
    [UIView commitAnimations];
}

- (void)closeToast
{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if ([self superview])
        {
            [self removeFromSuperview];
        }
    }];
}

- (void)closeToastNoAnimaed
{
    self.alpha = 0;
    [self removeFromSuperview];
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if(animationID && [animationID compare:@"close_toast"] == NSOrderedSame)
    {
        if([self superview])
        {
            [self removeFromSuperview];
        }
    }
}


#pragma mark -
#pragma mark out use functions

+ (void)showToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay
{
    if(type == ToastTypeWaiting)
    {
        TMToast *toast = [[TMToast alloc] initWaitingType:text];
        toast.tag = TOASTTAG;
        if(text && ![text isEqualToString:@""])
        {
            toast.textLabel.text = text;
        }
        
        [[UIApplication sharedApplication].keyWindow addSubview:toast];
        [toast newShowToast];
        [toast performSelector:@selector(closeToast) withObject:nil afterDelay:delay];
    }
    else
    {
        [TMNewToast showToast:type text:text closeAfterDelay:delay];
    }
}

+ (void)showMaskToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay
{
    if(type == ToastTypeWaiting)
    {
        TMToast *toast = [[TMToast alloc] initWaitingType:text];
        toast.tag = TOASTTAG;
        [toast.textLabel setText:text];
        
        [[UIApplication sharedApplication].keyWindow addSubview:toast];
        [toast newShowToast];
        [toast performSelector:@selector(closeToast) withObject:nil afterDelay:delay];
    }else{
        
        [TMNewToast showToast:type text:text closeAfterDelay:delay];
    }
}

+ (void)showToast:(ToastType)type text:(NSString*)text
{
    if(type == ToastTypeWaiting)
    {
        TMToast *toast = [[TMToast alloc] initWaitingType:text];
        toast.tag = TOASTTAG;
        [toast.textLabel setText:text];
        
        [[UIApplication sharedApplication].keyWindow addSubview:toast];
        [toast newShowToast];
    }else{
        //#warning 这里还得考虑特殊情况，不是waiting但也得阻挡的情况
        [TMNewToast showToast:type text:text closeAfterDelay:2];
    }
}

+ (void)showLayerToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay
{
    [self showToast:type text:text closeAfterDelay:delay];
}

+ (void)showLayerMaskToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay
{
    [self showMaskToast:type text:text closeAfterDelay:delay];
}

+ (void)showLayerToast:(ToastType)type text:(NSString*)text
{
    [self showToast:type text:text];
}

//关闭toast
+ (void)closeToast
{
    TMToast *toast = (TMToast*)[[UIApplication sharedApplication].keyWindow viewWithTag:TOASTTAG];
    if(toast)
    {
        [toast closeToast];
    }
}

//关闭toast无动画
+ (void)closeToastNOAnimated
{
    TMToast *toast = (TMToast*)[[UIApplication sharedApplication].keyWindow viewWithTag:TOASTTAG];
    if(toast)
    {
        [toast closeToastNoAnimaed];
    }
}

/**
 *  type1:显示一个过几秒会消失的toast,有些特殊的页面需要显示在上面，所以加了这个参数
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 *  @param showAbove 是否置顶
 */
+ (void)showToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay showAbove:(BOOL)showAbove
{
    [TMNewToast showToast:type text:text closeAfterDelay:delay showAbove:showAbove];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

#pragma mark - NEW TOAST
@implementation TMNewToast

+ (void)closeToastForce:(BOOL)force
{
    if (!force)
    {
        [super closeToast];
    }
    else
    {
        //get and close
        TMNewToast *toast = (TMNewToast*)[[UIApplication sharedApplication].keyWindow viewWithTag:TOASTTAG];
        if(toast && [toast isKindOfClass:[TMNewToast class]])
        {
            [toast closeToastForce:YES];
        }
        else
        {
            [toast closeToast];
        }
    }
}

- (void)closeToastForce:(BOOL)force
{
    if (!force)
    {
        [super closeToast];
    }
    else
    {
        if (self.superview)
        {
            [self removeFromSuperview];
        }
    }
}

- (void)dealloc
{
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _label_text = [[UILabel alloc]initWithFrame:CGRectMake(ATINT(0), 0, CGRectGetWidth(self.bounds) - ATINT(0) * 2, CGRectGetHeight(self.bounds))];
        _label_text.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _label_text.backgroundColor = [UIColor clearColor];
        _label_text.textColor = [UIColor whiteColor];
        _label_text.textAlignment = 1;
        _label_text.font = DDFONT(13);
        _label_text.lineBreakMode = NSLineBreakByTruncatingTail;
        
        self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        self.backgroundColor=[UIColor blackColor];
        self.tag = TOASTTAG;
        [self addSubview:_label_text];
    }
    return self;
}

+ (void)showToast:(ToastType)type text:(NSString *)text closeAfterDelay:(NSTimeInterval)delay
{
    [self showToast:type text:text closeAfterDelay:delay showAbove:NO];
}


/**
 *  type1:显示一个过几秒会消失的toast
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 *  @param showAbove 是否置顶
 */
+ (void)showToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay showAbove:(BOOL)showAbove
{
    //移除旧的
    [self closeToastForce:YES];
    //添加新的
    CGRect bounds = [UIApplication sharedApplication].keyWindow.bounds;
    //    如果当前最上层的有Tabbar，则不再上移xx像素
    //    AppDelegate*  app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    UINavigationController * navController = (UINavigationController *) [app navigationViewController];
    //    UIViewController* topVC = navController.topViewController;
    //
    //    UIViewController* presentedVC = topVC.presentedViewController;
    
    //    if (presentedVC)
    //    {
    //        topVC = presentedVC;
    //        if ([presentedVC isKindOfClass:[UINavigationController class]])
    //        {
    //            topVC = [(UINavigationController*)presentedVC topViewController];
    //        }
    //    }
    float height_toast = 49;
    CGRect frame = CGRectMake(0, 0, CGRectGetWidth(bounds), height_toast);
    TMNewToast* toast = [[TMNewToast alloc]initWithFrame:frame];
    toast.alpha =0;
    [toast setText:text];
    
    
    //修正toast大小
    CGSize size = [text sizeWithFont:DDFONT_B(12) constrainedToSize:CGSizeMake(toast.label_text.frame.size.width, 1000.0) lineBreakMode:NSLineBreakByWordWrapping];
    
    toast.layer.cornerRadius = 5.0;
    toast.frame=CGRectMake(frame.origin.x,frame.origin.y, size.width+ATINT(30)*2, 35);
    if (size.width>[UIApplication sharedApplication].keyWindow.frame.size.width-ATINT(30)*2-ATINT(20)*2) {
        toast.frame=CGRectMake(frame.origin.x,frame.origin.y, [UIApplication sharedApplication].keyWindow.frame.size.width-ATINT(20)*2, 50);
        
        toast.label_text.frame=CGRectMake(ATINT(30), toast.label_text.frame.origin.y,CGRectGetWidth(toast.frame)-
                                          
                                          (30)*2, toast.label_text.frame.size.height);
        
        toast.label_text.numberOfLines=2;
    }
    toast.center=CGPointMake([UIApplication sharedApplication].keyWindow.frame.size.width/2, [UIApplication sharedApplication].keyWindow.frame.size.height/2);
    toast.label_text.center =CGPointMake(toast.frame.size.width/2, toast.frame.size.height/2);
    [[UIApplication sharedApplication].keyWindow addSubview:toast];
    [UIView animateWithDuration:0.2 animations:^{
        toast.alpha = 0.7;
    } completion:^(BOOL finished) {
    }];
    [toast performSelector:@selector(closeToast) withObject:nil afterDelay:delay];
}

- (void)setText:(NSString*)text
{
    _label_text.text = text;
    //如果只有一行，numberOfLine为1
//    CGSize tsize = [_label_text.text sizeWithFont:DDFONT_B(12.) constrainedToSize:CGSizeMake(_label_text.frame.size.width, 1000)];
    CGRect rect = [_label_text.text boundingRectWithSize:CGSizeMake(_label_text.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:DDFONT_B(12.)} context:nil];
    CGSize tsize = rect.size;
    //        NSLog(@"%@", NSStringFromCGSize(tsize));
    //否则设置为两行
    if (20 < tsize.height)
    {
        _label_text.textAlignment = 0;
        _label_text.numberOfLines = 2;
    }
    else
    {
        _label_text.textAlignment = 1;
        _label_text.numberOfLines = 1;
    }
}
@end
