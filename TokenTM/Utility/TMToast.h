//
//  TMToast.h
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>


#define toast_height 43 //tosat条的高度

#define new_toast_height 50.f //只有文字的新toast高度
#define new_toast_width 270.f //只有文字的新toast宽度

#define new_wait_toast_height 98.f //带转圈的新toast高度
#define new_wait_toast_width 140.f //带转圈的新toast宽度

typedef enum {
    ToastTypeBlank,     //空白，只有文字
    ToastTypeWaiting,   //等待状态
    ToastTypeWarnning,  //叹号状态
    ToastTypeSuccess,   //对勾状态
    ToastTypeFailed     //叉子或者哭脸状态
} ToastType; //toast类型


@interface TMToast : UIView
{
    UIView *_backMaskView;//底层的遮罩
    UIImageView *_imageView;
    UILabel *_textLabel;
}
@property (nonatomic, retain) UIImageView *imageView;
//@property (nonatomic, retain) UILabel *textLabel;

#pragma mark -
#pragma mark 本类仅使用类方法即可

/**
 *  type1:显示一个过几秒会消失的toast
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 */
+ (void)showToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay;       //不卡住屏幕，手指点击穿透, 一般都使用这个

/**
 *  同type1，并没有其它功能，从5.2.0后不要使用
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 */
+ (void)showMaskToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay;

/**
 *  同type1，但是不会自动关闭
 *
 *  @param type 显示的种类，当前只有waiting是有效的
 *  @param text 文字
 */
+ (void)showToast:(ToastType)type text:(NSString*)text; //这个是卡住屏幕并等待配套使用，必须使用closeToast关闭，否则无法自动关闭

/**
 *  type1:显示一个过几秒会消失的toast,有些特殊的页面需要显示在上面，所以加了这个参数
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 *  @param showAbove 是否置顶
 */
+ (void)showToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay showAbove:(BOOL)showAbove;


/**
 *  同type1，并没有其它功能，从5.2.0后不要使用
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 */
+ (void)showLayerToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay;

/**
 *  同type1，并没有其它功能，从5.2.0后不要使用
 *
 *  @param type  显示的种类，当前只有waiting是有效的
 *  @param text  文字
 *  @param delay 关闭的时间
 */
+ (void)showLayerMaskToast:(ToastType)type text:(NSString*)text closeAfterDelay:(NSTimeInterval)delay;

/**
 *  同type1，但是不会自动关闭
 *
 *  @param type 显示的种类，当前只有waiting是有效的
 *  @param text 文字
 */
+ (void)showLayerToast:(ToastType)type text:(NSString*)text;

//关闭toast
+ (void)closeToast;

//关闭toast无动画
+ (void)closeToastNOAnimated;

@end
