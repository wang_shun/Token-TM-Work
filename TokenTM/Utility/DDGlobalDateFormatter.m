//
//  DDGlobalDateFormatter.m
//  LuoJiFM-IOS
//
//  Created by roc鹏 on 2017/12/20.
//  Copyright © 2017年 luojilab. All rights reserved.
//

#import "DDGlobalDateFormatter.h"

@interface DDGlobalDateFormatter (){
  
}

@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) NSDateFormatter *formatterNoLine;
@property (nonatomic, strong) NSDateFormatter *formatterOnlyMonthAndDay;
@property (nonatomic, strong) NSDateFormatter *formatterOnlyMonthAndDayMH;
@property (nonatomic, strong) NSDateFormatter *formatterOnlyDay;
@property (nonatomic, strong) NSDateFormatter *ddFormatterMD;
@property (nonatomic, strong) NSDateFormatter *ddFormatterYMD;
@property (nonatomic, strong) NSDateFormatter *hmsFormatter;

@end


@implementation DDGlobalDateFormatter

static DDGlobalDateFormatter *instance = nil;

/**
 *  单例，GlobalDatas
 *
 *  
 */
+(DDGlobalDateFormatter *)shareInstance{
    @synchronized(self) {
        if (instance == nil) {
            instance = [[self alloc] init];
        }
    }
    return instance;
}


#pragma mark - Global Date Formatter

- (NSDateFormatter *)getDDFormatterMD{
    return self.ddFormatterMD;
}

- (NSDateFormatter *)getDDFormatterYMD{
    return self.ddFormatterYMD;
}


- (NSDateFormatter *)ddFormatterMD{
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _ddFormatterMD = threadDictionary[@"DDFormatterMD"];
    if(!_ddFormatterMD){
        @synchronized(self){
            if(!_ddFormatterMD){
                _ddFormatterMD = [[NSDateFormatter alloc] init];
                [_ddFormatterMD setDateFormat:@"M月d日"];
                [_ddFormatterMD setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"DDFormatterMD"] = _ddFormatterMD;
            }
        }
    }
    return _ddFormatterMD;
}


- (NSDateFormatter *)ddFormatterYMD{
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _ddFormatterYMD = threadDictionary[@"DDFormatterYMD"];
    if(!_ddFormatterYMD){
        @synchronized(self){
            if(!_ddFormatterYMD){
                _ddFormatterYMD = [[NSDateFormatter alloc] init];
                [_ddFormatterYMD setDateFormat:@"yyyy年M月d日"];
                [_ddFormatterYMD setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"DDFormatterYMD"] = _ddFormatterYMD;
            }
        }
    }
    return _ddFormatterYMD;
}

/**
 *  获取年月日
 */
- (NSDateFormatter *)getYMDDateFormatter{
    
    return self.formatter;
    
}


//
- (NSDateFormatter *)formatter {
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _formatter = threadDictionary[@"FMDateFormatter"];
    if(!_formatter){
        @synchronized(self){
            if(!_formatter){
                _formatter = [[NSDateFormatter alloc] init];
                [_formatter setDateFormat:@"yyyy-MM-dd"];
                [_formatter setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"FMDateFormatter"] = _formatter;
            }
        }
    }
    return _formatter;
}

/**
 获取yyyyMMdd年月日格式

 */
- (NSDateFormatter *)getYMDDateNoLineFormatter{
    return self.formatterNoLine;
}


/**
 没有线的YMD格式化
 */
- (NSDateFormatter *)formatterNoLine{
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _formatterNoLine = threadDictionary[@"FMDateNoLineFormatter"];
    if(!_formatterNoLine){
        @synchronized(self){
            if(!_formatterNoLine){
                _formatterNoLine = [[NSDateFormatter alloc] init];
                [_formatterNoLine setDateFormat:@"yyyyMMdd"];
                [_formatterNoLine setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"FMDateNoLineFormatter"] = _formatterNoLine;
            }
        }
    }
    return _formatterNoLine;
}

- (NSDateFormatter *)getFormatterOnlyDay{
    
    return self.formatterOnlyDay;
}

/**
 没有年月的YMD格式化
 
 @return
 */
- (NSDateFormatter *)formatterOnlyDay{
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _formatterOnlyDay = threadDictionary[@"FMDateOnlyDayFormatter"];
    if(!_formatterOnlyDay){
        @synchronized(self){
            if(!_formatterOnlyDay){
                _formatterOnlyDay = [[NSDateFormatter alloc] init];
                [_formatterOnlyDay setDateFormat:@"dd日"];
                [_formatterOnlyDay setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"FMDateOnlyDayFormatter"] = _formatterOnlyDay;
            }
        }
    }
    return _formatterOnlyDay;
}


- (NSDateFormatter *)getYMDDateOnlyMonthAndDayMHFormatter{
    
    return self.formatterOnlyMonthAndDayMH;
}


/**
 没有年的YMD格式化，带冒号的
 formatterOnlyMonthAndDayMH
 @return
 */
- (NSDateFormatter *)formatterOnlyMonthAndDayMH{
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _formatterOnlyMonthAndDayMH = threadDictionary[@"FMDateOnlyMonthAndDayMHFormatter"];
    if(!_formatterOnlyMonthAndDayMH){
        @synchronized(self){
            if(!_formatterOnlyMonthAndDayMH){
                _formatterOnlyMonthAndDayMH = [[NSDateFormatter alloc] init];
                [_formatterOnlyMonthAndDayMH setDateFormat:@"M.d"];
                [_formatterOnlyMonthAndDayMH setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"FMDateOnlyMonthAndDayMHFormatter"] = _formatterOnlyMonthAndDayMH;
            }
        }
    }
    return _formatterOnlyMonthAndDayMH;
}


- (NSDateFormatter *)getYMDDateOnlyMonthAndDayFormatter{
    
    return self.formatterOnlyMonthAndDay;
}


/**
 没有年的YMD格式化
 
 @return
 */
- (NSDateFormatter *)formatterOnlyMonthAndDay{
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _formatterOnlyMonthAndDay = threadDictionary[@"FMDateOnlyMonthAndDayFormatter"];
    if(!_formatterOnlyMonthAndDay){
        @synchronized(self){
            if(!_formatterOnlyMonthAndDay){
                _formatterOnlyMonthAndDay = [[NSDateFormatter alloc] init];
                [_formatterOnlyMonthAndDay setDateFormat:@"MM-dd"];
                [_formatterOnlyMonthAndDay setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"FMDateOnlyMonthAndDayFormatter"] = _formatterOnlyMonthAndDay;
            }
        }
    }
    return _formatterOnlyMonthAndDay;
}



/**
 *  获取年月日
 */
- (NSDateFormatter *)getHMSDateFormatter{
    
    return self.hmsFormatter;
    
}


//
- (NSDateFormatter *)hmsFormatter {
    
    NSMutableDictionary *threadDictionary = [[NSThread currentThread] threadDictionary];
    _hmsFormatter = threadDictionary[@"HMSDateFormatter"];
    if(!_hmsFormatter){
        @synchronized(self){
            if(!_hmsFormatter){
                _hmsFormatter = [[NSDateFormatter alloc] init];
                [_hmsFormatter setDateFormat:@"HH:mm"];
                [_hmsFormatter setTimeZone:[NSTimeZone localTimeZone]];
                threadDictionary[@"HMSDateFormatter"] = _hmsFormatter;
            }
        }
    }
    return _hmsFormatter;
}




@end
