//
//  TMSelectedSysImage.h
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol TMSelectedSysImageDelegate;
@interface TMSelectedSysImage : NSObject<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>

@property (nonatomic,weak) id <TMSelectedSysImageDelegate> delegate;

@property(nonatomic,strong) UIImagePickerController *imagePicker;

- (instancetype)init;

- (void)openLibrary;

- (void)openCamera;



@end

@protocol TMSelectedSysImageDelegate <NSObject>

- (void)presentImgPickerViewController:(UIViewController*)vc animation:(BOOL)animation;

- (void)selectedImage:(UIImage*)img;
@end
