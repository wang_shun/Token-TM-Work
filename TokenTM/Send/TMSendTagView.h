//
//  TMSendTagView.h
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMSendTagView : UIView

@property (nonatomic,strong) UIImageView* bgImg;

@property (nonatomic,strong) UILabel* tagLabel;

- (void)setTagText:(NSString*)tag;

@end
