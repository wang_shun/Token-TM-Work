//
//  TMSendViewController.h
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMSendViewController : TMBaseViewController

- (instancetype)initWithData:(NSDictionary*)data;

@end
