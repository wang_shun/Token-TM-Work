//
//  TMSendViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMSendViewController.h"
#import "TMSendTagView.h"
#import "TMSendAddImageView.h"
#import "TMSelectedSysImage.h"

@interface TMSendViewController ()<UITextViewDelegate,TMSendAddImageViewDelegate,TMSelectedSysImageDelegate>
{
    BOOL isloading;
}
@property (nonatomic,strong) UIView* bannerView;
@property (nonatomic,strong) UIButton* sendBtn;
@property (nonatomic,strong) TMSendTagView* tagView;
@property (nonatomic,strong) UITextView* textView;
@property (nonatomic,strong) UILabel* placeholderLabel;
@property (nonatomic,strong) TMSendAddImageView* addImageView;


@property (nonatomic,strong) TMSelectedSysImage* sysImageLibrary;

@property (nonatomic,strong) NSDictionary* info;

@end

@implementation TMSendViewController

-(instancetype)initWithData:(NSDictionary*)data{
    if (self = [super init]) {
        self.info = data;
//        NSLog(@"TMSendViewController Data::%@",self.info);
        
        self.sysImageLibrary = [[TMSelectedSysImage alloc] init];
        self.sysImageLibrary.delegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = RGBACOLOR(244, 244, 244, 1);
    self.title = @"Token.TM";

    [self createView];
    [self createTextView];
    [self createAddImgView];
    
    [self createNavigationBar];
}

- (void)createView{
    self.bannerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), self.view.bounds.size.width, 40)];
    [self.view addSubview:_bannerView];
    self.bannerView.backgroundColor = [UIColor whiteColor];
    
    self.sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendBtn setFrame:CGRectMake(self.bannerView.bounds.size.width-80, 0, 80, 40)];
    [self.bannerView addSubview:self.sendBtn];
    [self.sendBtn setImage:[UIImage imageNamed:@"send_btn"] forState:UIControlStateNormal];
    [self.sendBtn setImage:[UIImage imageNamed:@"send_btn"] forState:UIControlStateHighlighted];
    
    [self.sendBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 30, 10, 10)];
    
    [self.sendBtn addTarget:self action:@selector(sendClick:) forControlEvents:UIControlEventTouchUpInside];

    self.tagView = [[TMSendTagView alloc] initWithFrame:CGRectMake(15, (40-(46/2.0))/2.0, 94/2.0, 46/2.0)];
    [self.bannerView addSubview:self.tagView];
    
    NSString* nickName = [self.info objectForKey:@"nickName"];
    [self.tagView setTagText:nickName];
}

- (void)createTextView{
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.bannerView.frame), self.view.bounds.size.width-15-15, 200)];
    
    self.textView.tintColor = RGBACOLOR(187, 160, 113, 1);
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.font = [UIFont systemFontOfSize:14];
    self.textView.delegate = self;
    
    self.placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(21, CGRectGetMaxY(self.bannerView.frame)+9, self.view.bounds.size.width-15-15, 200)];
    self.placeholderLabel.text = @"写点啊什么...";
    self.placeholderLabel.textColor = RGBACOLOR(212, 212, 212, 1);
    self.placeholderLabel.font = [UIFont systemFontOfSize:14];
    self.placeholderLabel.backgroundColor = [UIColor clearColor];
    [self.placeholderLabel sizeToFit];
    [self.view addSubview:self.placeholderLabel];
    
    [self.view addSubview:self.textView];
    
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textView::::%@",textView.text);
    if (textView.text.length>0) {
        self.placeholderLabel.hidden = YES;
    }
    else{
        self.placeholderLabel.hidden = NO;
    }
}



////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

-(void)presentImgPickerViewController:(UIViewController *)vc animation:(BOOL)animation{
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

- (void)selectedImage:(UIImage *)img{
    if (img) {
        [self.addImageView setBgImage:img];
    }
}

- (void)createAddImgView{
    self.addImageView = [[TMSendAddImageView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.textView.frame)+10, 85, 85)];
    [self.view addSubview:self.addImageView];
    self.addImageView.backgroundColor = [UIColor clearColor];
    self.addImageView.delegate = self;
}

- (void)addImageClick:(id)sender{
    if(self.addImageView.isHaveImage){
        [self.addImageView setBgImage:nil];
    }
    else{
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *Action0 = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.sysImageLibrary openLibrary];
        }];
        [alertController addAction:Action0];
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIAlertAction *Action1 = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.sysImageLibrary openCamera];
            }];
            [alertController addAction:Action1];
        }
        
        UIAlertAction *ActionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        [alertController addAction:ActionCancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)sendClick:(UIButton*)btn{
    
//    https://dj.gl/tokentm/v1/publish/post.json?openId=oT-Ok5JOvkd3-mPcX9EBAy3ZX-nY&nickname=%E7%8E%8B%E8%88%9C&sendMode=1&channelName=btc&content=%E5%93%88%E5%93%88%E5%93%88&images=%5B%5D&pushUgc=false
//
    if (isloading == YES) {
        return;
    }
    NSString* nickname =  [self.info objectForKey:@"nickname"];
    NSString* channelName= [self.info objectForKey:@"channelName"];
    NSString* content = [self.textView.text urlEncodingString];
    NSString* url = [NSString stringWithFormat:@"%@/publish/post.json?openId=%@&&sendMode=1&nickname=%@&channelName=%@&content=%@&pushUgc=false",TMDomain,[TMUserInfo getOpenID],[nickname urlEncodingString],[channelName urlEncodingString],content];
    
    isloading = YES;
    [TMNetService POST:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.integerValue == 200 ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [TMToast showToast:ToastTypeBlank text:@"发布成功"];
                });
            }
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        isloading = NO;
    }];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.textView resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.textView resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
