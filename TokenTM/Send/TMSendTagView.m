//
//  TMSendTagView.m
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMSendTagView.h"

@implementation TMSendTagView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.bgImg = [[UIImageView alloc] initWithFrame:self.bounds];
    [self.bgImg setImage:[UIImage imageNamed:@"send_tag"]];
    [self addSubview:self.bgImg];
    
    self.tagLabel = [[UILabel alloc] initWithFrame:self.bounds];
    [self.tagLabel setFont:[UIFont systemFontOfSize:14]];
    self.tagLabel.textColor = RGBACOLOR(187, 160, 113, 1);
    [self addSubview:self.tagLabel];
    [self.tagLabel setText:@""];
    self.tagLabel.textAlignment = NSTextAlignmentCenter;
    
}

-(void)setTagText:(NSString *)tag{
    if (tag) {
        [self.tagLabel setText:tag];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
