//
//  TMSendAddImageView.h
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMSendAddImageViewDelegate;
@interface TMSendAddImageView : UIView

@property (nonatomic,weak) id <TMSendAddImageViewDelegate> delegate;

@property (nonatomic,strong) UIView* horizontal_line;
@property (nonatomic,strong) UIView* vertical_line;

@property (nonatomic,strong) UIImageView* selectedImageView;
@property (nonatomic,assign) BOOL isHaveImage;

- (void)setBgImage:(UIImage*)image;

@end


@protocol TMSendAddImageViewDelegate <NSObject>

- (void)addImageClick:(id)sender;

@end
