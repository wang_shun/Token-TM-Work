//
//  TMSendAddImageView.m
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMSendAddImageView.h"

@implementation TMSendAddImageView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createLine];
        [self createLayer];
        [self createImage];
    }
    return self;
}

- (void)createLine{
    self.vertical_line = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.width-3)/2.0, (self.bounds.size.height-34)/2.0, 3, 34)];
    [self addSubview:self.vertical_line];
    self.horizontal_line = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.height-34)/2.0, (self.bounds.size.width-3)/2.0, 34, 3)];
    [self addSubview:self.horizontal_line];
    
    [self.vertical_line setBackgroundColor:RGBACOLOR(204, 204, 204, 1)];
    [self.horizontal_line setBackgroundColor:RGBACOLOR(204, 204, 204, 1)];
    
    self.vertical_line.layer.cornerRadius = 1;
    self.vertical_line.layer.masksToBounds = YES;
    
    self.horizontal_line.layer.cornerRadius = 1;
    self.horizontal_line.layer.masksToBounds = YES;
}

- (void)createLayer{
    
    CAShapeLayer *border = [CAShapeLayer layer];
    
    //虚线的颜色
    border.strokeColor = RGBACOLOR(212, 212, 212, 1).CGColor;
    //填充的颜色
    border.fillColor = [UIColor clearColor].CGColor;
    
    //设置路径
    border.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    
    border.frame = self.bounds;
    //虚线的宽度
    border.lineWidth = 1.f;
    
    //设置线条的样式
    //border.lineCap = @"square";
    //虚线的间隔
    border.lineDashPattern = @[@4, @2];
    
    [self.layer addSublayer:border];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addImageClick:)]) {
        [self.delegate addImageClick:self];
    }
}


- (void)createImage{
    self.selectedImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.selectedImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.selectedImageView];
    self.selectedImageView.hidden = YES;
    self.isHaveImage = !self.selectedImageView.hidden;
}

-(void)setBgImage:(UIImage *)image{
    if (image) {
        self.selectedImageView.hidden = NO;
        self.selectedImageView.image = image;
        self.isHaveImage = YES;
    }
    else{
        self.selectedImageView.image = nil;
        self.selectedImageView.hidden = YES;
        self.isHaveImage = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
