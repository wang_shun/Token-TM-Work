//
//  TMSelectedSysImage.m
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMSelectedSysImage.h"

@implementation TMSelectedSysImage

- (instancetype)init{
    if (self = [super init]) {
        self.imagePicker = [[UIImagePickerController alloc] init];
    }
    return self;
}


#pragma mark -头像UIImageview的点击事件-

-(void)openCamera{
    self.imagePicker.delegate = self; //设置代理
    self.imagePicker.allowsEditing = YES;
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera; //图片来源
    
    [self.delegate presentImgPickerViewController:self.imagePicker animation:YES];
}

-(void)openLibrary{
    self.imagePicker.delegate = self; //设置代理
    self.imagePicker.allowsEditing = YES;
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; //图片来源
    
    [self.delegate presentImgPickerViewController:self.imagePicker animation:YES];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo{
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage]; //通过key值获取到图片
    
    if(image){
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectedImage:)]) {
            [self.delegate selectedImage:image];
        }
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{}];
}


@end
