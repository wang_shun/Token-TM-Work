//
//  AppDelegate.h
//  TokenTM
//
//  Created by wang shun on 2018/5/9.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (id)weixinObj;

@end

