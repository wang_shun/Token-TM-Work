//
//  TMOnlyTextCell.m
//  TokenTM
//
//  Created by qz on 2018/5/11.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMTitleImageCell.h"
#import "UIImageView+WebCache.h"
#import "TMCellButtonsView.h"

@interface TMTitleImageCell()
@property (nonatomic,strong)UIView *whiteBgView;
@property (nonatomic,strong)UIImageView *headerImageView;
@property (nonatomic,strong)UIImageView *rightImageView;

@property (nonatomic,strong)UILabel *recLbl;
@property (nonatomic,strong)UILabel *dateLbl;
@property (nonatomic,strong)UILabel *titleLbl;

@property (nonatomic,strong)UILabel *transLbl;

@property (nonatomic,strong)UILabel *translationLbl;

@property (nonatomic,strong)TMCellButtonsView *buttonView;
@end

@implementation TMTitleImageCell

-(void)initViews{
    [super initViews];
    self.cellStyle = TMCellStyleTitleAndRightImage;
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.headerImageView];
    
    [self.whiteBgView addSubview:self.recLbl];
    [self.whiteBgView addSubview:self.titleLbl];
    [self.whiteBgView addSubview:self.dateLbl];
    
    [self.whiteBgView addSubview:self.transLbl];
    [self.whiteBgView addSubview:self.translationLbl];
    [self.whiteBgView addSubview:self.rightImageView];
    
    [self.whiteBgView addSubview:self.buttonView];
}

-(void)reloadDataWithModel:(TMListModel *)dataModel{
    self.dataModel = dataModel;
    self.cellHeight = dataModel.cellHeight;

    NSString *nick = @"";
    if (dataModel.userInfo) {
        nick = dataModel.userInfo[@"nickName"];
    }
    _recLbl.text = [NSString stringWithFormat:@"%@ 推荐",nick];
    
    _titleLbl.text = dataModel.title;
    
    _translationLbl.text = dataModel.zhTitle;
    
    NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
    [dateF setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateF setTimeZone:[NSTimeZone localTimeZone]];
    NSString *dateS = [dateF stringFromDate: [NSDate date]];
    _dateLbl.text = dateS;
    
    NSString *imageString = dataModel.shareImage;
    if (dataModel.pics && dataModel.pics.count) {
        imageString = dataModel.pics[0];
    }
    [_rightImageView sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"place_holder_72"] completed:nil];
    
    NSString *avatarUrl = @"";
    if (dataModel.userInfo) {
        avatarUrl = dataModel.userInfo[@"avatarUrl"];
    }
    [_headerImageView sd_setImageWithURL:[NSURL URLWithString:avatarUrl] placeholderImage:[UIImage imageNamed:@"place_holder_72"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    if (dataModel.zhTitle && dataModel.zhTitle.length) {
        _transLbl.hidden = NO;
        _translationLbl.hidden = NO;
    }else{
        _transLbl.hidden = YES;
        _translationLbl.hidden = YES;
    }
    
    _buttonView.textArray = @[dataModel.useful,dataModel.useless,dataModel.commentNum,dataModel.shareNum,dataModel.newsId];
    
    [self refreshUIFrame];
}

-(void)refreshUIFrame{
    _whiteBgView.frame = CGRectMake(0, 0, Screen_Width, self.cellHeight-5);
    _headerImageView.frame = CGRectMake([TMCellManager shareInstance].cellPadding, 13, 36, 36);
    
    _rightImageView.frame = CGRectMake(Screen_Width-125-[TMCellManager shareInstance].cellPadding, 65, 125, 80);
    
    [_recLbl sizeToFit];
    _recLbl.frame = CGRectMake(CGRectGetMaxX(_headerImageView.frame)+12, CGRectGetMinY(_headerImageView.frame)+1, CGRectGetWidth(_recLbl.frame), CGRectGetHeight(_recLbl.frame));
    
    [_titleLbl sizeToFit];
    _titleLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMinY(_rightImageView.frame), CGRectGetWidth(_titleLbl.frame), CGRectGetHeight(_titleLbl.frame)>CGRectGetHeight(_rightImageView.frame)?CGRectGetHeight(_rightImageView.frame):CGRectGetHeight(_titleLbl.frame));
    
    [_dateLbl sizeToFit];
    _dateLbl.frame = CGRectMake(CGRectGetMinX(_recLbl.frame), CGRectGetMaxY(_headerImageView.frame)-CGRectGetHeight(_dateLbl.frame), CGRectGetWidth(_dateLbl.frame), CGRectGetHeight(_dateLbl.frame));

    if (!_transLbl.hidden) {
        _transLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_rightImageView.frame)+15, 50, 15);
       _translationLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_transLbl.frame)+15, Screen_Width-[TMCellManager shareInstance].cellPadding*2, self.dataModel.translationHeight);
    }
    
    _buttonView.frame = CGRectMake(0, CGRectGetHeight(_whiteBgView.frame)-40, CGRectGetWidth(_whiteBgView.frame), 40);
}

-(UIView *)whiteBgView{
    if (_whiteBgView == nil) {
        _whiteBgView = [[UIView alloc] init];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.frame = CGRectMake(0, 0, Screen_Width, self.cellHeight-5);
        _whiteBgView.userInteractionEnabled = YES;
    }
    return _whiteBgView;
}

-(UIImageView *)headerImageView{
    if (_headerImageView == nil) {
        _headerImageView = [[UIImageView alloc] init];
        _headerImageView.frame = CGRectMake(10, 10, 36, 36);
        _headerImageView.backgroundColor = [UIColor redColor];
        _headerImageView.layer.cornerRadius = 18;
        _headerImageView.layer.masksToBounds = YES;
    }
    return _headerImageView;
}

-(UIImageView *)rightImageView{
    if (_rightImageView == nil) {
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.frame = CGRectMake(10, 10, 125, 80);
    }
    return _rightImageView;
}

-(UILabel *)recLbl{
    if (!_recLbl) {
        _recLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, self.cellHeight-5)];
        _recLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _recLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _recLbl;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-[TMCellManager shareInstance].cellPadding*2-125-10, 50)];
        _titleLbl.textColor = [TMCellManager shareInstance].cellTitleColor;
        _titleLbl.font = [TMCellManager shareInstance].cellTitleFont;
        _titleLbl.numberOfLines = 0;
    }
    return _titleLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _dateLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _dateLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _dateLbl;
}

-(UILabel *)translationLbl{
    if (!_translationLbl) {
        _translationLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _translationLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _translationLbl.font = [TMCellManager shareInstance].cellTextFont;
        _translationLbl.numberOfLines = 0;
    }
    return _translationLbl;
}

-(UILabel *)transLbl{
    if (!_transLbl) {
        _transLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 15)];
        _transLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _transLbl.text = @"翻译";
        _transLbl.font = [TMCellManager shareInstance].cellTextFont;
    }
    return _transLbl;
}

-(TMCellButtonsView *)buttonView{
    if (_buttonView == nil) {
        _buttonView = [[TMCellButtonsView alloc] init];
        _buttonView.frame = CGRectMake(0, 10, 36, 36);
    }
    return _buttonView;
}


@end
