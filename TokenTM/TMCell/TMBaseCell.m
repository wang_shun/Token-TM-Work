//
//  TMBaseCell.m
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBaseCell.h"

@interface TMBaseCell()

@end

@implementation TMBaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

-(void)initViews{
    self.contentView.backgroundColor = [TMCellManager shareInstance].cellBgColor;
}

-(void)reloadDataWithModel:(TMListModel *)dataModel{
    
}
@end
