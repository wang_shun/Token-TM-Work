//
//  TMTitleCell.m
//  TokenTM
//
//  Created by qz on 2018/5/11.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMTitleTextCell.h"
#import "UIImageView+WebCache.h"
#import "TMCellButtonsView.h"

@interface TMTitleTextCell()

@property (nonatomic,strong)UIView *whiteBgView;
@property (nonatomic,strong)UIImageView *headerImageView;

@property (nonatomic,strong)UILabel *recLbl;
@property (nonatomic,strong)UILabel *dateLbl;
@property (nonatomic,strong)UILabel *titleLbl;


@property (nonatomic,strong)UILabel *textLbl;
@property (nonatomic,strong)UIButton *fulltextBtn;

@property (nonatomic,strong)TMCellButtonsView *buttonView;
@end

@implementation TMTitleTextCell

-(void)initViews{
    [super initViews];
    self.cellStyle = TMCellStyleTitleAndText;
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.headerImageView];
    
    [self.whiteBgView addSubview:self.recLbl];
    [self.whiteBgView addSubview:self.titleLbl];
    [self.whiteBgView addSubview:self.textLbl];
    [self.whiteBgView addSubview:self.dateLbl];
    [self.whiteBgView addSubview:self.fulltextBtn];
    [self.whiteBgView addSubview:self.buttonView];
}

-(void)reloadDataWithModel:(TMListModel *)dataModel{
    self.dataModel = dataModel;
    if (dataModel.is_fullText) {
        self.cellHeight = dataModel.fullTextHeight;
    }else{
        self.cellHeight = dataModel.cellHeight;
    }
    
    NSString *nick = @"";
    if (dataModel.userInfo) {
        nick = dataModel.userInfo[@"nickName"];
    }
    _recLbl.text = [NSString stringWithFormat:@"%@ 推荐",nick];
    _titleLbl.text = dataModel.title;
    _textLbl.text = dataModel.content;
    
    NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
    [dateF setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateF setTimeZone:[NSTimeZone localTimeZone]];
    NSString *dateS = [dateF stringFromDate: [NSDate date]];
    _dateLbl.text = dateS;
    
    NSString *avatarUrl = @"";
    if (dataModel.userInfo) {
        avatarUrl = dataModel.userInfo[@"avatarUrl"];
    }
    [_headerImageView sd_setImageWithURL:[NSURL URLWithString:avatarUrl] placeholderImage:[UIImage imageNamed:@"place_holder_72"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    if (dataModel.can_fullText) {
        _fulltextBtn.hidden = NO;
    }else{
        _fulltextBtn.hidden = YES;
    }
    
    _buttonView.textArray = @[dataModel.useful,dataModel.useless,dataModel.commentNum,dataModel.shareNum,dataModel.newsId];
    
    [self refreshUIFrame];
}

-(void)refreshUIFrame{
    if (self.dataModel.is_fullText) {
        _whiteBgView.frame = CGRectMake(0, 0, Screen_Width, self.dataModel.cellFullTextHeight-5);
    }else{
        _whiteBgView.frame = CGRectMake(0, 0, Screen_Width, self.cellHeight-5);
    }
    
    _headerImageView.frame = CGRectMake([TMCellManager shareInstance].cellPadding, 13, 36, 36);
    
    [_recLbl sizeToFit];
    _recLbl.frame = CGRectMake(CGRectGetMaxX(_headerImageView.frame)+[TMCellManager shareInstance].cellPadding, CGRectGetMinY(_headerImageView.frame)+1, CGRectGetWidth(_recLbl.frame), CGRectGetHeight(_recLbl.frame));
    
    _titleLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_headerImageView.frame)+15, CGRectGetWidth(_titleLbl.frame), self.dataModel.titleHeight);
    
    if (self.dataModel.is_fullText) {
        _textLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_titleLbl.frame)+15, CGRectGetWidth(_titleLbl.frame), self.dataModel.fullTextHeight);
    }else{
        _textLbl.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_titleLbl.frame)+15, CGRectGetWidth(_titleLbl.frame), ceil([TMCellManager shareInstance].four_line_height));
    }

    
    [_dateLbl sizeToFit];
    _dateLbl.frame = CGRectMake(CGRectGetMinX(_recLbl.frame), CGRectGetMaxY(_headerImageView.frame)-CGRectGetHeight(_dateLbl.frame), CGRectGetWidth(_dateLbl.frame), CGRectGetHeight(_dateLbl.frame));

    if (!_fulltextBtn.hidden) {
        _fulltextBtn.frame = CGRectMake(CGRectGetMinX(_headerImageView.frame), CGRectGetMaxY(_textLbl.frame)+15, 50, 15);
    }
    
    _buttonView.frame = CGRectMake(0, CGRectGetHeight(_whiteBgView.frame)-40, CGRectGetWidth(_whiteBgView.frame), 40);
}


-(void)fullTextClick{
    if (self.delegate && [self.delegate respondsToSelector:@selector(TMCellClickFullTextOrHold:)]) {
        if (self.dataModel.is_fullText) {
            //收起
            self.dataModel.is_fullText = NO;
        }else{
            //展开
            self.dataModel.is_fullText = YES;
        }
        [self.delegate TMCellClickFullTextOrHold:self.dataModel];
    }
}


-(UIView *)whiteBgView{
    if (_whiteBgView == nil) {
        _whiteBgView = [[UIView alloc] init];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.frame = CGRectMake(0, 0, Screen_Width, self.cellHeight-5);
        _whiteBgView.userInteractionEnabled = YES;
    }
    return _whiteBgView;
}

-(UIImageView *)headerImageView{
    if (_headerImageView == nil) {
        _headerImageView = [[UIImageView alloc] init];
        _headerImageView.frame = CGRectMake(10, 10, 36, 36);
        _headerImageView.backgroundColor = [UIColor redColor];
        _headerImageView.layer.cornerRadius = 18;
        _headerImageView.layer.masksToBounds = YES;
    }
    return _headerImageView;
}

-(UILabel *)recLbl{
    if (!_recLbl) {
        _recLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, self.cellHeight-5)];
        _recLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _recLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _recLbl;
}

-(UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-[TMCellManager shareInstance].cellPadding*2, 50)];
        _titleLbl.textColor = [TMCellManager shareInstance].cellTitleColor;
        _titleLbl.font = [TMCellManager shareInstance].cellTitleFont;
        _titleLbl.numberOfLines = 0;
    }
    return _titleLbl;
}

-(UILabel *)dateLbl{
    if (!_dateLbl) {
        _dateLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _dateLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _dateLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _dateLbl;
}

-(UILabel *)textLbl{
    if (!_textLbl) {
        _textLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _textLbl.textColor = [TMCellManager shareInstance].cellDateColor;
        _textLbl.font = [TMCellManager shareInstance].cellTextFont;
        _textLbl.numberOfLines = 0;
    }
    return _textLbl;
}

-(TMCellButtonsView *)buttonView{
    if (_buttonView == nil) {
        _buttonView = [[TMCellButtonsView alloc] init];
        _buttonView.frame = CGRectMake(0, 10, 36, 36);
    }
    return _buttonView;
}

-(UIButton *)fulltextBtn{
    if (_fulltextBtn == nil) {
        _fulltextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fulltextBtn setTitle:@"查看全文" forState:0];
        [_fulltextBtn setTitle:@"查看全文" forState:UIControlStateSelected];
        
        [_fulltextBtn setTitleColor:[TMCellManager shareInstance].cellDateColor forState:0];
        [_fulltextBtn addTarget:self action:@selector(fullTextClick) forControlEvents:UIControlEventTouchUpInside];
        _fulltextBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //_fulltextBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
        _fulltextBtn.titleLabel.font = [TMCellManager shareInstance].cellTextFont;
        _fulltextBtn.frame = CGRectMake(0, 0, 50, 15);
    }
    return _fulltextBtn;
}


@end
