//
//  TMCellManager.m
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCellManager.h"

static TMCellManager *instance = nil;

@implementation TMCellManager

+ (TMCellManager *)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        self.cellDateColor = GRAY(0x66);
        self.cellTitleColor = GRAY(0x33);
        self.cellButtonColor = GRAY(0x99);
        self.cellBgColor = GRAY(0xf4);
        
        self.cellDateFont = [UIFont systemFontOfSize:12];
        self.cellTitleFont = [UIFont systemFontOfSize:17];
        self.cellButtonFont = [UIFont systemFontOfSize:14];

        self.cellTextFont = [UIFont systemFontOfSize:12];
        
        self.four_line_height = 4*([@"三三" boundingRectWithSize:CGSizeMake(Screen_Width-14*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.height);
        
        self.cellPadding = 14.f;
        self.height_before_title = 13+36+15;
    }
    return self;
}

- (void)configHeightForCellWithData:(TMListModel *)model{
    if(model.templateType.integerValue == TMCellStyleTitleAndText){
        CGFloat textWidth = [[TMCellManager shareInstance] textWidthForTemplateType:TMCellStyleTitleAndText];
        
        CGFloat titleHeight = ceil([model.title boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTitleFont} context:nil].size.height);
        model.titleHeight = titleHeight;
        
        CGFloat textHeight = [model.content boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTextFont} context:nil].size.height;
        
        model.fullTextHeight = ceil(textHeight);
        
        if (textHeight > [TMCellManager shareInstance].four_line_height) {
            model.can_fullText = YES;
        }
        
        if (model.can_fullText) {
            //可以展开全文
            model.cellHeight = [TMCellManager shareInstance].height_before_title+titleHeight+15+ceil([TMCellManager shareInstance].four_line_height)+15+15+15+40+5;
            model.cellFullTextHeight = [TMCellManager shareInstance].height_before_title+titleHeight+15+ceil(textHeight)+15+15+15+40+5;
        }else{
            model.cellHeight = [TMCellManager shareInstance].height_before_title+titleHeight+15+ceil(textHeight)+15+40+5;
            model.cellFullTextHeight = 0;
        }
    }else if(model.templateType.integerValue == TMCellStyleTitleAndRightImage){
        if (model.zhTitle && model.zhTitle.length) {
            //有翻译内容
            CGFloat translation_width = Screen_Width-[TMCellManager shareInstance].cellPadding*2;
            CGFloat translation_height = ceil([model.zhTitle boundingRectWithSize:CGSizeMake(translation_width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTextFont} context:nil].size.height);
            model.translationHeight = translation_height;
            model.cellHeight = [TMCellManager shareInstance].height_before_title+80+15+15+15+translation_height+15+40+5;
        }else{
            model.cellHeight = 205.f;
        }
    }else if(model.templateType.integerValue == TMCellStyleContentAndDownImage){
        CGFloat total_height = [TMCellManager shareInstance].height_before_title+40+5;
        
        CGFloat translation_width = Screen_Width-[TMCellManager shareInstance].cellPadding*2;
        CGFloat textHeight = ceil([model.content boundingRectWithSize:CGSizeMake(translation_width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTextFont} context:nil].size.height);
        
        model.fullTextHeight = textHeight;
        total_height = total_height + textHeight + 15;
        
        if (model.translation && model.translation.length) {
            //有翻译内容
            CGFloat translation_height = ceil([model.translation boundingRectWithSize:CGSizeMake(translation_width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTextFont} context:nil].size.height);

            model.translationHeight = translation_height;
            total_height = total_height +15 + 15 + translation_height + 15;
        }
        
        if (model.pics && model.pics.count) {
            if (model.pics.count == 1) {
                CGFloat pic_width = translation_width;
                CGFloat pic_height = ceil(pic_width/2.2);
                model.picHeight = pic_height;
                total_height = total_height + pic_height + 15;
            }else if (model.pics.count == 2) {
                CGFloat pic_width = (translation_width-12)/2;
                total_height = total_height + pic_width + 15;
                model.picHeight = pic_width;
            }else if (model.pics.count == 3) {
                CGFloat pic_width = (translation_width-24)/3;
                total_height = total_height + pic_width + 15;
                model.picHeight = pic_width;
            }
        }
        model.cellHeight = total_height;
    }
}

- (CGFloat)heightForCellWithData:(TMListModel *)model{
    if(model.templateType.integerValue == TMCellStyleTitleAndText){
        CGFloat textWidth = [[TMCellManager shareInstance] textWidthForTemplateType:TMCellStyleTitleAndText];
        
        CGFloat titleHeight = ceil([model.title boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTitleFont} context:nil].size.height);
        model.titleHeight = titleHeight;
        
        CGFloat textHeight = [model.content boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [TMCellManager shareInstance].cellTextFont} context:nil].size.height;
        
        model.fullTextHeight = ceil(textHeight);
        
        if (textHeight > [TMCellManager shareInstance].four_line_height) {
            model.can_fullText = YES;
        }
        return titleHeight+15+textHeight+15;
    }else if(model.templateType.integerValue == TMCellStyleTitleAndRightImage){
        return 230.f;
    }
    return 0;
}

- (CGFloat)textWidthForTemplateType:(TMCellStyle)cellStyle{
    switch (cellStyle) {
        case TMCellStyleTitleAndRightImage:
            return Screen_Width-[TMCellManager shareInstance].cellPadding*2;
            break;
        case TMCellStyleContentAndDownImage:
            return Screen_Width-[TMCellManager shareInstance].cellPadding*2-125-10;
            break;
        case TMCellStyleTitleAndText:
            return Screen_Width-[TMCellManager shareInstance].cellPadding*2;
            break;
        default:
            break;
    }
}
@end
