//
//  TMCellManager.h
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

typedef NS_ENUM(NSInteger, TMCellStyle) {
    TMCellStyleTitleAndRightImage = 1,       //左标题 右图    zhTitle表示翻译字段
    TMCellStyleContentAndDownImage = 2,      //正文+翻译+图片 translation表示翻译字段 翻译部分下面是图片
    TMCellStyleTitleAndText = 3,             //标题、正文 正文展开
};

#import <Foundation/Foundation.h>
#import "TMListModel.h"

@interface TMCellManager : NSObject

@property (nonatomic,strong) UIColor *cellBgColor;

@property (nonatomic,strong) UIColor *cellDateColor;
@property (nonatomic,strong) UIFont *cellDateFont;

@property (nonatomic,strong) UIColor *cellTitleColor;
@property (nonatomic,strong) UIFont *cellTitleFont;

@property (nonatomic,strong) UIFont *cellTextFont;
@property (nonatomic,strong) UIColor *contentColor;

@property (nonatomic,strong) UIColor *cellButtonColor;
@property (nonatomic,strong) UIFont *cellButtonFont;

@property (nonatomic,assign) CGFloat cellPadding;

@property (nonatomic,assign) CGFloat four_line_height;
@property (nonatomic,assign) CGFloat height_before_title;

@property (nonatomic,strong) NSMutableDictionary *heightCacheDic;

+ (instancetype)shareInstance;

- (CGFloat)heightForCellWithData:(TMListModel *)model;

- (CGFloat)textWidthForTemplateType:(TMCellStyle)cellStyle;

- (void)configHeightForCellWithData:(TMListModel *)model;
@end
