//
//  TMCellButtonsView.m
//  TokenTM
//
//  Created by qz on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCellButtonsView.h"
#import "TMCellManager.h"
#import "TMNetService.h"

@interface TMCellButtonsView (){
    CGFloat btn_image_padding;
    BOOL isloading;
}

@property (nonatomic,strong)UIImageView *usefulImageView;
@property (nonatomic,strong)UILabel *usefulLbl;
@property (nonatomic,strong)UIButton *usefulBtn;
@property (nonatomic,strong)NSString *newsID;


@property (nonatomic,assign)NSInteger useNum;
@property (nonatomic,assign)NSInteger unuseNum;

@property (nonatomic,strong)UIImageView *unusefulImageView;
@property (nonatomic,strong)UIButton *unusefulBtn;
@property (nonatomic,strong)UILabel *unusefulLbl;

@property (nonatomic,strong)UILabel *shareNumsLbl;
@property (nonatomic,strong)UIImageView *shareImageView;
@property (nonatomic,strong)UIButton *shareBtn;

@property (nonatomic,strong)UILabel *commentNumsLbl;
@property (nonatomic,strong)UIImageView *commentImageView;
@property (nonatomic,strong)UIButton *commentBtn;

@end

@implementation TMCellButtonsView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initViews];
    }
    return self;
}

-(void)initViews{
    btn_image_padding = 8;
    
    [self addSubview:self.usefulBtn];
    [self addSubview:self.unusefulBtn];
    
    [self addSubview:self.usefulLbl];
    [self addSubview:self.unusefulLbl];
    
    [self addSubview:self.usefulImageView];
    [self addSubview:self.unusefulImageView];
    
    [self addSubview:self.shareNumsLbl];
    [self addSubview:self.shareImageView];
    [self addSubview:self.shareBtn];
    
    [self addSubview:self.commentNumsLbl];
    [self addSubview:self.commentImageView];
    [self addSubview:self.commentBtn];
}

-(void)setTextArray:(NSArray *)textArray{
    _textArray = textArray;
    if (textArray.count < 4) {
        return;
    }
    
    self.useNum = [textArray[0] integerValue];
    self.unuseNum = [textArray[1] integerValue];
    
    _usefulLbl.text = [NSString stringWithFormat:@"有用%@",textArray[0]];
    _unusefulLbl.text = [NSString stringWithFormat:@"无用%@",textArray[1]];
    _commentNumsLbl.text = textArray[2];
    _shareNumsLbl.text = textArray[3];
    
    if (textArray.count > 4) {
        self.newsID = textArray[4];
    }
    
    [self refreshUIFrame];
}

- (void)usefulBtnTap{
    https://dj.gl/tokentm/v1/like/2356194525732405248.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&newsType=0&action=3&source=1
    
    if (isloading) {
        return;
    }
    
    NSString *action = @"1";
    if (self.usefulBtn.selected == YES) {
        action = @"3";
        self.usefulBtn.selected = NO;
    }else{
        self.usefulBtn.selected = YES;
        self.unusefulBtn.selected = NO;
    }
    __weak typeof(self) weakSelf = self;
    isloading = YES;
    NSString* url = [NSString stringWithFormat:@"%@/like/%@.json?openId=%@&newsType=0&action=%@&source=1",TMDomain,self.newsID,[TMUserInfo getOpenID],action];//@"2354259557066211333"

    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                if ([action integerValue] == 3) {
                    weakSelf.useNum -= 1;
                }else{
                    weakSelf.useNum += 1;
                }
                weakSelf.usefulLbl.text = [NSString stringWithFormat:@"有用%ld",weakSelf.useNum];
            }
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        isloading = NO;
    }];
}

- (void)unusefulBtnTap{
    if (isloading) {
        return;
    }
    
    NSString *action = @"2";
    if (self.unusefulBtn.selected == YES) {
        action = @"3";
        self.unusefulBtn.selected = NO;
    }else{
        self.unusefulBtn.selected = YES;
        self.usefulBtn.selected = NO;
    }
    __weak typeof(self) weakSelf = self;
    isloading = YES;
    NSString* url = [NSString stringWithFormat:@"%@/like/%@.json?openId=%@&newsType=0&action=%@&source=1",TMDomain,self.newsID,[TMUserInfo getOpenID],action];//@"2354259557066211333"
    
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                if ([action integerValue] == 3) {
                    weakSelf.unuseNum -= 1;
                }else{
                    weakSelf.unuseNum += 1;
                }
                weakSelf.unusefulLbl.text = [NSString stringWithFormat:@"有用%ld",weakSelf.unuseNum];
            }
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        isloading = NO;
    }];
}

- (void)refreshUIFrame{
    [_usefulLbl sizeToFit];
    [_unusefulLbl sizeToFit];
    [_commentNumsLbl sizeToFit];
    [_shareNumsLbl sizeToFit];

    _usefulImageView.frame = CGRectMake([TMCellManager shareInstance].cellPadding+8, 13, 14, 14);
    _usefulLbl.frame = CGRectMake(CGRectGetMaxX(_usefulImageView.frame)+4, 10, CGRectGetWidth(_usefulLbl.frame), 20);
    _usefulBtn.frame = CGRectMake([TMCellManager shareInstance].cellPadding, 10, CGRectGetMaxX(_usefulLbl.frame)+8-[TMCellManager shareInstance].cellPadding, 20);
    
    _unusefulImageView.frame = CGRectMake(8+CGRectGetMaxX(_usefulBtn.frame)+15, 13, 14, 14);
    _unusefulLbl.frame = CGRectMake(CGRectGetMaxX(_unusefulImageView.frame)+4, 10, CGRectGetWidth(_unusefulLbl.frame), 20);
    _unusefulBtn.frame = CGRectMake(CGRectGetMaxX(_usefulBtn.frame)+15, 10, CGRectGetMaxX(_unusefulLbl.frame)+8-(CGRectGetMaxX(_usefulBtn.frame)+15), 20);
    
    _shareNumsLbl.frame = CGRectMake(Screen_Width-CGRectGetWidth(_shareNumsLbl.frame)-[TMCellManager shareInstance].cellPadding, 10, CGRectGetWidth(_shareNumsLbl.frame), 20);
    _shareImageView.frame = CGRectMake(CGRectGetMinX(_shareNumsLbl.frame)-14-btn_image_padding, 13, 14, 14);
    _shareBtn.frame = CGRectMake(CGRectGetMinX(_shareImageView.frame), 13, CGRectGetWidth(_shareImageView.frame)+CGRectGetWidth(_shareNumsLbl.frame)+btn_image_padding, 20);
    
    _commentNumsLbl.frame = CGRectMake(CGRectGetMinX(_shareImageView.frame)-30-CGRectGetWidth(_commentNumsLbl.frame), 10, CGRectGetWidth(_commentNumsLbl.frame), 20);
    _commentImageView.frame = CGRectMake(CGRectGetMinX(_commentNumsLbl.frame)-14-btn_image_padding, 13, 14, 14);
    _commentBtn.frame = CGRectMake(CGRectGetMinX(_commentImageView.frame), 10, CGRectGetWidth(_commentImageView.frame)+CGRectGetWidth(_commentImageView.frame)+btn_image_padding, 20);
}

-(UILabel *)usefulLbl{
    if (!_usefulLbl) {
        _usefulLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _usefulLbl.textColor = HEXCOLOR(0xff9999);
        _usefulLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _usefulLbl;
}

-(UILabel *)unusefulLbl{
    if (!_unusefulLbl) {
        _unusefulLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _unusefulLbl.textColor = [UIColor whiteColor];
        _unusefulLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _unusefulLbl;
}

-(UIButton *)usefulBtn{
    if (!_usefulBtn) {
        _usefulBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _usefulBtn.backgroundColor = [UIColor whiteColor];
        [_usefulBtn addTarget:self action:@selector(usefulBtnTap) forControlEvents:UIControlEventTouchUpInside];
        _usefulBtn.layer.masksToBounds = YES;
        _usefulBtn.layer.cornerRadius = 10;
        _usefulBtn.layer.borderColor = HEXCOLOR(0xff9999).CGColor;
        _usefulBtn.layer.borderWidth = TMOnePixel;
    }
    return _usefulBtn;
}

-(UIButton *)unusefulBtn{
    if (!_unusefulBtn) {
        _unusefulBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_unusefulBtn addTarget:self action:@selector(unusefulBtnTap) forControlEvents:UIControlEventTouchUpInside];
        _unusefulBtn.backgroundColor = HEXCOLOR(0x99cc66);
        _unusefulBtn.layer.masksToBounds = YES;
        _unusefulBtn.layer.cornerRadius = 10;
    }
    return _unusefulBtn;
}

-(UIButton *)shareBtn{
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareBtn.backgroundColor = [UIColor clearColor];
    }
    return _shareBtn;
}

-(UIButton *)commentBtn{
    if (!_commentBtn) {
        _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _commentBtn.backgroundColor = [UIColor clearColor];
    }
    return _commentBtn;
}


-(UIImageView *)usefulImageView{
    if (_usefulImageView == nil) {
        _usefulImageView = [[UIImageView alloc] init];
        _usefulImageView.frame = CGRectMake(10, 10, 14, 14);
        _usefulImageView.image = [UIImage imageNamed:@"cell_useful"];
    }
    return _usefulImageView;
}

-(UIImageView *)unusefulImageView{
    if (_unusefulImageView == nil) {
        _unusefulImageView = [[UIImageView alloc] init];
        _unusefulImageView.frame = CGRectMake(10, 10, 14, 14);
        _unusefulImageView.image = [UIImage imageNamed:@"cell_useless"];
    }
    return _unusefulImageView;
}

-(UIImageView *)shareImageView{
    if (_shareImageView == nil) {
        _shareImageView = [[UIImageView alloc] init];
        _shareImageView.frame = CGRectMake(10, 10, 14, 14);
        _shareImageView.image = [UIImage imageNamed:@"cell_share"];
    }
    return _shareImageView;
}

-(UIImageView *)commentImageView{
    if (_commentImageView == nil) {
        _commentImageView = [[UIImageView alloc] init];
        _commentImageView.frame = CGRectMake(10, 10, 14, 14);
        _commentImageView.image = [UIImage imageNamed:@"cell_comment"];
    }
    return _commentImageView;
}

-(UILabel *)shareNumsLbl{
    if (!_shareNumsLbl) {
        _shareNumsLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 40)];
        //_shareNumsLbl.textAlignment = NSTextAlignmentRight;
        _shareNumsLbl.textColor = GRAY(0x66);
        _shareNumsLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _shareNumsLbl;
}


-(UILabel *)commentNumsLbl{
    if (!_commentNumsLbl) {
        _commentNumsLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 40)];
        _commentNumsLbl.textColor = GRAY(0x66);
        _commentNumsLbl.font = [TMCellManager shareInstance].cellDateFont;
    }
    return _commentNumsLbl;
}

@end
