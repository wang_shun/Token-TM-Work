//
//  TMBaseCell.h
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMCellManager.h"
#import "TMListModel.h"

@protocol TMListCellDelegate <NSObject>
/**
 *  点击【查看全文】
 *
 *  @param model 个人中心页uid
 *
 */
- (void)TMCellClickFullTextOrHold:(TMListModel *)model;

@end

@interface TMBaseCell : UITableViewCell

@property (nonatomic,weak) id<TMListCellDelegate>delegate;

@property (nonatomic,assign) CGFloat cellStyle;
@property (nonatomic,assign) CGFloat cellHeight;
@property (nonatomic,strong) TMListModel * dataModel;

-(void)initViews;
-(void)reloadDataWithModel:(TMListModel *)dataModel;
@end
