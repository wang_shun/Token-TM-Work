//
//  TMArticleModel.h
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMNewsDetailCellHeader.h"

@interface TMArticleModel : NSObject
{
    BOOL isloading;
}

@property (nonatomic,strong) NSDictionary* resp;

@property (nonatomic,strong) NSMutableArray* content;

@property (nonatomic,strong) NSMutableArray* readerList;//阅读人头像列表
@property (nonatomic,strong) NSNumber* readerNum;//有多少朋友阅读过

@property (nonatomic,strong) NSDictionary* userInfo;//文章顶部用户

@property (nonatomic,strong) NSNumber* readTimes;//阅读次数记录 (猜测应该是 阅读次数>阅读人)


@property (nonatomic,strong) NSString* newsId;
@property (nonatomic,strong) NSString* newsType;
@property (nonatomic,strong) NSString* templateType;

@property (nonatomic,strong) NSString* time;
@property (nonatomic,strong) NSString* media;

@property (nonatomic,assign) BOOL isCollected;

@property (nonatomic,strong) NSString* title;

- (void)getTMArticleModel:(NSDictionary*)dic;


- (void)updateCellInfo:(TMNewsDetailBaseCell*)cell;

- (NSString *)getDetailHeaderCellClassName:(NSIndexPath*)indexPath;


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section;



- (void)report:(NSDictionary*)info;
- (void)collect:(NSDictionary*)info WithCompletion:(void(^)(BOOL b))method;
//分享朋友圈 图
- (void)shareTimeLine:(NSDictionary*)info WithCompletion:(void(^)(NSData* d))method;

@end
