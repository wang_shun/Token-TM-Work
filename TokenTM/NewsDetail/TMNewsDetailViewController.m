//
//  TMNewsDetailViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/5/13.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailViewController.h"
#import "TMNewsDetailData.h"
#import "TMArticleCellModel.h"

#import "TMNewsDetailBaseCell.h"

#import "TMArticleEditView.h"
#import "TMArticleShareImageView.h"

#import "TMCommentHeaderView.h"
#import "TMCommentConetentView.h"

@interface TMNewsDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,TMCommentHeaderViewDelegate,TMCommentConetentViewDelegate,TMArticleEditViewDelegate>

@property (nonatomic,strong) TMNewsDetailData* detailData;

@property (nonatomic,strong) UITableView* table_View;

@property (nonatomic,strong) TMArticleEditView* editView;

@property (nonatomic,strong) TMArticleShareImageView* shareImageView;

@property (nonatomic,strong) TMCommentHeaderView * commentHeaderView;
@property (nonatomic,strong) TMCommentConetentView * commentConetentView;

@property (nonatomic,strong) UIView * textFieldBg;
@property (nonatomic,strong) UITextField * commentTextField;

@property (nonatomic,assign) CGFloat commentTotalHeight;
@property (nonatomic,strong) NSArray * comments;
@end

@implementation TMNewsDetailViewController

-(instancetype)initWithTMListModel:(TMListModel *)model{
    if (self = [super init]) {
        self.detailData = [[TMNewsDetailData alloc] initWithModel:model];
    }
    return self;
}

-(instancetype)initWithNewsId:(NSString *)newsId{
    if (self = [super init]) {
        TMListModel* model = [[TMListModel alloc] init];
        model.newsId = newsId;
        self.detailData = [[TMNewsDetailData alloc] initWithModel:model];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Token.TM";
    
    self.commentTotalHeight = 0;
    
    [self createTable];

    [self createEditView];

    [self createShareImageView];

    [self createNavigationBar];

    __weak TMNewsDetailViewController* weakSelf = self;
    [self.detailData getDetailDataCompletion:^{
        [self.table_View reloadData];

        if ([self.detailData.articleModel.templateType isEqualToString:@"1"]) {
            [weakSelf.editView setEnableCollect:YES];

            [weakSelf.editView setIsCollect:self.detailData.articleModel.isCollected];
        }
    }];
    
    [self.detailData getCommentDataCompletion:^(NSArray *commentList,CGFloat height){
        weakSelf.commentTotalHeight = height;
        weakSelf.comments = commentList;
        [weakSelf.table_View reloadData];
    }];
    
}

- (void)createEditView{
    self.editView = [[TMArticleEditView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-60, self.view.bounds.size.width, 60)];
    [self.view addSubview:self.editView];
    self.editView.delegate = self;
}

- (void)createShareImageView{
    self.shareImageView = [[TMArticleShareImageView alloc] initWithFrame:CGRectMake(0, TMNavigationBarHeight, self.view.bounds.size.width, self.view.bounds.size.height-TMNavigationBarHeight)];
    [self.view addSubview:self.shareImageView];
    self.shareImageView.hidden = YES;
}

#pragma mark - TMCommentHeaderViewDelegate

- (void)TMCommentHeaderViewTapToIndex:(NSInteger)index{
    [self.commentConetentView selectIndex:index];
}

#pragma mark - TMCommentConetentViewDelegate

- (void)TMCommentConetentViewScrollToIndex:(NSInteger)index{
    [self.commentHeaderView tapIndex:index];
}

#pragma mark - TMArticleEditViewDelegate

- (void)reportClick:(id)sender{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *Action0 = [UIAlertAction actionWithTitle:@"广告软文" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"广告软文",@"reason":@"0"}];
    }];
    
    UIAlertAction *Action1 = [UIAlertAction actionWithTitle:@"排版混乱" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"排版混乱",@"reason":@"1"}];
    }];
    
    UIAlertAction *Action2 = [UIAlertAction actionWithTitle:@"虚假信息" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"虚假信息",@"reason":@"2"}];
    }];
    
    UIAlertAction *Action3 = [UIAlertAction actionWithTitle:@"低俗色情" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"低俗色情",@"reason":@"3"}];
    }];
    
    UIAlertAction *Action4 = [UIAlertAction actionWithTitle:@"涉嫌违法犯罪" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"涉嫌违法犯罪",@"reason":@"4"}];
    }];
    
    UIAlertAction *Action5 = [UIAlertAction actionWithTitle:@"频道类别错误" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.detailData.articleModel report:@{@"name":@"频道类别错误",@"reason":@"5"}];
    }];
    
    UIAlertAction *Action6 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alertController addAction:Action0];
    [alertController addAction:Action1];
    [alertController addAction:Action2];
    [alertController addAction:Action3];
    [alertController addAction:Action4];
    [alertController addAction:Action5];
    [alertController addAction:Action6];
   
    [self presentViewController:alertController animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //https://dj.gl/tokentm/v1/comment/post.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&content=&type=1&newsId=2356008983145218048&newsType=0&action=1
    
    __weak typeof(self) weakSelf = self;
    
    NSString *content = textField.text;
    if (textField.text.length > 140) {
        content = [content substringToIndex:140];
    }
    
    NSString * encodedString = [content stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                          
    NSString* url = [NSString stringWithFormat:@"%@/comment/post.json?openId=%@&content=%@&type=1&newsId=%@&newsType=0&action=1",TMDomain,[TMUserInfo getOpenID],encodedString,self.detailData.news_model.newsId];
    
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                weakSelf.textFieldBg.hidden = YES;
                [weakSelf.commentTextField resignFirstResponder];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

    }];
    return YES;
}

- (void)commentClick:(id)sender{
    if (!_textFieldBg) {
        self.textFieldBg = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-562/2-40, Screen_Width, 40)];
        self.textFieldBg.backgroundColor = HEXCOLOR(0xf9f5ef);
        [self.view addSubview:self.textFieldBg];
        
        self.commentTextField = [[UITextField alloc]initWithFrame:CGRectMake(12, 3, Screen_Width-24, 34)];
        self.commentTextField.backgroundColor = [UIColor whiteColor];
        
        self.commentTextField.layer.masksToBounds = YES;
        self.commentTextField.layer.cornerRadius = 10;
        self.commentTextField.layer.borderWidth  = TMOnePixel;
        self.commentTextField.layer.borderColor = Global_Brown_Color.CGColor;
        self.commentTextField.returnKeyType = UIReturnKeySend;
        self.commentTextField.delegate = self;
        [self.textFieldBg addSubview:self.commentTextField];
    }
    self.textFieldBg.hidden = NO;
    [self.commentTextField becomeFirstResponder];
}

- (void)collectClick:(id)sender{
    
    __weak TMNewsDetailViewController* weakSelf = self;
    [self.detailData.articleModel collect:nil WithCompletion:^(BOOL b) {
       
        [weakSelf.editView setIsCollect:b];
    }];
}

- (void)shareClick:(id)sender{
    [self.detailData shareToWeixin];
}

- (void)shareTimeLineClick:(id)sender{
    [self.detailData.articleModel shareTimeLine:nil WithCompletion:^(NSData *d) {
        if (d) {
            UIImage* img = [UIImage imageWithData:d];
            self.shareImageView.hidden = NO;
            [self.shareImageView setImage:img];
        }
    }];
}

- (void)createTable{
    
    _table_View = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), self.view.bounds.size.width, self.view.bounds.size.height-CGRectGetMaxY(self.navigationBar.frame)) style:UITableViewStylePlain];
    
    _table_View.delegate = self;
    _table_View.dataSource = self;
    
    _table_View.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_table_View];
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    view.backgroundColor = [UIColor clearColor];
    _table_View.tableFooterView = view;
}

#pragma mark - TableViewDelegate & TableViewDataSource

- (nonnull TMNewsDetailBaseCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSString* classNameStr = @"";
    if (indexPath.section <3) {
        classNameStr = [self.detailData.articleModel getDetailHeaderCellClassName:indexPath];
        TMNewsDetailBaseCell* cell = [tableView dequeueReusableCellWithIdentifier:classNameStr];
        if (cell == nil) {
            Class cellClass = NSClassFromString(classNameStr);
            cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:classNameStr Frame:self.view.bounds];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [self.detailData.articleModel updateCellInfo:cell];
        return cell;
    }else  if (indexPath.section == 4) {
        TMNewsDetailBaseCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TMNewsDetailBaseCell"];
        if (cell == nil) {
            cell = [[TMNewsDetailBaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:classNameStr];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell.contentView addSubview:self.commentConetentView];
        }
        //self.commentConetentView.hotCommentsArray =
        self.commentConetentView.commentsArray = self.comments;
        return cell;
    }
    else{
        TMArticleCellModel* eachCellModel = [self.detailData.articleModel.content objectAtIndex:indexPath.row];
        if (eachCellModel) {
            classNameStr = eachCellModel.classname;
            if (!classNameStr || [classNameStr isEqualToString:@""]) {
                NSLog(@"wangshun no have cell class");
                NSLog(@"TMNewsDetailBaseCell class name::%@",classNameStr);
            }
        }
        
        TMNewsDetailBaseCell* cell = [tableView dequeueReusableCellWithIdentifier:classNameStr];
        if (cell == nil) {
            Class cellClass = NSClassFromString(classNameStr);
            cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:classNameStr Frame:self.view.bounds];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        [cell updataCellInfo:eachCellModel];
        
        return cell;
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.detailData.articleModel tableView:tableView numberOfRowsInSection:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 4) {
        return self.commentTotalHeight;
    }
    return [self.detailData.articleModel tableView:tableView heightForRowAtIndexPath:indexPath];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.detailData.articleModel numberOfSectionsInTableView:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 4) {
        return 85;
    }
    return 0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    self.commentHeaderView.commentCount = self.comments.count;
    return self.commentHeaderView;
}


- (TMCommentHeaderView *)commentHeaderView {
    if (_commentHeaderView == nil) {
        _commentHeaderView = [[TMCommentHeaderView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 85)];
        _commentHeaderView.delegate = self;
        [_commentHeaderView tapIndex:1];
    }
    return _commentHeaderView;
}

- (TMCommentConetentView *)commentConetentView {
    if (_commentConetentView == nil) {
        _commentConetentView = [[TMCommentConetentView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _commentConetentView.delegate = self;
    }
    return _commentConetentView;
}


@end
