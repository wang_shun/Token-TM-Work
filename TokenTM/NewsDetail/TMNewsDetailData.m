//
//  TMNewsDetailData.m
//  TokenTM
//
//  Created by wang shun on 2018/5/22.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailData.h"
#import "WeiXinObject.h"
#import "TMCommentModel.h"

@implementation TMNewsDetailData

-(instancetype)initWithModel:(TMListModel *)model{
    if (self = [super init]) {
        self.news_model = model;
    }
    return self;
}

- (void)getDetailDataCompletion:(void (^)(void))method{
    if (isloading == YES) {
        return;
    }
    isloading = YES;
    
    NSString* url = [NSString stringWithFormat:@"%@/article/%@.json?openId=%@",TMDomain,self.news_model.newsId?:@"",[TMUserInfo getOpenID]];//@"2354259557066211333"
    isloading = YES;
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"getDetailData::%@",responseObject);
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                [self analyzeData:responseObject Completion:method];
            }
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"getDetailData::%@",error);
        isloading = NO;
    }];
}

- (void)getCommentDataCompletion:(void (^) (NSArray *commentList,CGFloat height) )method{
    
//    https://dj.gl/tokentm/v1/comment/list.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&newsId=2335291400939110400&newsType=0&page=1&pageSize=10
//    https://dj.gl/tokentm/v1/comment/hot.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&newsId=2335291400939110400&newsType=0&page=1&pageSize=10
    
    NSString* url = [NSString stringWithFormat:@"%@/comment/list.json?openId=%@&newsId=%@&newsType=0&page=1&pageSize=10",TMDomain,[TMUserInfo getOpenID],self.news_model.newsId?:@""];
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        //NSLog(@"getDetailData::%@",responseObject);
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                if ([responseObject objectForKey:@"data"] && [[responseObject objectForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dataDic = [responseObject objectForKey:@"data"];
                    if ([dataDic objectForKey:@"list"] && [[dataDic objectForKey:@"list"] isKindOfClass:[NSArray class]]) {
                        NSMutableArray *commentList = [NSMutableArray array];
                        CGFloat totalHeight = 0.f;
                        for (NSDictionary *dic in [dataDic objectForKey:@"list"]) {
                            TMCommentModel *model = [TMCommentModel commentModelFromDictionary:dic];
                            totalHeight += model.cellHeight;
                            [commentList addObject:model];
                        }
                        NSArray *comments = [NSArray arrayWithArray:commentList];
                        
                        if (method) {
                            method(comments,totalHeight);
                        }
                    }
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

    }];
}

- (void)analyzeData:(NSDictionary*)resp Completion:(void (^)(void))method{
    if (resp) {
        NSDictionary* articleDic = [resp objectForKey:@"data"];
        if (articleDic && [articleDic isKindOfClass:[NSDictionary class]]) {
            self.articleModel = [[TMArticleModel alloc] init];
            self.articleModel.resp = articleDic;
            
            [self.articleModel getTMArticleModel:articleDic];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (method) {
                    method();
                }
            });
        }
    }
}


-(void)shareToWeixin{
    if (self.articleModel) {
        if (self.articleModel.newsId) {
            NSString* shareUrl = [NSString stringWithFormat:@"http://token.tm/a/%@",self.articleModel.newsId];
        
            NSMutableDictionary* shareDic = [[NSMutableDictionary alloc] initWithCapacity:0];
            [shareDic setObject:shareUrl forKey:@"shareUrl"];
            [shareDic setObject:self.articleModel.title?:@"" forKey:@"shareTitle"];
            [shareDic setObject:@"" forKey:@"sharePic"];
            
            [WeiXinObject share:shareDic Completion:^(NSDictionary *resp) {
                
            }];
        }
    }
}


@end
