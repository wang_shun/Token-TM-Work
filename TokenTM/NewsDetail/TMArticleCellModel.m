//
//  TMArticleCellModel.m
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMArticleCellModel.h"

#import "TMNewsDetailCellHeader.h"

@implementation TMArticleCellModel

+ (TMArticleCellModel *)getCellModel:(NSDictionary *)dic{
    if (dic && [dic isKindOfClass:[NSDictionary class]]) {
        
        TMArticleCellModel* model = [[TMArticleCellModel alloc] init];
        model.cell_info = dic;
        CGFloat height = [TMArticleCellModel getNewsDetailHeight:dic WithModel:model];
        model.cell_height = height;
        
        NSString* type = [model.cell_info objectForKey:@"type"];
        model.classname = [TMArticleCellModel identificerCell:type];
        
        return model;
    }
    return nil;
}

+ (NSString*)identificerCell:(NSString*)type{
    NSString* classname = @"";
    
    if ([type isEqualToString:@"text"]) {
        classname = @"TMNewsDetailTextCell";
    }
    else if ([type isEqualToString:@"image"]) {
        classname = @"TMNewsDetailPicCell";
    }
    else if ([type isEqualToString:@"translation"]) {
        classname = @"TMNewsDetailTextCell";
    }
    else{
        classname = @"TMNewsDetailBaseCell";
    }
    
    return classname;
}


+ (CGFloat)getNewsDetailHeight:(NSDictionary*)dic WithModel:(TMArticleCellModel*)model{
    NSString* type = [dic objectForKey:@"type"];
    if ([type isEqualToString:@"text"] || [type isEqualToString:@"translation"]) {
        NSDictionary* attDic = [TMNewsDetailTextCell attriDic:dic];
        model.attr_Dic = attDic;
        NSString* info = [dic objectForKey:@"info"];
        CGFloat w = [UIScreen mainScreen].bounds.size.width-22;
        CGRect rect = [info boundingRectWithSize:CGSizeMake(w, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:attDic context:nil];
        return rect.size.height+10;
    }
    else if([type isEqualToString:@"image"]){
        CGFloat h = [TMNewsDetailPicCell getImageCellHeight:dic];
        return h;
    }
    
    return 0;
}

@end
