//
//  TMNewsDetailTextCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailTextCell : TMNewsDetailBaseCell

@property (nonatomic,strong) UILabel* content_Label;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

+ (UIFont*)getFont:(NSDictionary*)dic;

+ (NSDictionary*)attriDic:(NSDictionary*)data;

@end
