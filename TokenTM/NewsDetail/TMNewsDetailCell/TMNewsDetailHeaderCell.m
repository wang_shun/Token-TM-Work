//
//  TMNewsDetailHeaderCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailHeaderCell.h"
#import "UIImageView+WebCache.h"

@implementation TMNewsDetailHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.header = [[UIImageView alloc] initWithFrame:CGRectMake(11, 15, 36, 36)];
    self.header.layer.cornerRadius = 18;
    self.header.layer.masksToBounds = YES;
    [self.contentView addSubview:self.header];
    
    
    CGFloat x = CGRectGetMaxX(self.header.frame)+10;
    CGFloat y = CGRectGetMinY(self.header.frame);
    CGFloat w = self.cell_frame.size.width-x;
    CGFloat h = CGRectGetHeight(self.header.frame)/2.0;
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel setFont:[UIFont systemFontOfSize:14]];
    
    self.readerNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y+h, w, h)];
    [self.contentView addSubview:self.readerNumLabel];
    [self.readerNumLabel setFont:[UIFont systemFontOfSize:14]];
    
}

-(void)updataCellInfo:(id)cell_info{
    if ([cell_info isKindOfClass:[NSDictionary class]]) {
        NSDictionary* d = (NSDictionary*)cell_info;
        
        NSString* avatarUrl = [d objectForKey:@"avatarUrl"];
        [self.header sd_setImageWithURL:[NSURL URLWithString:avatarUrl] placeholderImage:nil];
        
        NSString* nickName = [d objectForKey:@"nickName"];
        [self.nameLabel setText:[NSString stringWithFormat:@"%@ 推荐",nickName?:@""]];
        
        NSNumber* readTimes = [d objectForKey:@"readTimes"];
        [self.readerNumLabel setText:[NSString stringWithFormat:@"阅读 %@",readTimes]];
    }
}


+ (CGFloat)getCellHeight:(NSDictionary*)cellInfo{
    return 15+15+36;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
