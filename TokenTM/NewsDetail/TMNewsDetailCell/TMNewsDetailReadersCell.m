//
//  TMNewsDetailReadersCell.m
//  TokenTM
//
//  Created by wang shun on 2018/7/22.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailReadersCell.h"
#import "UIImageView+WebCache.h"

@implementation TMNewsDetailReadersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)updataCellInfo:(id)cell_info{
    
    if (cell_info) {
        NSArray* list = [cell_info objectForKey:@"readerList"];
        if (list.count<=9) {
            [self.bgView setFrame:CGRectMake(11, 5, self.cell_frame.size.width-11-11, 80)];
        }
        else{
            [self.bgView setFrame:CGRectMake(11, 5, self.cell_frame.size.width-11-11, 110)];
        }
        
        [self createHeaders:list];
        NSNumber* num = [cell_info objectForKey:@"readNum"];
        [self createLabel:num];
    }
    else{
        for (UIView* v in self.bgView.subviews) {
            [v removeFromSuperview];
        }
        [self.bgView setFrame:CGRectZero];
    }
}

- (void)createLabel:(NSNumber*)num{
    NSString* numstr = [NSString stringWithFormat:@"%@",num];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(11,self.bgView.frame.size.height-30 , self.cell_frame.size.width-11-11, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
    [self.contentView addSubview:label];
    NSString* c = [NSString stringWithFormat:@"有%@位朋友阅读过",numstr];

    NSDictionary* att_dic = @{NSFontAttributeName:[UIFont systemFontOfSize:13 weight:UIFontWeightThin],NSForegroundColorAttributeName:[UIColor blackColor]};
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:c];
    [str setAttributes:att_dic range:NSMakeRange(0, c.length)];
    att_dic = @{NSFontAttributeName:[UIFont systemFontOfSize:13 weight:UIFontWeightThin],NSForegroundColorAttributeName:RGBACOLOR(188, 166, 121, 1)};
    [str setAttributes:att_dic range:NSMakeRange(1, numstr.length)];
    
    [label setAttributedText:str];
}

- (void)createHeaders:(NSArray*)list{
    
    CGFloat img_w = 26;
    CGFloat img_s = 10;
    CGFloat img_y = 7;
    CGFloat img_x = 0;
    
    if (_bgView.frame.size.height == 60) {
        
        CGFloat n = list.count;
        if (list.count >= 9) {
            n = 9;
        }
        
        CGFloat w = n* (img_w) + (n-1)*img_s;
        img_x = ((self.cell_frame.size.width-11-11) - w)/2.0;
        
        for (int i = 0; i<list.count; i++) {
            NSDictionary* dic = [list objectAtIndex:i];
            NSString* avatarUrl = [dic objectForKey:@"avatarUrl"];
            if (i>=9) {
                break;
            }
            
            CGRect rect = CGRectMake(img_x, img_y, img_w, img_w);
            UIImageView* imageV = [[UIImageView alloc] initWithFrame:rect];
            [imageV sd_setImageWithURL:[NSURL URLWithString:avatarUrl]];
            [self.bgView addSubview:imageV];
            imageV.layer.cornerRadius = img_w/2.0;
            imageV.layer.masksToBounds = YES;
            
            img_x = img_x + img_w + img_s;
        }
    }
    else{
        
        CGFloat n = list.count;
        if (list.count >= 9) {
            n = 9;
        }
        
        CGFloat w = n* (img_w) + (n-1)*img_s;
        img_x = ((self.cell_frame.size.width-11-11) - w)/2.0;
        
        for (int i = 0; i<list.count; i++) {
            NSDictionary* dic = [list objectAtIndex:i];
            NSString* avatarUrl = [dic objectForKey:@"avatarUrl"];
            
            if (i ==9) {
                img_x = ((self.cell_frame.size.width-11-11) - w)/2.0;
                img_y = img_y+img_w+img_y;
            }
            
            CGRect rect = CGRectMake(img_x, img_y, img_w, img_w);
            
            UIImageView* imageV = [[UIImageView alloc] initWithFrame:rect];
            [imageV sd_setImageWithURL:[NSURL URLWithString:avatarUrl]];
            [self.bgView addSubview:imageV];
            imageV.layer.cornerRadius = img_w/2.0;
            imageV.layer.masksToBounds = YES;
            
            img_x = img_x + img_w + img_s;
        }
        
    }
}

- (void)createView{
    self.bgView = [[UIView alloc] initWithFrame:CGRectMake(11, 5, self.cell_frame.size.width-11-11, 0)];
    [self.contentView addSubview:self.bgView];
    self.bgView.backgroundColor = RGBACOLOR(249, 245, 239, 1);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat)getCellHeight:(NSDictionary *)cellInfo{
    if (cellInfo) {
        NSArray* list = [cellInfo objectForKey:@"readerList"];
        if (list.count<=9) {
            return 90;
        }
    }
    return 120;
}

@end
