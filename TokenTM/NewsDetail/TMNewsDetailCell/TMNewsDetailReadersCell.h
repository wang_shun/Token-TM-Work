//
//  TMNewsDetailReadersCell.h
//  TokenTM
//
//  Created by wang shun on 2018/7/22.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailReadersCell : TMNewsDetailBaseCell

@property (nonatomic,strong) UIView* bgView;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

-(void)updataCellInfo:(id)cell_info;

+ (CGFloat)getCellHeight:(NSDictionary *)cellInfo;

@end
