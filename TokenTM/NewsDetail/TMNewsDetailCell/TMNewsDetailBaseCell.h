//
//  TMNewsDetailBaseCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/23.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMArticleCellModel.h"

@interface TMNewsDetailBaseCell : UITableViewCell

@property (nonatomic,assign) CGRect cell_frame;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

- (void)updataCellInfo:(id)cell_info;

+ (CGFloat)getCellHeight:(NSDictionary*)cellInfo;

@end
