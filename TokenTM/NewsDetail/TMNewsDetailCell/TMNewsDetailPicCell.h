//
//  TMNewsDetailPicCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/27.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailPicCell : TMNewsDetailBaseCell

@property (nonatomic,strong) UIImageView* content_Image;
@property (nonatomic,strong) NSString* imgUrl;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

+ (CGFloat)getImageCellHeight:(NSDictionary*)dic;

@end
