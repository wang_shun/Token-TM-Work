//
//  TMNewsDetailTitleSourceCell.h
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailTitleSourceCell : TMNewsDetailBaseCell

-(void)updataCellInfo:(id)cell_info;
@end
