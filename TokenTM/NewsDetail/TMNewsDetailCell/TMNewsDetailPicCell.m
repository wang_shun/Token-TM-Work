//
//  TMNewsDetailPicCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/27.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailPicCell.h"
#import "UIImageView+WebCache.h"

@implementation TMNewsDetailPicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.content_Image = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.content_Image];
}

- (void)updataCellInfo:(TMArticleCellModel *)cell_info{

    CGFloat h = [TMNewsDetailPicCell getImageCellHeight:cell_info.cell_info];
    [self.content_Image setFrame:CGRectMake(11, 5, self.cell_frame.size.width-22, h-10)];
    
    NSDictionary* imageInfo = [cell_info.cell_info objectForKey:@"imageInfo"];
    NSString* height = [imageInfo objectForKey:@"height"];
    NSString* width  = [imageInfo objectForKey:@"width"];
    if (height.floatValue>0 && width.floatValue >0) {
        NSString* src = [imageInfo objectForKey:@"src"];
        if (src && src.length>0) {
            [self.content_Image sd_setImageWithURL:[NSURL URLWithString:src] placeholderImage:nil];
        }
        else{
            [self.content_Image sd_setImageWithURL:nil placeholderImage:nil];
        }
        self.content_Image.hidden = NO;
    }
    else{
        self.content_Image.hidden = YES;
        [self.content_Image sd_setImageWithURL:nil];
    }
}

+(CGFloat)getImageCellHeight:(NSDictionary *)dic{
    
    NSDictionary* imageInfo = [dic objectForKey:@"imageInfo"];
    if (imageInfo) {
        NSString* height = [imageInfo objectForKey:@"height"];
        NSString* width  = [imageInfo objectForKey:@"width"];
        if (height.floatValue >0 && width.floatValue>0) {
            CGFloat h = height.floatValue;
            CGFloat w = width.floatValue;
            CGFloat b = h/(float)w;
            CGFloat img_W = [UIScreen mainScreen].bounds.size.width-22;
            CGFloat img_H = img_W * b;
            return img_H+10;
        }
    }
    
    return 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
