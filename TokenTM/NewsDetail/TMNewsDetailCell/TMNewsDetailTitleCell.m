//
//  TMNewsDetailTItleCell.m
//  TokenTM
//
//  Created by wang shun on 2018/6/5.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailTitleCell.h"

@implementation TMNewsDetailTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.title_Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.title_Label setTextAlignment:NSTextAlignmentLeft];
    [self.title_Label setFont:[TMNewsDetailTitleCell myFont]];
    [self.title_Label setBackgroundColor:[UIColor clearColor]];
    self.title_Label.numberOfLines = 0;
    [self.contentView addSubview:self.title_Label];
    
    self.source_Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.source_Label setBackgroundColor:[UIColor clearColor]];
    [self.source_Label setTextAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.source_Label];
    
    self.time_Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.time_Label setBackgroundColor:[UIColor clearColor]];
    [self.time_Label setTextAlignment:NSTextAlignmentRight];
    [self.contentView addSubview:self.time_Label];
}

-(void)updataCellInfo:(TMArticleCellModel *)cell_info{
    if ([cell_info isKindOfClass:[NSDictionary class]]) {
        NSDictionary* dic = (NSDictionary*)cell_info;
        NSString* str = [dic objectForKey:@"title"];
        CGFloat h = [TMNewsDetailTitleCell getTitleCellHeight:dic];
        [self.title_Label setFrame:CGRectMake(11, 7, self.cell_frame.size.width-22, h-15)];
        [self.title_Label setText:str];
    }
}

+ (CGFloat)getTitleCellHeight:(NSDictionary*)dic{
    NSString* title = [dic objectForKey:@"title"];
    if (title.length>0) {
        CGFloat w = [UIScreen mainScreen].bounds.size.width-22;
        CGRect r = [title boundingRectWithSize:CGSizeMake(w, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[TMNewsDetailTitleCell myFont]} context:nil];
        return r.size.height+15;
    }
    return 0;
}

+ (UIFont*)myFont{
    return [UIFont systemFontOfSize:22 weight:UIFontWeightBold];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
