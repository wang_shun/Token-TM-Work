//
//  TMNewsDetailTitleSourceCell.m
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailTitleSourceCell.h"

@interface TMNewsDetailTitleSourceCell ()

@property (nonatomic,strong) UILabel* sourceLabel;
@property (nonatomic,strong) UILabel* timeLabel;

@end

@implementation TMNewsDetailTitleSourceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.sourceLabel = [[UILabel alloc] initWithFrame:CGRectMake(11, ((35-13)/2.0), (self.cell_frame.size.width-22)/2.0, 13)];
    [self.sourceLabel setTextColor:RGBACOLOR(107, 107, 107, 1)];
    [self.sourceLabel setFont:[UIFont systemFontOfSize:11]];
    [self.sourceLabel setText:@"来源:区块链在线"];
    [self.contentView addSubview:self.sourceLabel];
    self.sourceLabel.textAlignment = NSTextAlignmentLeft;
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.cell_frame.size.width-(self.cell_frame.size.width-22)/2.0-11, ((35-13)/2.0), (self.cell_frame.size.width-22)/2.0, 13)];
    [self.timeLabel setTextColor:RGBACOLOR(107, 107, 107, 1)];
    [self.timeLabel setFont:[UIFont systemFontOfSize:11]];
    [self.timeLabel setText:@""];
    [self.contentView addSubview:self.timeLabel];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
}

- (void)updataCellInfo:(id)cell_info{
    [self.sourceLabel setText:@""];
    [self.timeLabel setText:@""];
    
    NSString* time = [cell_info objectForKey:@"time"];
    if (time) {
        [self.timeLabel setText:time];
    }
    
    NSString* media = [cell_info objectForKey:@"media"];
    if (media) {
        NSString* str = [NSString stringWithFormat:@"来源：%@",media];
        [self.sourceLabel setText:str];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
