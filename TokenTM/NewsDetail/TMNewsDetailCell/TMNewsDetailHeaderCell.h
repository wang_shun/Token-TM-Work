//
//  TMNewsDetailHeaderCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailHeaderCell : TMNewsDetailBaseCell

@property (nonatomic,strong) UIImageView* header;
@property (nonatomic,strong) UILabel* nameLabel;
@property (nonatomic,strong) UILabel* readerNumLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

+ (CGFloat)getCellHeight:(NSDictionary*)cellInfo;

@end

