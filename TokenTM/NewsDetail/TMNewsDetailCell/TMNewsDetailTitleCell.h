//
//  TMNewsDetailTItleCell.h
//  TokenTM
//
//  Created by wang shun on 2018/6/5.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailTitleCell : TMNewsDetailBaseCell

@property (nonatomic,strong) UILabel* title_Label;

@property (nonatomic,strong) UILabel* source_Label;
@property (nonatomic,strong) UILabel* time_Label;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect;

+ (CGFloat)getTitleCellHeight:(NSDictionary*)dic;

@end
