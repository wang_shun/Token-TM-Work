//
//  TMNewsDetailTextCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailTextCell.h"

@implementation TMNewsDetailTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    self.content_Label = [[UILabel alloc] initWithFrame:CGRectMake(11, 0, self.cell_frame.size.width-22, 0)];
    [self.content_Label setText:@"Elpis 如何运作?"];
    [self.content_Label setFont:[UIFont systemFontOfSize:15 weight:UIFontWeightBold]];
    [self.contentView addSubview:self.content_Label];
    self.content_Label.numberOfLines = 0;
}

- (void)updataCellInfo:(TMArticleCellModel *)cell_info{
    
    [self.content_Label setFrame:CGRectMake(11, 0, self.cell_frame.size.width-22, cell_info.cell_height)];
    
    NSDictionary* dic = cell_info.cell_info;
    UIFont* font = [TMNewsDetailTextCell getFont:dic];
    [self.content_Label setFont:font];

    NSString* info = [dic objectForKey:@"info"];
    NSAttributedString* attr_info = [[NSAttributedString alloc] initWithString:info attributes:cell_info.attr_Dic];
    [self.content_Label setAttributedText:attr_info];
}

+ (UIFont*)getFont:(NSDictionary*)dic{
    NSString* tag = [dic objectForKey:@"tag"];
    if ([tag isEqualToString:@"strong"]) {
        return [UIFont systemFontOfSize:15 weight:UIFontWeightBold];
    }
    else{
        return [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
    }
}

+ (NSDictionary*)attriDic:(NSDictionary*)data{
    UIFont* font = [TMNewsDetailTextCell getFont:data];
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setObject:font forKey:NSFontAttributeName];
    
    NSMutableParagraphStyle* style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 8;
    [dic setObject:style forKey:NSParagraphStyleAttributeName];
    
    return dic;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
