//
//  TMNewsDetailBaseCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/23.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMNewsDetailBaseCell.h"

@interface TMNewsDetailBaseCell ()

@end

@implementation TMNewsDetailBaseCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.cell_frame = rect;
    }
    return self;
}

- (void)updataCellInfo:(TMArticleCellModel *)cell_info{
    
}

+ (CGFloat)getCellHeight:(NSDictionary*)cellInfo{
    return 0;
}
@end
