//
//  TMNewsDetailViewController.h
//  TokenTM
//
//  Created by wang shun on 2018/5/13.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBaseViewController.h"
#import "TMListModel.h"

@interface TMNewsDetailViewController : TMBaseViewController

- (instancetype)initWithTMListModel:(TMListModel*)model; 
- (instancetype)initWithNewsId:(NSString*)newsId;

@end
