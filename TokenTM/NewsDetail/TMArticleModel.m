//
//  TMArticleModel.m
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMArticleModel.h"

#import "TMArticleCellModel.h"

@implementation TMArticleModel

-(instancetype)init{
    if (self = [super init]) {
        self.content = [[NSMutableArray alloc] initWithCapacity:0];
        self.isCollected = NO;
        self.readerList = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)getTMArticleModel:(NSDictionary *)dic{
    if (dic) {
        NSString* newsId = [dic objectForKey:@"newsId"];
        self.newsId = newsId;
        NSNumber* news_t = [dic objectForKey:@"newsType"];
        self.newsType = [NSString stringWithFormat:@"%@",news_t];
        NSNumber* templateType = [dic objectForKey:@"templateType"];
        self.templateType = [NSString stringWithFormat:@"%@",templateType];
        
        NSNumber* n = [dic objectForKey:@"collected"];
        self.isCollected = (n.integerValue==1)?YES:NO;
        
        self.time = [dic objectForKey:@"time"]?:@"";
        self.media = [dic objectForKey:@"media"]?:@"";
        
        NSArray* arr = [dic objectForKey:@"content"];
        if (arr && arr.count > 0) {
            [self.content removeAllObjects];
            for (NSDictionary* eachCellInfo in arr) {
                TMArticleCellModel* each_Cell_Model = [TMArticleCellModel getCellModel:eachCellInfo];
                if (each_Cell_Model && [each_Cell_Model isKindOfClass:[TMArticleCellModel class]]) {
                    [self.content addObject:each_Cell_Model];
                }
            }
        }
        
        NSDictionary* u_dic = [dic objectForKey:@"userInfo"];
        if (u_dic && [u_dic isKindOfClass:[NSDictionary class]]) {
            self.userInfo = u_dic;
        }
        
        
        NSNumber* rts = [dic objectForKey:@"readTimes"];
        if (rts && [rts isKindOfClass:[NSNumber class]]) {
            self.readTimes = rts;
        }
        
        NSString* t = [dic objectForKey:@"title"];
        self.title = t;
        
        NSArray* rlist = [dic objectForKey:@"readerList"];
        if (rlist && rlist.count>0) {
            self.readerList = [NSMutableArray arrayWithArray:rlist];
            NSNumber* rNum = [dic objectForKey:@"readerNum"];
            if (rNum && [rNum isKindOfClass:[NSNumber class]]) {
                self.readerNum = rNum;
            }
        }
        
        NSDictionary* uinfo = [dic objectForKey:@"userInfo"];
        if (uinfo && [uinfo isKindOfClass:[NSDictionary class]]) {
            self.userInfo = uinfo;
        }
        
    }
}


- (void)updateCellInfo:(TMNewsDetailBaseCell*)cell{
    if ([cell isMemberOfClass:[TMNewsDetailTitleCell class]]) {
        TMNewsDetailTitleCell* c = (TMNewsDetailTitleCell*)cell;
        [c updataCellInfo:@{@"title":self.title}];
    }
    if ([cell isMemberOfClass:[TMNewsDetailHeaderCell class]]) {
        TMNewsDetailHeaderCell* c = (TMNewsDetailHeaderCell*)cell;
        NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithDictionary:self.userInfo];
        if (self.readTimes) {
            [dic setObject:self.readTimes forKey:@"readTimes"];
        }
        
        [c updataCellInfo:dic];
    }
    
    if ([cell isMemberOfClass:[TMNewsDetailReadersCell class]]) {
        TMNewsDetailReadersCell* c = (TMNewsDetailReadersCell*)cell;
        
        NSDictionary* dic = @{@"readerList":self.readerList,@"readNum":self.readerNum?:[NSNumber numberWithInt:0]};
        [c updataCellInfo:dic];
    }
    
    if ([cell isMemberOfClass:[TMNewsDetailTitleSourceCell class]]) {
        TMNewsDetailTitleSourceCell* c = (TMNewsDetailTitleSourceCell*)cell;
        [c updataCellInfo:@{@"media":self.media,@"time":self.time}];
    }
}

- (NSString *)getDetailHeaderCellClassName:(NSIndexPath*)indexPath{
    
    if (indexPath.section == 0) {
        return @"TMNewsDetailHeaderCell";
    }
    else if (indexPath.section ==1){
        if (indexPath.row ==1) {
            return @"TMNewsDetailTitleSourceCell";
        }
        return @"TMNewsDetailTitleCell";
    }
    else if (indexPath.section ==2){
        if (self.readerList.count>0) {
            return @"TMNewsDetailReadersCell";
        }
        return @"TMNewsDetailBaseCell";
    }
    
    return @"TMNewsDetailBaseCell";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [TMNewsDetailHeaderCell getCellHeight:nil];
    }
    else if (indexPath.section ==1){
        if (indexPath.row == 0) {
            if(self.title && self.title.length>0){
                CGFloat h = [TMNewsDetailTitleCell getTitleCellHeight:@{@"title":self.title?:@""}];
                return h;
            }
            return 0;
        }
        else if (indexPath.row == 1){
            return 35;
        }
        else{
            return 0;
        }
    }
    else if (indexPath.section ==2){
        if(self.readerList.count>0){
            NSDictionary* dic = @{@"readerList":self.readerList,@"readNum":self.readerNum?:[NSNumber numberWithInt:0]};
            return [TMNewsDetailReadersCell getCellHeight:dic];
        }
        else{
            return 0;
        }
    }
    else{
        TMArticleCellModel* eachCellModel = [self.content objectAtIndex:indexPath.row];
        if (eachCellModel) {
            CGFloat height = eachCellModel.cell_height;
            return height;
        }
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        if(self.userInfo){
            return 1;
        }
        else{
            return 0;
        }
    }
    else if (section ==1){
        if(self.title && self.title.length>0){
            return 2;
        }
        else{
            return 0;
        }
    }
    else if (section == 2){
        if(self.readerList.count>0){
            return 1;
        }
        else{
            return 0;
        }
    }
    else if (section == 4){
        return 1;
    }
    else{
        NSInteger num = self.content.count;
        return num;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger number = 4;
    number += 1;
    return number;
}



#pragma mark - 举报

-(void)report:(NSDictionary *)info{
    
    if (isloading == YES) {
        return;
    }
    
    NSString* reason = [info objectForKey:@"reason"];
    
    NSString* url = [NSString stringWithFormat:@"%@/collect/report.json?openId=%@&reason=%@&newsId=%@&newsType=%@",TMDomain,[TMUserInfo getOpenID],reason,self.newsId,self.newsType];
    isloading = YES;
    
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"report::resp:::%@",responseObject);
        if (responseObject) {
            NSNumber* statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.integerValue == 200) {
//                NSString* msg = [responseObject objectForKey:@""];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [TMToast showToast:ToastTypeBlank text:@"举报成功"];
                });
                
            }
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        isloading = NO;
    }];
}

#pragma mark - 收藏

- (void)collect:(NSDictionary*)info WithCompletion:(void(^)(BOOL b))method{
    
    if (isloading == YES) {
        return;
    }
    
    if (self.isCollected == NO) {
        NSString* url = [NSString stringWithFormat:@"%@/collect/add.json?openId=%@&newsId=%@",TMDomain,[TMUserInfo getOpenID],self.newsId];
        isloading = YES;
        [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"add collect  resp::::%@",responseObject);
            
            if (responseObject) {
                NSNumber* statusCode = [responseObject objectForKey:@"statusCode"];
                if (statusCode.integerValue == 200) {
                    NSString* data = [responseObject objectForKey:@"data"];
                    if ([data isEqualToString:@"success"]) {
                        self.isCollected = YES;
                        if (method) {
                            method(self.isCollected);
                        }
                        isloading = NO;
                        return;
                    }
                }
            }
            
            isloading = NO;
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            isloading = NO;
        }];
    }
    else{
        NSString* url = [NSString stringWithFormat:@"%@/collect/delete.json?openId=%@&newsId=%@",TMDomain,[TMUserInfo getOpenID],self.newsId];
        isloading = YES;
        [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"add collect  resp::::%@",responseObject);
            
            if (responseObject) {
                NSNumber* statusCode = [responseObject objectForKey:@"statusCode"];
                if (statusCode.integerValue == 200) {
                    NSString* data = [responseObject objectForKey:@"data"];
                    if ([data isEqualToString:@"success"]) {
                        self.isCollected = NO;
                        if (method) {
                            method(self.isCollected);
                        }
                        isloading = NO;
                        return;
                    }
                }
            }
            isloading = NO;
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            isloading = NO;
        }];
    }

}

- (void)shareTimeLine:(NSDictionary*)info WithCompletion:(void(^)(NSData* d))method{
    if (isloading == YES) {
        return;
    }
    
    NSString* url = [NSString stringWithFormat:@"%@/share/qrCode.json?openId=%@&newsId=%@",TMDomain,[TMUserInfo getOpenID],self.newsId];
    isloading = YES;
    [TMNetService GETIMAGE:url success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"shareTimeLine resp:::%@",responseObject);
        
        NSData* data = [NSData dataWithData:responseObject];
        if (method) {
            method(data);
        }
        isloading = NO;
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"shareTimeLine error:::%@",error);
        isloading = NO;
    }];
}


@end
