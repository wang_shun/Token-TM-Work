//
//  TMArticleEditView.h
//  TokenTM
//
//  Created by wang shun on 2018/6/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMArticleEditViewDelegate;
@interface TMArticleEditView : UIView

@property (nonatomic,weak) id <TMArticleEditViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setIsCollect:(BOOL)b;
- (void)setEnableCollect:(BOOL)b;

@end

@protocol TMArticleEditViewDelegate <NSObject>

- (void)reportClick:(id)sender;
- (void)collectClick:(id)sender;
- (void)commentClick:(id)sender;

- (void)shareClick:(id)sender;
- (void)shareTimeLineClick:(id)sender;

@end
