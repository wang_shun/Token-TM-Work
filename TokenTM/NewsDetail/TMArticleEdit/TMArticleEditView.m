//
//  TMArticleEditView.m
//  TokenTM
//
//  Created by wang shun on 2018/6/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMArticleEditView.h"

#import "TMRedView.h"

@interface TMArticleEditView ()<TMRedViewDelegate>

@property (nonatomic,strong) TMRedView* reportBtn;
@property (nonatomic,strong) TMRedView* commentBtn;
@property (nonatomic,strong) TMRedView* collectBtn;

@property (nonatomic,strong) TMRedView* redView;

@property (nonatomic,strong) UIButton* shareBtn;
@property (nonatomic,strong) UIButton* shareTimeLineBtn;


@end

@implementation TMArticleEditView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}


- (void)createView{
    CGFloat w = 70/2.0;
    CGFloat y = self.bounds.size.height-w-15;
    
    CGFloat x = self.bounds.size.width-11-w;
    self.reportBtn = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, w)];
    [self.reportBtn setBgColor:Global_Brown_Color];
    [self.reportBtn setImage:@"news_report.png"];
    [self addSubview:_reportBtn];
    self.reportBtn.delegate = self;
    
    x = self.bounds.size.width-11-w-15-w;
    self.collectBtn = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, w)];
    [self.collectBtn setBgColor:Global_Brown_Color];
    [self.collectBtn setImage:@"news_collect.png"];
    [self.collectBtn setSelected_name:@"news_collect_press.png"];
    [self addSubview:self.collectBtn];
    self.collectBtn.delegate = self;
    
    x = self.bounds.size.width-11-w-15-w-15-w;
    self.commentBtn = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, w)];
    [self.commentBtn setBgColor:Global_Brown_Color];
    [self.commentBtn setImage:@"news_comment.png"];
    [self addSubview:self.commentBtn];
    self.commentBtn.delegate = self;
    
    self.redView = [[TMRedView alloc] initWithFrame:CGRectMake(11, y, 90, w)];
    [self.redView setlayCornerRadius:w/2.0];
    [self addSubview:_redView];
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake((self.redView.bounds.size.width-TMOnePixel)/2.0, 0, TMOnePixel, w)];
    [line setBackgroundColor:RGBACOLOR(149, 32, 24, 1)];
    [self.redView addSubview:line];
    
    x = (w/4.0)+(CGRectGetMinX(line.frame)-(w/4.0)-16)/2.0;
    y = (w-16)/2.0;
    UIImageView* share = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 16, 16)];
    share.image = [UIImage imageNamed:@"news_share.png"];
    [self.redView addSubview:share];

    x = CGRectGetMaxX(line.frame)+(((90-w/4.0)-CGRectGetMaxX(line.frame))-18)/2.0;
    y = (w-18)/2.0;
    UIImageView* shareTimeLine = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 18, 18)];
    shareTimeLine.image = [UIImage imageNamed:@"news_share_Timeline.png"];
    [self.redView addSubview:shareTimeLine];
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, 45, w)];
    [self.redView addSubview:btn];
    [btn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    self.shareBtn = btn;
    
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(45, 0, 45, w)];
    [self.redView addSubview:btn];
    [btn addTarget:self action:@selector(shareTimeLine) forControlEvents:UIControlEventTouchUpInside];
    self.shareTimeLineBtn = btn;
    
    [self setEnableCollect:NO];
}

- (void)share{
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareClick:)]) {
        [self.delegate shareClick:self];
    }
}

- (void)shareTimeLine{
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareTimeLineClick:)]) {
        [self.delegate shareTimeLineClick:self];
    }
}

-(void)click:(id)sender{
    if ([sender isEqual:self.reportBtn]) {
        if(self.delegate && [self.delegate respondsToSelector:@selector(reportClick:)]){
            [self.delegate reportClick:nil];
        }
    }
    else if ([sender isEqual:self.collectBtn]){
        if(self.delegate && [self.delegate respondsToSelector:@selector(collectClick:)]){
            [self.delegate collectClick:self];
        }
    }
    else if ([sender isEqual:self.commentBtn]){
        if(self.delegate && [self.delegate respondsToSelector:@selector(commentClick:)]){
            [self.delegate commentClick:nil];
        }
    }
}

- (void)setIsCollect:(BOOL)b{
    [self.collectBtn setIsSelected:b];
}

- (void)setEnableCollect:(BOOL)b{
    if (b) {
        self.collectBtn.hidden = NO;
        CGFloat w = self.collectBtn.frame.size.width;
        CGFloat x = self.bounds.size.width-11-w-15-w-15-w;
        CGFloat y = self.commentBtn.frame.origin.y;
        [self.commentBtn setFrame:CGRectMake(x, y, w, w)];
    }
    else{
        self.collectBtn.hidden = YES;
        [self.commentBtn setFrame:self.collectBtn.frame];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
