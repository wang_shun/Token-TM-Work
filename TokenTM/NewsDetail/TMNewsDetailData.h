//
//  TMNewsDetailData.h
//  TokenTM
//
//  Created by wang shun on 2018/5/22.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMListModel.h"
#import "TMArticleModel.h"

@interface TMNewsDetailData : NSObject
{
    BOOL isloading;
}
@property (nonatomic,strong) TMListModel* news_model;

@property (nonatomic,strong) NSMutableArray* detailArr;
@property (nonatomic,strong) TMArticleModel* articleModel;


- (instancetype)initWithModel:(TMListModel*)model;

- (void)getDetailDataCompletion:(void (^)(void))method;

- (void)getCommentDataCompletion:(void (^) (NSArray *commentList,CGFloat height) )method;

- (void)shareToWeixin;

@end

@protocol TMNewsDetailDataDelegate <NSObject>

@end
