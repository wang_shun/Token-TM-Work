//
//  TMArticleShareImageView.h
//  TokenTM
//
//  Created by wang shun on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMArticleShareImageView : UIView

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setImage:(UIImage*)image;

@end
