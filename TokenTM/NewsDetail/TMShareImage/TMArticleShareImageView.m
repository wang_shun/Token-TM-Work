//
//  TMArticleShareImageView.m
//  TokenTM
//
//  Created by wang shun on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMArticleShareImageView.h"

@interface TMArticleShareImageView ()

@property (nonatomic,strong) UIView* bgView;

@property (nonatomic,strong) UIImageView* shareImageView;

@property (nonatomic,strong) UIButton* saveBtn;

@property (nonatomic,strong) UIImage* saveImage;

@end

@implementation TMArticleShareImageView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}


- (void)createView{
    self.bgView = [[UIView alloc] initWithFrame:self.bounds];
    self.bgView.backgroundColor = [UIColor blackColor];
    self.bgView.alpha = 0.5;
    [self addSubview:self.bgView];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [self.bgView addGestureRecognizer:tap];
    
    self.shareImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self addSubview:self.shareImageView];
    
    self.saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.saveBtn];
    [self.saveBtn setFrame:CGRectZero];
    [self.saveBtn addTarget:self action:@selector(saveClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.saveBtn setBackgroundColor:RGBACOLOR(204, 51, 51, 1)];
    [self.saveBtn setTitle:@"保存" forState:UIControlStateNormal];

}

- (void)tapPress:(UIGestureRecognizer*)gusture{
    self.hidden = YES;
}

-(void)setImage:(UIImage *)image{
    if (image) {
        
        if (image.size.height>image.size.width) {
            CGFloat h = self.bounds.size.height*0.8;
            CGFloat b = image.size.width/image.size.height;
            CGFloat w = h*b;
            CGFloat x = (self.bounds.size.width-w)/2.0;
            CGFloat y = self.bounds.size.height*0.04;
            [self.shareImageView setFrame:CGRectMake(x, y, w, h)];
            [self.shareImageView setImage:image];
            
            w = w*0.5;
            h = 40;
            y = CGRectGetMaxY(self.shareImageView.frame)+ (self.bounds.size.height-CGRectGetMaxY(self.shareImageView.frame)-h)/2.0;
            x = (self.bounds.size.width-w)/2.0;
            
            [self.saveBtn setFrame:CGRectMake(x, y, w, h)];
            self.saveBtn.layer.cornerRadius = h/2.0;
            self.saveBtn.layer.masksToBounds = YES;
            
            self.saveImage = image;
            
        }
    }
}

- (void)saveClick:(UIButton*)sender{
    [self loadImageFinished:self.saveImage];
}


- (void)loadImageFinished:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
