//
//  TMArticleCellModel.h
//  TokenTM
//
//  Created by wang shun on 2018/5/26.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMArticleCellModel : NSObject

@property (nonatomic,strong) NSString* classname;
@property (nonatomic,strong) NSDictionary* cell_info;
@property (nonatomic,assign) CGFloat cell_height;
@property (nonatomic,strong) NSDictionary* attr_Dic;

+ (TMArticleCellModel*)getCellModel:(NSDictionary*)dic;

@end
