//
//  TMNetService.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMNetService : NSObject

//获取图片 仅
+ (void)GETIMAGE:(NSString*)url success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (void)GET:(NSString*)url success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (void)GET:(NSString*)url AcceptableContentTypes:(NSString*)contentTypes success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (void)POST:(NSString*)url parameters:(NSDictionary*)param success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

//post
+ (void)POST:(NSString*)url parameters:(NSDictionary*)param File:(NSArray <NSData*>*)files success:(void (^)(NSURLSessionDataTask *task, id responseObject)) success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (id)shared;


@end
