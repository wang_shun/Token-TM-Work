//
//  TMNetService.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMNetService.h"
#import <AFNetworking/AFNetworking.h>

@interface TMNetService ()

@property (nonatomic,strong) AFHTTPSessionManager *manager;

@property (nonatomic,strong) AFHTTPSessionManager *file_manager;

@end

@implementation TMNetService

+ (void)GETIMAGE:(NSString*)url success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    TMNetService* serivce = [TMNetService shared];
    [serivce.file_manager GET:url parameters:nil success:success failure:failure];
}


+ (void)GET:(NSString*)url success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    [TMNetService GET:url AcceptableContentTypes:nil success:success failure:failure];
}



+ (void)GET:(NSString*)url AcceptableContentTypes:(NSString*)contentTypes success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    TMNetService* serivce = [TMNetService shared];
    [serivce setHeader];
    NSLog(@"get:::url:%@",url);
    [serivce.manager GET:url parameters:nil success:success failure:failure];
}

+ (void)POST:(NSString*)url parameters:(NSDictionary*)param success:(void (^)(NSURLSessionDataTask *task, id responseObject)) success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    TMNetService* serivce = [TMNetService shared];
    [serivce setHeader];
    NSLog(@"post:::url:%@",url);
    [serivce.manager POST:url parameters:param success:success failure:failure];
}

+ (void)POST:(NSString*)url parameters:(NSDictionary*)param File:(NSArray <NSData*>*)files success:(void (^)(NSURLSessionDataTask *task, id responseObject)) success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    TMNetService* serivce = [TMNetService shared];
    [serivce setHeader];
    NSLog(@"post:::url:%@",url);
    
    [serivce.manager POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (files && [files isKindOfClass:[NSArray class]] && files.count>0) {
            for (int i = 0; i < files.count; i++) {
                NSData*   data = [files objectAtIndex:i];
                NSString* name = [NSString stringWithFormat:@"image[%d]",i];
                [formData appendPartWithFormData:data name:name];
            }
        }
    } success:success failure:failure];
}


#pragma mark - share

+ (id)shared
{
    static TMNetService *instance;
    static dispatch_once_t oncetoken;
    dispatch_once(&oncetoken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self config];
    }
    return self;
}

- (void)config
{
    //    ClientParam* param = [ClientParam getClientParamForiPhone];
    //
    NSURLSessionConfiguration *sessionConfig =[NSURLSessionConfiguration defaultSessionConfiguration];
    //    sessionConfig.timeoutIntervalForRequest = param.timeout;
    
    self.manager =[[AFHTTPSessionManager alloc] initWithSessionConfiguration:sessionConfig];
    
    self.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [self.manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    
    
    self.file_manager =[[AFHTTPSessionManager alloc] initWithSessionConfiguration:sessionConfig];
    
    self.file_manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    self.file_manager.responseSerializer = [AFHTTPResponseSerializer serializer];

}

- (void)setHeader{
    //    ClientParam* param = [ClientParam getClientParamForiPhone];
    //    [self.manager.requestSerializer setValue:param.token forHTTPHeaderField:@"token"];
    //NSLog(@"headerField:%@",self.manager.requestSerializer.HTTPRequestHeaders);
}

@end
