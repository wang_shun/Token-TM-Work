//
//  TMCollectCellLabel.h
//  TokenTM
//
//  Created by wang shun on 2018/7/4.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMCollectCellLabelDelegate ;
@interface TMCollectCellLabel : UIView

@property (nonatomic,weak) id <TMCollectCellLabelDelegate>delegate;

@property (nonatomic,strong) UIImageView* icon;
@property (nonatomic,strong) UILabel* textLabel;

@property (nonatomic,strong) UIButton* btn;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)update:(NSDictionary*)data;

- (void)setImage:(NSString*)imgName;
- (void)setText:(NSString*)text;

@end

@protocol TMCollectCellLabelDelegate <NSObject>

- (void)btnClick:(id)sender;

@end
