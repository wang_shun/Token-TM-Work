//
//  TMCollectListModel.m
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCollectListModel.h"

@interface TMCollectListModel ()
{
    BOOL isPullUploading;
    BOOL isFinished;
}
@property (nonatomic,assign) NSInteger pageIndex;

@end

@implementation TMCollectListModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.mArr = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
 
    if (scrollView.dragging) {
        
    }
    else{
        
        if (scrollView.contentSize.height>scrollView.frame.size.height) {
            if (scrollView.contentOffset.y>=(scrollView.contentSize.height+0-scrollView.bounds.size.height)) {
                [self collectListWith:nil];
            }
        }
    }
}

- (void)collectListWith:(id)sender{
    if (isFinished == YES) {
        return;
    }
    if (isPullUploading) {
        return;
    }
    isPullUploading = YES;
    NSString* url = [NSString stringWithFormat:@"%@/collect/list.json?openId=%@&page=%d&pageSize=6",TMDomain,[TMUserInfo getOpenID],(int)self.pageIndex];
    
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"resp:::%@",responseObject);
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* statusCode = [responseObject objectForKey:@"statusCode"];
            if (statusCode.integerValue ==200) {
                NSDictionary* data = [responseObject objectForKey:@"data"];
                if (data) {
                    NSArray* list = [data objectForKey:@"list"];
                    [self.mArr removeAllObjects];
                    for (NSDictionary* dic in list) {
                        [self.mArr addObject:dic];
                    }
                    
                    if (list.count>6) {
                        self.pageIndex = self.pageIndex +1;
                    }
                    else{
                        isFinished = YES;
                    }
                    isPullUploading = NO;
                    
                    if (self.delegate && [self.delegate respondsToSelector:@selector(refreshCollectList:)]) {
                        [self.delegate refreshCollectList:nil];
                    }
                    return;
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        isPullUploading = NO;
    }];
}


/*
 resp:::{
 data =     {
 collectCount = 3;
 list =         (
 {
 commentNum = 0;
 docId = "71fe05088bcbe5ea-c5edf94a59c8d1f7";
 history = 0;
 likeValue = 0;
 newsId = 2285121716155842560;
 newsType = 0;
 pics =                 (
 "https://h5uid.net/7DB5B874E1A3842D75C368CF960DBF8C.jpg"
 );
 publishTime = 1530179154000;
 readTimes = 1;
 recomTime = 0;
 shareImage = "https://h5uid.net/7DB5B874E1A3842D75C368CF960DBF8C.jpg";
 shareNum = 0;
 templateType = 1;
 time = "2018-06-28 17:45";
 title = "\U5e72\U8d27\Uff1a\U533a\U5757\U94fe\U9879\U76ee\U7684\U5206\U7c7b\U548c\U5e94\U7528";
 useful = 0;
 useless = 0;
 userInfo =                 {
 avatarUrl = "http://img.dj.gl/token_default_icon.jpg";
 gender = 0;
 nickName = "Token.TM";
 type = 0;
 };
 },
 {
 commentNum = 0;
 docId = "3804e13d97f551fe-744960aec6bdba0a";
 history = 0;
 likeValue = 0;
 newsId = 2285152639920373760;
 newsType = 0;
 pics =                 (
 "https://h5uid.net/D7E31A1B8A725B2C40F2D2226EDE0FBF.png",
 "https://h5uid.net/72A06CE0DDE971F05BEAC423F71AF930.png",
 "https://h5uid.net/C40145B7640B0450CB08DEBEC00E7AD1.png"
 );
 publishTime = 1530180286000;
 readTimes = 1;
 recomTime = 0;
 shareImage = "https://h5uid.net/D7E31A1B8A725B2C40F2D2226EDE0FBF.png";
 shareNum = 0;
 templateType = 1;
 time = "2018-06-28 18:04";
 title = "Bitcoin Price Defends $6K as Traders Go Long";
 useful = 0;
 useless = 0;
 userInfo =                 {
 avatarUrl = "http://img.dj.gl/token_default_icon.jpg";
 gender = 0;
 nickName = "Token.TM";
 type = 0;
 };
 zhTitle = "\U6bd4\U7279\U5e01\U4ef7\U683c\U7ef4\U63016K\Uff0c\U4ea4\U6613\U8005\U8d70\U957f\U8def";
 },
 {
 commentNum = 0;
 docId = "f8b1e088eb79269f-efae29988e7a240c";
 history = 0;
 likeValue = 0;
 newsId = 2287445808859054080;
 newsType = 0;
 pics =                 (
 "https://h5uid.net/4C73280EECD51FCDC085B150B4BB9741.jpg",
 "https://h5uid.net/7D274D440434E6910256B60345FA7068.png",
 "https://h5uid.net/8B147554183F4DE289681DB8F92B9AC7.png"
 );
 publishTime = 1530246002000;
 readTimes = 1;
 recomTime = 0;
 shareImage = "https://h5uid.net/4C73280EECD51FCDC085B150B4BB9741.jpg";
 shareNum = 0;
 templateType = 1;
 time = "2018-06-29 12:20";
 title = "\U4e2d\U949e\U5f20\U4e00\U950b\Uff1a\U533a\U5757\U94fe\U5bf9\U63a5\U73b0\U5b9e\U4e16\U754c\Uff0c\U5fc5\U987b\U8981\U548c\U8eab\U4efd\U5173\U8054";
 useful = 0;
 useless = 0;
 userInfo =                 {
 avatarUrl = "http://img.dj.gl/token_default_icon.jpg";
 gender = 0;
 nickName = "Token.TM";
 type = 0;
 };
 }
 );
 };
 statusCode = 200;
 }

 */

@end
