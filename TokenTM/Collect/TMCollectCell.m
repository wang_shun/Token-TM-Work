//
//  TMCollectCell.m
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCollectCell.h"
#import "UIImageView+WebCache.h"

#import "TMCollectCellLabel.h"
#import "WeiXinObject.h"

@interface TMCollectCell ()<TMCollectCellLabelDelegate>

@property (nonatomic,strong) UIImageView* headerView;
@property (nonatomic,strong) UILabel* title_Label;
@property (nonatomic,strong) UILabel* time_Label;
@property (nonatomic,strong) UIView*  bline;

@property (nonatomic,strong) TMCollectCellLabel*  comment;
@property (nonatomic,strong) TMCollectCellLabel*  reader;
@property (nonatomic,strong) TMCollectCellLabel*  share;

@end

@implementation TMCollectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Rect:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.cell_rect = rect;
        [self createView];
    }
    return self;
}

-(void)updateData:(id)data{
    if (data) {
        if ([data isKindOfClass:[NSDictionary class]]) {
            self.newsInfo = (NSDictionary*)data;
            
            NSArray* pics = [self.newsInfo objectForKey:@"pics"];
            if (pics && [pics isKindOfClass:[NSArray class]]){
                if (pics.count>0) {
                    NSString*pic = [pics firstObject];
                    [self.headerView sd_setImageWithURL:[NSURL URLWithString:pic] placeholderImage:nil];
                    self.headerView.hidden = NO;
                }
                else{
                    [self.headerView setImage:nil];
                    self.headerView.hidden = YES;
 
                }
            }
            else{
                [self.headerView setImage:nil];
                self.headerView.hidden = YES;
            }
            
            NSString* title = [self.newsInfo objectForKey:@"title"];
            CGFloat w = CGRectGetMinX(self.headerView.frame)-13-10;
            if (self.headerView.hidden == YES) {
                w = self.cell_rect.size.width-13-13;
            }
            CGRect r = [title boundingRectWithSize:CGSizeMake(w, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]} context:nil];
            
            if (r.size.height>60) {
                [self.title_Label setFrame:CGRectMake(13, 7, w, 60)];
            }
            else{
                [self.title_Label setFrame:CGRectMake(13, 7, w, r.size.height)];
            }
            [self.title_Label setText:title];
            self.title_Label.lineBreakMode = NSLineBreakByTruncatingTail;
            
            NSString* time = [self.newsInfo objectForKey:@"time"];
            [self.time_Label setText:time];
            [self.time_Label setFrame:CGRectMake(13, self.headerView.frame.origin.y+self.headerView.frame.size.height-20, self.title_Label.frame.size.width, 15)];
            
            NSNumber* readTimes = [self.newsInfo objectForKey:@"readTimes"];
            NSString* read_str = [NSString stringWithFormat:@"%@",readTimes];
            [self.reader setText:read_str];
            
        }
    }
}

- (void)createView{
    self.headerView = [[UIImageView alloc] initWithFrame:CGRectMake(self.cell_rect.size.width-125-13, 7, 125, 80)];
    [self.headerView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:self.headerView];
    
    self.title_Label = [[UILabel alloc] initWithFrame:CGRectMake(13, 7, CGRectGetMinX(self.headerView.frame)-13-10, 80)];
    [self.title_Label setText:@"动手了，美国税务局对比特币启动征税！"];
    [self.title_Label setFont:[UIFont systemFontOfSize:16]];
    self.title_Label.numberOfLines = 0;
    [self.contentView addSubview:self.title_Label];
    
    self.time_Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.time_Label setFont:[UIFont systemFontOfSize:13]];
    [self.time_Label setTextColor:RGBACOLOR(107, 107, 107, 1)];
    [self.contentView addSubview:self.time_Label];
    
//    self.line = [[UIView alloc] initWithFrame:CGRectMake(13, 95, self.cell_rect.size.width-13-13, TMOnePixel)];
//    [self.line setBackgroundColor:RGBACOLOR(217, 217, 217, 1)];
//    [self.contentView addSubview:self.line];
//    self.line.hidden = YES;
    
    self.bline = [[UIView alloc] initWithFrame:CGRectMake(0, 116-2, self.cell_rect.size.width, 2)];
    [self.bline setBackgroundColor:RGBACOLOR(244, 244, 244, 1)];
    [self.contentView addSubview:self.bline];
    
    
    
    CGFloat y = 116-25;
    self.share = [[TMCollectCellLabel alloc] initWithFrame:CGRectMake(self.cell_rect.size.width-13-50, y, 50, 20)];
    [self.share setImage:@"cell_share"];
    [self.share setText:@"分享"];
    [self.contentView addSubview:self.share];
    self.share.delegate = self;
    
    self.comment = [[TMCollectCellLabel alloc] initWithFrame:CGRectMake(13, y, self.cell_rect.size.width/3, 20)];
    [self.comment setImage:@"cell_comment"];
    [self.comment setText:@"评论"];
    [self.contentView addSubview:self.comment];
    self.comment.delegate = self;
    
    self.reader = [[TMCollectCellLabel alloc] initWithFrame:CGRectMake(13+self.comment.frame.size.width,  y, self.cell_rect.size.width/3, 20)];
    [self.reader setImage:@"cell_read"];
    [self.contentView addSubview:self.reader];
    self.reader.delegate = self;

}

-(void)btnClick:(id)sender{
    if ([sender isEqual:self.share]) {
        if (self.newsInfo) {
            NSString* title = [self.newsInfo objectForKey:@"title"];
            NSString* newsId = [self.newsInfo objectForKey:@"newsId"];
            NSString* shareUrl = [NSString stringWithFormat:@"http://token.tm/a/%@",newsId];
            
            NSMutableDictionary* shareDic = [[NSMutableDictionary alloc] initWithCapacity:0];
            [shareDic setObject:shareUrl forKey:@"shareUrl"];
            [shareDic setObject:title?:@"" forKey:@"shareTitle"];
            [shareDic setObject:@"" forKey:@"sharePic"];
            
            [WeiXinObject share:shareDic Completion:^(NSDictionary *resp) {
                
            }];
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
