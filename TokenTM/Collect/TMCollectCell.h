//
//  TMCollectCell.h
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCollectCell : UITableViewCell

@property (nonatomic,assign) CGRect cell_rect;

@property (nonatomic,strong) NSDictionary* newsInfo;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Rect:(CGRect)rect;

- (void)updateData:(id)data;

@end
