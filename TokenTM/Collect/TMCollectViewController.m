//
//  TMCollectViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCollectViewController.h"

#import "TMCollectListModel.h"

#import "TMCollectCell.h"

#import "TMNewsDetailViewController.h"

@interface TMCollectViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,TMCollectListModelDelegate>

@property (nonatomic,strong) UITableView* table_View;

@property (nonatomic,strong) TMCollectListModel* collectList;


@end

@implementation TMCollectViewController

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGBACOLOR(244, 244, 244, 1);
    self.title = @"我的收藏";
    
    [self createTable];
    
    [self createNavigationBar];
    
    self.collectList = [[TMCollectListModel alloc] init];
    self.collectList.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.collectList collectListWith:nil];
}

- (void)createTable{
    _table_View = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), self.view.bounds.size.width, self.view.bounds.size.height-CGRectGetMaxY(self.navigationBar.frame)) style:UITableViewStylePlain];
    
    _table_View.delegate = self;
    _table_View.dataSource = self;
    
    _table_View.backgroundColor = [UIColor clearColor];
    
    _table_View.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_table_View];
    
    UIView* v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 2)];
    v.backgroundColor = RGBACOLOR(244, 244, 244, 1);
    _table_View.tableHeaderView = v;
}


#pragma mark - TableViewDelegate & TableViewDataSource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* str = @"TMCollectCell";
    
    TMCollectCell* cell = [tableView dequeueReusableCellWithIdentifier:str];
    
    if (cell == nil) {
        cell = [[TMCollectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str Rect:self.view.bounds];
    }
    
    id data =  [self.collectList.mArr objectAtIndex:indexPath.row];
    [cell updateData:data];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.collectList.mArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 116;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* newsInfo = [self.collectList.mArr objectAtIndex:indexPath.row];
    NSString* newsId = [newsInfo objectForKey:@"newsId"];
    
    TMNewsDetailViewController* vc = [[TMNewsDetailViewController alloc] initWithNewsId:newsId];
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.collectList scrollViewDidScroll:scrollView];
}

#pragma mark - TMCollectListModelDelegate

-(void)refreshCollectList:(id)sender{
    [self.table_View reloadData];
}

#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
