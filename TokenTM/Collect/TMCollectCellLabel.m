//
//  TMCollectCellLabel.m
//  TokenTM
//
//  Created by wang shun on 2018/7/4.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCollectCellLabel.h"

@implementation TMCollectCellLabel

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

- (void)update:(NSDictionary*)data{
    
}

-(void)setImage:(NSString *)imgName{
    if (imgName) {
        [self.icon setImage:[UIImage imageNamed:imgName]];
    }
}

- (void)setText:(NSString*)text{
    if (text) {
        [self.textLabel setText:text];
    }
}

- (void)createView{
    self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 3, 14, 14)];
    [self.icon setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.icon];
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.icon.frame.size.width+5, 3, self.bounds.size.width-(self.icon.frame.size.width+5), 14)];
    [self.textLabel setTextColor:RGBACOLOR(107, 107, 107, 1)];
    [self.textLabel setFont:[UIFont systemFontOfSize:11]];
    [self addSubview:self.textLabel];
    
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn setFrame:self.bounds];
    [self addSubview:self.btn];
    [self.btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)btnClick:(UIButton*)b{
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnClick:)]) {
        [self.delegate btnClick:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
