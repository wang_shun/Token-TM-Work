//
//  TMCollectListModel.h
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol TMCollectListModelDelegate;
@interface TMCollectListModel : NSObject

@property (nonatomic,strong) NSMutableArray* mArr;

@property (nonatomic,weak) id <TMCollectListModelDelegate>delegate;

- (void)collectListWith:(id)sender;

-(void)scrollViewDidScroll:(UIScrollView *)scrollView;

@end


@protocol TMCollectListModelDelegate <NSObject>

- (void)refreshCollectList:(id)sender;

@end
