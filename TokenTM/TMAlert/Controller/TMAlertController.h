//
//  TMAlertController.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"
#import "TMCoinPriceModel.h"
#import "TMAlertModel.h"

@interface TMAlertController : TMBaseViewController

@property (nonatomic,strong) TMCoinPriceModel *priceModel;
@property (nonatomic,strong) TMAlertModel *alertModel;

-(void)changeSettingRequest;

@end
