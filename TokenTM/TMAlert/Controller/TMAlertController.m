//
//  TMCoinNoticeController.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMAlertController.h"
#import "TMAlertHeadView.h"
#import "TMWaveView.h"
#import "TMMaxView.h"

@interface TMAlertController ()

@property (nonatomic,strong) TMAlertHeadView *headView;
@property (nonatomic,strong) TMWaveView *waveView;
@property (nonatomic,strong) TMMaxView *maxView;

@end

@implementation TMAlertController

- (void)viewDidLoad {
    [super viewDidLoad];
//    https://dj.gl/tokentm/v1/alert/info.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&coinName=btc

//    https://dj.gl/tokentm/v1/alert/form_id.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&formId=8e03fd6eeacbe306b4fe5af3d71a9130
    
    self.title = @"预警提醒";
    [self createNavigationBar];
    [self initViews];
    
    __weak typeof(self) weakSelf = self;
    
    NSString* url = [NSString stringWithFormat:@"%@/alert/info.json?openId=%@&coinName=%@",TMDomain,TokenId,self.priceModel.coinName.lowercaseString];
    //NSString *url = @"https://dj.gl/tokentm/v1/info/btc/brief.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o"
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"status"];
            if (status.integerValue == 200) {
                NSDictionary* dataDic = [responseObject objectForKey:@"data"];
                
                weakSelf.alertModel = [TMAlertModel briefModelFromDictionary:dataDic];
                if (weakSelf.alertModel) {
                    weakSelf.alertModel.fullName = self.priceModel.coinName;
                    weakSelf.headView.mainModel = weakSelf.alertModel;
                    weakSelf.maxView.mainModel = weakSelf.alertModel;
                    weakSelf.waveView.mainModel = weakSelf.alertModel;
                }
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

- (void)initViews {
    self.headView = [[TMAlertHeadView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), Screen_Width, 100)];
    //self.headView.mainModel = self.priceModel;
    [self.view addSubview:self.headView];
    [self.view addSubview:self.maxView];
    //[self.view addSubview:self.waveView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeSettingRequest{
    
    //    https://dj.gl/tokentm/v1/alert/setting.json?limitSwitch=1&maxPrice=59064.03&minPrice=48325.11&openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o&price=42570.97&ratioLevel=2&ratioSwitch=0&register=true&smsSwitch=0&coinName=btc
    
    NSString *limitSwitch = @"0";
    NSString *maxPrice = self.alertModel.maxPrice;
    NSString *minPrice = self.alertModel.minPrice;
    NSString *price = self.alertModel.price;
    NSString *ratioLevel = [self.maxView getRatioLevel];
    NSString *ratioSwitch = [self.maxView getRatioLevelStatus];

    
    NSString* url = [NSString stringWithFormat:@"%@/alert/setting.json?limitSwitch=%@&maxPrice=%@&minPrice=%@&openId=%@&price=%@&ratioLevel=%@&ratioSwitch=%@&register=true&smsSwitch=0&coinName=%@",TMDomain,limitSwitch,maxPrice,minPrice,TokenId,price,ratioLevel,ratioSwitch,self.priceModel.coinName.lowercaseString];
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"status"];
            if (status.integerValue == 200) {
                
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(TMWaveView *)waveView{
    if (!_waveView) {
        _waveView = [[TMWaveView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_waveView.frame), Screen_Width, 150)];
    }
    return _waveView;
}

-(TMMaxView *)maxView{
    if (!_maxView) {
        _maxView = [[TMMaxView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_headView.frame), Screen_Width, 185)];
    }
    return _maxView;
}

@end
