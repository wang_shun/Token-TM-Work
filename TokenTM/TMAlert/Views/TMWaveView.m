//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMWaveView.h"

@interface TMWaveView ()

@property (nonatomic,strong) UITextField* leftField;
@property (nonatomic,strong) UITextField* rightField;


@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;


@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMWaveView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{

    self.leftField.keyboardType = UIKeyboardTypeNumberPad;
    
    [self addSubview:self.leftField];
    [self addSubview:self.rightField];
    
    [self addSubview:self.rightLbl];
    [self addSubview:self.leftLbl];
}


- (void)setMainModel:(TMAlertModel *)mainModel{
    _mainModel = mainModel;
    
    _leftLbl.text = mainModel.fullName;
    
    _rightLbl.text = mainModel.fullName;
    
    [_leftLbl sizeToFit];
    [_rightLbl sizeToFit];
    
    
    
    _leftLbl.frame = CGRectMake(25, 25, _leftLbl.frame.size.width, _leftLbl.frame.size.height);
    _rightLbl.frame = CGRectMake(25, CGRectGetMaxY(_leftLbl.frame)+5, _rightLbl.frame.size.width, _rightLbl.frame.size.height);
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _rightLbl.textColor = Global_Brown_Color;
        _rightLbl.font = [UIFont boldSystemFontOfSize:13];
    }
    return _rightLbl;
}

-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftLbl.textColor = Global_Brown_Color;
        _leftLbl.font = [UIFont boldSystemFontOfSize:16];
    }
    return _leftLbl;
}




@end
