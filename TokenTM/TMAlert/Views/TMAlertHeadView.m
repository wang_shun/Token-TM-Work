//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMAlertHeadView.h"

@interface TMAlertHeadView ()


@property (nonatomic,strong) UIView* leftView;

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;

@property (nonatomic,strong) UILabel* priceLbl;

@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMAlertHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.leftView];

    [self addSubview:self.rightLbl];
    [self addSubview:self.leftLbl];
    [self addSubview:self.priceLbl];
}


-(void)setMainModel:(TMAlertModel *)mainModel{
    _mainModel = mainModel;
    
    _leftLbl.text = mainModel.fullName;
    
    _rightLbl.text = mainModel.fullName;
    
    _priceLbl.text = mainModel.price;
    
    [_leftLbl sizeToFit];
    [_rightLbl sizeToFit];
    [_priceLbl sizeToFit];
    
    _leftView.frame = CGRectMake(0, 0, 5, self.viewRect.size.height);
    
    _leftLbl.frame = CGRectMake(25, 25, _leftLbl.frame.size.width, _leftLbl.frame.size.height);
    _rightLbl.frame = CGRectMake(25, CGRectGetMaxY(_leftLbl.frame)+5, _rightLbl.frame.size.width, _rightLbl.frame.size.height);
    _priceLbl.frame = CGRectMake(Screen_Width-_priceLbl.frame.size.width-12, CGRectGetMinY(_rightLbl.frame), _priceLbl.frame.size.width, _priceLbl.frame.size.height);
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _rightLbl.textColor = Global_Brown_Color;
        _rightLbl.font = [UIFont boldSystemFontOfSize:13];
    }
    return _rightLbl;
}

-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftLbl.textColor = Global_Brown_Color;
        _leftLbl.font = [UIFont boldSystemFontOfSize:16];
    }
    return _leftLbl;
}

-(UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _priceLbl.textColor = Global_Brown_Color;
        _priceLbl.font = [UIFont boldSystemFontOfSize:16];
    }
    return _priceLbl;
}

-(UIView *)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftView.backgroundColor = Global_Brown_Color;
    }
    return _leftView;
}



@end
