//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMMaxView.h"

@interface TMMaxView ()


@property (nonatomic,strong) NSString* status;

@property (nonatomic,strong) UISwitch* waveSwitch;

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;

@property (nonatomic,strong) UILabel* twoLbl;
@property (nonatomic,strong) UILabel* fiveLbl;
@property (nonatomic,strong) UILabel* tenLbl;

@property (nonatomic,strong) UIButton* twoBtn;
@property (nonatomic,strong) UIButton* fiveBtn;
@property (nonatomic,strong) UIButton* tenBtn;

@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMMaxView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.waveSwitch];

    [self addSubview:self.rightLbl];
    [self addSubview:self.leftLbl];
    [self addSubview:self.twoBtn];
    [self addSubview:self.twoLbl];
    [self addSubview:self.fiveBtn];
    [self addSubview:self.fiveLbl];
    [self addSubview:self.tenBtn];
    [self addSubview:self.tenLbl];
    
}


-(void)setMainModel:(TMAlertModel *)mainModel{
    _mainModel = mainModel;

    
    [_leftLbl sizeToFit];
    [_rightLbl sizeToFit];
    [_twoLbl sizeToFit];
    [_fiveLbl sizeToFit];
    [_tenLbl sizeToFit];
    
    _waveSwitch.on = mainModel.ratioSwitch;
    
    _leftLbl.frame = CGRectMake(12, 30, _leftLbl.frame.size.width, _leftLbl.frame.size.height);
    _waveSwitch.frame = CGRectMake(Screen_Width - 12 - 54, CGRectGetMinY(_leftLbl.frame)-3, 54, 32);
    _rightLbl.frame = CGRectMake(12, self.viewRect.size.height - 24 - _rightLbl.frame.size.height, _rightLbl.frame.size.width, _rightLbl.frame.size.height);
    _twoBtn.frame = CGRectMake(12, CGRectGetMaxY(_leftLbl.frame)+35, 24, 24);
    _twoLbl.frame = CGRectMake(CGRectGetMaxX(_twoBtn.frame)+5, CGRectGetMaxY(_leftLbl.frame)+35, _twoLbl.frame.size.width, _twoLbl.frame.size.height);
    _fiveBtn.frame = CGRectMake(Screen_Width/2 - 12 - 8, CGRectGetMaxY(_leftLbl.frame)+35, 24, 24);
    
    _fiveLbl.frame = CGRectMake(CGRectGetMaxX(_fiveBtn.frame)+5, CGRectGetMinY(_twoLbl.frame), _fiveLbl.frame.size.width, _fiveLbl.frame.size.height);
    _tenLbl.frame = CGRectMake(Screen_Width - 12 - _tenLbl.frame.size.width , CGRectGetMinY(_twoLbl.frame), _tenLbl.frame.size.width, _tenLbl.frame.size.height);
    _tenBtn.frame = CGRectMake(CGRectGetMinX(_tenLbl.frame)-24-5, CGRectGetMaxY(_leftLbl.frame)+35, 24, 24);
    
    switch (mainModel.ratioLevel.integerValue) {
        case 1:
            self.twoBtn.selected = YES;
            break;
        case 2:
            self.fiveBtn.selected = YES;
            break;
        case 3:
            self.tenBtn.selected = YES;
            break;
        default:
            break;
    }
}

- (void)waveSwitchChanged:(UISwitch *)sender{
    if (sender.on) {
        
    }else{
        
    }
}

-(NSString *)getRatioLevelStatus{
    return self.status;
}

-(NSString *)getRatioLevel{
    return self.waveSwitch.on ? @"1" : @"0";
}

-(void)twoBtnTaped{
    if (self.twoBtn.selected) {
        return;
    }else{
        self.status = @"1";
        self.twoBtn.selected = YES;
        self.fiveBtn.selected = NO;
        self.tenBtn.selected = NO;
        if (self.waveSwitch.on) {
            [self.delegate changeSettingRequest];
        }
    }
}

-(void)fiveBtnTaped{
    if (self.fiveBtn.selected) {
        return;
    }else{
        self.status = @"2";
        self.fiveBtn.selected = YES;
        self.twoBtn.selected = NO;
        self.tenBtn.selected = NO;
        if (self.waveSwitch.on) {
            [self.delegate changeSettingRequest];
        }
    }
}

-(void)tenBtnTaped{
    if (self.tenBtn.selected) {
        return;
    }else{
        self.status = @"3";
        self.tenBtn.selected = YES;
        self.twoBtn.selected = NO;
        self.fiveBtn.selected = NO;
        if (self.waveSwitch.on) {
            [self.delegate changeSettingRequest];
        }
    }
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 53)];
        _rightLbl.textColor = GRAY(0x99);
        _rightLbl.text = @"市场价格在单位时间内的波动率超过设定值，系统自动触发报警提醒";
        _rightLbl.font = [UIFont boldSystemFontOfSize:13];
        _rightLbl.numberOfLines = 0;
    }
    return _rightLbl;
}

-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftLbl.textColor = GRAY(0x33);
        _leftLbl.text = @"波动提醒";
        _leftLbl.font = [UIFont boldSystemFontOfSize:16];
    }
    return _leftLbl;
}

-(UILabel *)twoLbl{
    if (!_twoLbl) {
        _twoLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _twoLbl.textColor = GRAY(0x33);
        _twoLbl.text = @"±2%";
        _twoLbl.font = [UIFont systemFontOfSize:16];
    }
    return _twoLbl;
}

-(UILabel *)fiveLbl{
    if (!_fiveLbl) {
        _fiveLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _fiveLbl.textColor = GRAY(0x33);
        _fiveLbl.text = @"±5%";
        _fiveLbl.font = [UIFont systemFontOfSize:16];
    }
    return _fiveLbl;
}

-(UILabel *)tenLbl{
    if (!_tenLbl) {
        _tenLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _tenLbl.textColor = GRAY(0x33);
        _tenLbl.text = @"±10%";
        _tenLbl.font = [UIFont systemFontOfSize:16];
    }
    return _tenLbl;
}

- (UIButton *)twoBtn{
    if (!_twoBtn) {
        _twoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_twoBtn setImage:[UIImage imageNamed:@"wave_percent_select"]  forState:UIControlStateSelected];
        _twoBtn.layer.masksToBounds = YES;
        _twoBtn.layer.cornerRadius = 12;
        [_twoBtn addTarget:self action:@selector(twoBtnTaped) forControlEvents:UIControlEventTouchUpInside];
        _twoBtn.layer.borderColor = GRAY(0x99).CGColor;
        _twoBtn.layer.borderWidth = TMOnePixel;
        
    }
    return _twoBtn;
}

- (UIButton *)fiveBtn{
    if (!_fiveBtn) {
        _fiveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fiveBtn setImage:[UIImage imageNamed:@"wave_percent_select"]  forState:UIControlStateSelected];
        _fiveBtn.layer.masksToBounds = YES;
        _fiveBtn.layer.cornerRadius = 12;
        [_fiveBtn addTarget:self action:@selector(fiveBtnTaped) forControlEvents:UIControlEventTouchUpInside];
        _fiveBtn.layer.borderColor = GRAY(0x99).CGColor;
        _fiveBtn.layer.borderWidth = TMOnePixel;
        
    }
    return _fiveBtn;
}

- (UIButton *)tenBtn{
    if (!_tenBtn) {
        _tenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tenBtn setImage:[UIImage imageNamed:@"wave_percent_select"]  forState:UIControlStateSelected];
        _tenBtn.layer.masksToBounds = YES;
        _tenBtn.layer.cornerRadius = 12;
        [_tenBtn addTarget:self action:@selector(tenBtnTaped) forControlEvents:UIControlEventTouchUpInside];
        _tenBtn.layer.borderColor = GRAY(0x99).CGColor;
        _tenBtn.layer.borderWidth = TMOnePixel;
        
    }
    return _tenBtn;
}

-(UISwitch *)waveSwitch{
    if (!_waveSwitch) {
        _waveSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _waveSwitch.onTintColor = Global_Brown_Color;
        [_waveSwitch addTarget:self action:@selector(waveSwitchChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _waveSwitch;
}



@end
