//
//  TMHomeHeadInfoView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMAlertController.h"
#import "TMAlertModel.h"
@interface TMMaxView : UIView

@property (nonatomic,strong) TMAlertModel *mainModel;
@property (nonatomic,weak) TMAlertController *delegate;

-(NSString *)getRatioLevelStatus;
-(NSString *)getRatioLevel;

@end
