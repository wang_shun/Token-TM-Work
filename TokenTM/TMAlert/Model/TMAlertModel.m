//
//  TMBriefModel.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMAlertModel.h"

@implementation TMAlertModel

+ (TMAlertModel *)briefModelFromDictionary:(NSDictionary *)dic{
    
    TMAlertModel *model = [[TMAlertModel alloc]init];
    
    model.coinName = dic[@"coinName"] ?:@"";
    
    id ratioSwitch = [NSString stringWithFormat:@"%@",dic[@"ratioSwitch"]?:@""];
    id smsSwitch = [NSString stringWithFormat:@"%@",dic[@"smsSwitch"]?:@""];
    id is_register = [NSString stringWithFormat:@"%@",dic[@"register"]?:@""];
    id limitSwitch = [NSString stringWithFormat:@"%@",dic[@"limitSwitch"]?:@""];
    
    model.ratioSwitch = ([ratioSwitch integerValue] == 1);
    model.smsSwitch = ([smsSwitch integerValue] == 1);
    model.is_register = ([is_register integerValue] == 1);
    model.limitSwitch = ([limitSwitch integerValue] == 1);
    
    model.maxPrice = dic[@"maxPrice"] ?:@"";
    model.minPrice = dic[@"minPrice"] ?:@"";
    model.openId = dic[@"openId"] ?:@"";
    model.ratioLevel = [NSString stringWithFormat:@"%@",dic[@"ratioLevel"]?:@""];

    
    model.price = [NSString stringWithFormat:@"%@",dic[@"price"]?:@""];


    return model;
}


@end
