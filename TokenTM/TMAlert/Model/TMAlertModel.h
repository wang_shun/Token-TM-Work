//
//  TMBriefModel.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMAlertModel : NSObject

@property (nonatomic,strong) NSString *fullName;
@property (nonatomic,strong) NSString *coinName;

@property (nonatomic,strong) NSString *maxPrice;
@property (nonatomic,strong) NSString *minPrice;
@property (nonatomic,strong) NSString *openId;

@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *ratioLevel;
@property (nonatomic,assign) BOOL ratioSwitch;
@property (nonatomic,assign) BOOL smsSwitch;
@property (nonatomic,assign) BOOL is_register;
@property (nonatomic,assign) BOOL limitSwitch;

+ (TMAlertModel *)briefModelFromDictionary:(NSDictionary *)dic;

@end


