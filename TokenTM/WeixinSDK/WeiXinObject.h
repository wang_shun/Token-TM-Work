//
//  WeiXinObject.h
//  TokenTM
//
//  Created by wang shun on 2018/5/27.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

#define WeiXinAPPID @"wx140a39ebd657b1cf"
#define WeiXinAppSecret @"e14434345e2c4ffa0d79091540eaaf69"

/*
 * tm.token.iphone.test  正式版
 * tm.token.iphone       测试版
 */

@interface WeiXinObject : NSObject

- (instancetype)init;

- (BOOL)application:(UIApplication *)application handleOpenURL:(nonnull NSURL *)url;

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation;

+ (void)login:(id)sender Completion:(void (^)(NSDictionary*result))mehtod;

+ (void)share:(NSDictionary*)shareInfo Completion:(void(^)(NSDictionary*resp))method;

@end
