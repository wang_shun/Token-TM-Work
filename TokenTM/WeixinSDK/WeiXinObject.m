//
//  WeiXinObject.m
//  TokenTM
//
//  Created by wang shun on 2018/5/27.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "WeiXinObject.h"

#import "WXApi.h"
#import "AppDelegate.h"
#import "TMUserInfo.h"

@interface WeiXinObject ()<WXApiDelegate>

@property (nonatomic,copy) void (^resultMethod)(NSDictionary* result);
@property (nonatomic,copy) void (^resultShare)(NSDictionary* result);

@end

@implementation WeiXinObject

- (instancetype)init{
    if (self = [super init]) {
        [WXApi registerApp:WeiXinAPPID];
    }
    return self;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return [WXApi handleOpenURL:url delegate:self];
}

-(void)sendAuthRequestCompletion:(void (^)(NSDictionary*result))method
{
    if (![WXApi isWXAppInstalled]) {
        [TMToast showToast:ToastTypeBlank text:@"未安装微信"];
        return;
    }
    
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
    if (method) {
        self.resultMethod = method;
    }
}

+ (void)share:(NSDictionary*)shareInfo Completion:(void(^)(NSDictionary*resp))method{
    WeiXinObject* obj = [WeiXinObject getWeixinObj];
    [obj shareWithData:shareInfo Completion:method];
}

- (void)shareWithData:(NSDictionary*)shareInfo Completion:(void(^)(NSDictionary*resp))method{
    
    if (![WXApi isWXAppInstalled]) {
        [TMToast showToast:ToastTypeBlank text:@"未安装微信"];
        return;
    }
    
    
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = [shareInfo objectForKey:@"shareTitle"];
    message.description = @"描述";//[shareInfo objectForKey:@"description"];
    [message setThumbImage:[UIImage imageNamed:@"TMLogo56.png"]];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    NSString* url = [shareInfo objectForKey:@"shareUrl"];
    ext.webpageUrl = url;
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    
    req.message = message;
    req.scene = WXSceneSession;
    
    [WXApi sendReq:req];
    
    if (method) {
        self.resultShare = method;
    }
}


#pragma mark - WXApiDelegate

- (void)onReq:(BaseReq *)req{
    
}

- (void)onResp:(BaseResp*)resp{
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        int errCode = resp.errCode;
        [self analyzeWeiXinCallBack:errCode];
        
    }
    else if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *authResp = (SendAuthResp *)resp;
        if (authResp.code) {
            [self setURLWithCode:authResp.code];
        }
    }
}

- (void)setURLWithCode:(NSString *)code {
    
    NSString* url = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WeiXinAPPID,WeiXinAppSecret,code];
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSLog(@"resp:::%@",responseObject);
            [TMUserInfo setUserInfo:responseObject];
            if (self.resultMethod) {
                self.resultMethod(nil);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"weixin setURLWithCode :::%@",error);
    }];
}


+ (void)login:(id)sender Completion:(void (^)(NSDictionary *))method{
    WeiXinObject* obj = [WeiXinObject getWeixinObj];
    [obj sendAuthRequestCompletion:method];
}

+ (WeiXinObject*)getWeixinObj{
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    WeiXinObject* obj = [app weixinObj];
    return obj;
}



- (void)analyzeWeiXinCallBack:(int)errCode{
    /*
    WXSuccess           = 0,     成功
    WXErrCodeCommon     = -1,    普通错误类型
    WXErrCodeUserCancel = -2,    用户点击取消并返回
    WXErrCodeSentFail   = -3,    发送失败
    WXErrCodeAuthDeny   = -4,    授权失败
    WXErrCodeUnsupport  = -5,    微信不支持
    */
    
    switch (errCode) {
        case WXSuccess:
        {
            [TMToast showToast:ToastTypeBlank text:@"分享成功"];
        }
            break;
        case WXErrCodeCommon:
        {
            [TMToast showToast:ToastTypeBlank text:@"微信错误"];
        }
            break;
        case WXErrCodeUserCancel:
        {
            [TMToast showToast:ToastTypeBlank text:@"用户点击取消并返回"];
        }
            break;
        case WXErrCodeSentFail:
        {
            [TMToast showToast:ToastTypeBlank text:@"发送失败"];
        }
            break;
        case WXErrCodeAuthDeny:
        {
            [TMToast showToast:ToastTypeBlank text:@"授权失败"];
        }
            break;
        case WXErrCodeUnsupport:
        {
            [TMToast showToast:ToastTypeBlank text:@"微信不支持"];
        }
            break;
            
        default:
            break;
    }
}

@end
