//
//  TMNavigationBar.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMNavigationBar.h"

@interface TMNavigationBar()

@property (nonatomic,strong) UIButton* backBtn;
@property (nonatomic,strong) UILabel*  title_label;

@end

@implementation TMNavigationBar

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self createTitleView];
        [self createbackBtn];
    }
    return self;
}

- (void)createbackBtn{
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backBtn setFrame:CGRectMake(0, self.bounds.size.height-TMNavigationBarHeight, TMNavigationBarHeight, TMNavigationBarHeight)];
    [self.backBtn setImage:[UIImage imageNamed:@"TM_NaviBar_Back"] forState:UIControlStateNormal];
    [self.backBtn setImageEdgeInsets:UIEdgeInsetsMake(20, 11, 0, 43)];//{top, left, bottom, right} 65-11-11
    [self.backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backBtn];
}

- (void)createTitleView{
    _title_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.bounds.size.width, self.bounds.size.height-20)];
    [_title_label setText:@"Token.TM"];
    [_title_label setFont:[UIFont boldSystemFontOfSize:18]];
    [_title_label setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_title_label];
}


- (void)backBtnClick:(UIButton*)b{
    if (self.delegate && [self.delegate respondsToSelector:@selector(naviBack:)]) {
        [self.delegate naviBack:b];
    }
}

-(void)setTitle:(NSString *)title{
    [_title_label setText:title];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
