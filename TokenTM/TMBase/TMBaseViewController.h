//
//  TMBaseViewController.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMNavigationBar.h"

@interface TMBaseViewController : UIViewController

@property (nonatomic,strong) TMNavigationBar* navigationBar;

- (void)createNavigationBar;

- (void)naviBack:(UIButton *)b;

@end
