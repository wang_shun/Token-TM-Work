//
//  TMBaseViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMBaseViewController ()<TMNavigationBarDelegate>

@end

@implementation TMBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    CGFloat h = 0;
    if (IS_IPHONE_X) {
        h = 24;//电池栏+24
    }
    self.navigationBar = [[TMNavigationBar alloc] initWithFrame:CGRectMake(0, h, self.view.bounds.size.width, TMNavigationBarHeight)];
    self.navigationBar.delegate = self;
    [self.view addSubview:self.navigationBar];
    self.navigationBar.hidden = YES;
    
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)createNavigationBar{
    [self.navigationBar setTitle:self.title];
    [self.view bringSubviewToFront:self.navigationBar];
    self.navigationBar.hidden = NO;
}

#pragma mark - TMNavigationBarDelegate

- (void)naviBack:(UIButton *)b{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
