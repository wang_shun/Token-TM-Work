//
//  TMNavigationBar.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TMNavigationBarHeight 65

@protocol TMNavigationBarDelegate;
@interface TMNavigationBar : UIView

@property (nonatomic,weak) id <TMNavigationBarDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setTitle:(NSString*)title;

@end

@protocol TMNavigationBarDelegate<NSObject>

- (void)naviBack:(UIButton*)b;

@end
