//
//  TMCoreData.h
//  TokenTM
//
//  Created by wang shun on 2018/5/9.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

API_AVAILABLE(ios(10.0))
@interface TMCoreData : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

- (void)applicationWillTerminate:(UIApplication *)application ;


@end
