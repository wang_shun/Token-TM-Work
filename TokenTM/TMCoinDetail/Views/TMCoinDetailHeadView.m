//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMCoinDetailHeadView.h"

@interface TMCoinDetailHeadView ()


@property (nonatomic,strong) UIView* leftView;

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;
@property (nonatomic,strong) UILabel* baseLbl;
@property (nonatomic,strong) UILabel* currencyLbl;

@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMCoinDetailHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.leftView];
    [self addSubview:self.baseLbl];
    [self addSubview:self.rightLbl];
    [self addSubview:self.leftLbl];
    [self addSubview:self.currencyLbl];
}


-(void)setMainModel:(TMCoinPriceModel *)mainModel{
    _mainModel = mainModel;
    
    _leftLbl.text = mainModel.fullName;
    
    _rightLbl.text = mainModel.coinName;
    
    [_leftLbl sizeToFit];
    [_rightLbl sizeToFit];
    
    _leftView.frame = CGRectMake(0, 0, 5, self.viewRect.size.height);
    
    _leftLbl.frame = CGRectMake(25, 25, _leftLbl.frame.size.width, _leftLbl.frame.size.height);
    _rightLbl.frame = CGRectMake(25, CGRectGetMaxY(_leftLbl.frame)+5, _rightLbl.frame.size.width, _rightLbl.frame.size.height);
    
    _baseLbl.frame = CGRectMake(self.viewRect.size.width-115, self.viewRect.size.height-20-22, 45, 20);
    _currencyLbl.frame = CGRectMake(CGRectGetMaxX(_baseLbl.frame)+5, CGRectGetMinY(_baseLbl.frame), 45, 20);
    
}

-(UILabel *)baseLbl{
    if (!_baseLbl) {
        _baseLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 45, 20)];
        _baseLbl.textColor = Global_Brown_Color;
        _baseLbl.font = [UIFont boldSystemFontOfSize:12];
        _baseLbl.text = @"基础链";
        _baseLbl.layer.masksToBounds = YES;
        _baseLbl.layer.cornerRadius = 2;
        _baseLbl.layer.borderColor = Global_Brown_Color.CGColor;
        _baseLbl.layer.borderWidth = TMOnePixel;
        _baseLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _baseLbl;
}

-(UILabel *)currencyLbl{
    if (!_currencyLbl) {
        _currencyLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 45, 20)];
        _currencyLbl.textColor = Global_Brown_Color;
        _currencyLbl.font = [UIFont boldSystemFontOfSize:12];
        _currencyLbl.text = @"货币";
        _currencyLbl.layer.masksToBounds = YES;
        _currencyLbl.layer.cornerRadius = 2;
        _currencyLbl.layer.borderColor = Global_Brown_Color.CGColor;
        _currencyLbl.layer.borderWidth = TMOnePixel;
        _currencyLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _currencyLbl;
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _rightLbl.textColor = Global_Brown_Color;
        _rightLbl.font = [UIFont boldSystemFontOfSize:13];
    }
    return _rightLbl;
}

-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftLbl.textColor = Global_Brown_Color;
        _leftLbl.font = [UIFont boldSystemFontOfSize:16];
    }
    return _leftLbl;
}

-(UIView *)leftView{
    if (!_leftView) {
        _leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
        _leftView.backgroundColor = Global_Brown_Color;
    }
    return _leftView;
}



@end
