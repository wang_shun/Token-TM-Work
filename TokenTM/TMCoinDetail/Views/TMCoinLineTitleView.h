//
//  TMHomeHeadInfoView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCoinLineTitleView : UIView

@property (nonatomic,strong) NSString *title;

@end
