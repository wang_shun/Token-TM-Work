//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMCoinLineTitleView.h"

@interface TMCoinLineTitleView ()

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UIView* leftLine;
@property (nonatomic,strong) UIView* rightLine;
@property(nonatomic,assign) CGRect viewRect;
@property(nonatomic,assign) CGFloat lineWidth;

@end

@implementation TMCoinLineTitleView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    self.lineWidth = (Screen_Width-40-65)/2;
    [self addSubview:self.leftLbl];
    [self addSubview:self.leftLine];
    [self addSubview:self.rightLine];
}

-(void)setTitle:(NSString *)title{
    _title = title;
    
    _leftLbl.text = title;
    _rightLine.frame = CGRectMake(CGRectGetMaxX(_leftLine.frame)+65, self.viewRect.size.height/2, self.lineWidth, TMOnePixel);
}

-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, self.viewRect.size.height)];
        _leftLbl.textColor = Global_Brown_Color;
        _leftLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _leftLbl;
}

-(UIView *)leftLine{
    if (!_leftLine) {
        _leftLine = [[UILabel alloc]initWithFrame:CGRectMake(20, self.viewRect.size.height/2, self.lineWidth, TMOnePixel)];
        _leftLine.backgroundColor = Global_Brown_Color;
    }
    return _leftLine;
}

-(UIView *)rightLine{
    if (!_rightLine) {
        _rightLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.viewRect.size.height/2, self.lineWidth, TMOnePixel)];
        _rightLine.backgroundColor = Global_Brown_Color;
    }
    return _rightLine;
}


@end
