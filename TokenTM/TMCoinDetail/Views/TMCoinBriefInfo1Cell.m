//
//  TMCoinBriefInfoCell.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinBriefInfo1Cell.h"
#import "TMCoinLineTitleView.h"

@interface TMCoinBriefInfo1Cell ()

@property (nonatomic,strong) TMCoinLineTitleView* titleView;
@property (nonatomic,strong) UIView* lineView;
@property (nonatomic,strong) UILabel* rightLbl;

@end

@implementation TMCoinBriefInfo1Cell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.titleView];
    [self addSubview:self.rightLbl];
    [self addSubview:self.lineView];
}

-(void)setBrief:(NSString *)brief{
    
    _brief = brief;
    
    self.rightLbl.text = brief;
    
    [self.rightLbl sizeToFit];
    
    self.rightLbl.frame = CGRectMake(20, CGRectGetMaxY(self.titleView.frame), Screen_Width-20*2, self.rightLbl.frame.size.height);
    self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.rightLbl.frame)+20, Screen_Width, 5);
}


-(TMCoinLineTitleView *)titleView{
    if (!_titleView) {
        _titleView = [[TMCoinLineTitleView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
        _titleView.backgroundColor = [UIColor whiteColor];
        _titleView.tag = 111;
        _titleView.title = @"简介";
    }
    return _titleView;
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-20*2, 53)];
        _rightLbl.textColor = GRAY(0x39);
        _rightLbl.font = [UIFont systemFontOfSize:14];
        _rightLbl.numberOfLines = 0;
    }
    return _rightLbl;
}

-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-20*2, 5)];
        _lineView.backgroundColor = GRAY(0xf4);
    }
    return _lineView;
}



@end
