//
//  TMCoinBriefInfoCell.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCoinBriefInfo1Cell : UITableViewCell

@property (nonatomic,strong) NSString *brief;

@end
