//
//  TMCoinBriefInfoCell.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBriefModel.h"

@interface TMCoinBriefInfo2Cell : UITableViewCell

@property(nonatomic,strong) TMBriefModel *mainModel;

@end
