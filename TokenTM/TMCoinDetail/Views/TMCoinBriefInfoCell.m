//
//  TMCoinBriefInfoCell.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinBriefInfoCell.h"
@interface TMCoinBriefInfoCell ()

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;
@property (nonatomic,strong) UIView* rightLine;
@end

@implementation TMCoinBriefInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.leftLbl];
    [self addSubview:self.rightLbl];
    [self addSubview:self.rightLine];
}

-(void)setData:(NSDictionary *)data{
    _data  = data;
    
    self.leftLbl.text = [NSString stringWithFormat:@"%@",[data allKeys][0]] ;
    self.rightLbl.text = [NSString stringWithFormat:@"%@",[data allValues][0]];
    
    [self.leftLbl sizeToFit];
    [self.rightLbl sizeToFit];
    
    self.leftLbl.frame = CGRectMake(20, 0, self.leftLbl.frame.size.width, 53);
    self.rightLbl.frame = CGRectMake(Screen_Width-self.rightLbl.frame.size.width-20, 0, self.rightLbl.frame.size.width, 53);
    self.rightLine.frame = CGRectMake(7, 53-TMOnePixel, Screen_Width-14, TMOnePixel);
}


-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
    }
    return _leftLbl;
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 53)];
    }
    return _rightLbl;
}

-(UIView *)rightLine{
    if (!_rightLine) {
        _rightLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, TMOnePixel)];
        _rightLine.backgroundColor = GRAY(0xf4);
    }
    return _rightLine;
}
@end
