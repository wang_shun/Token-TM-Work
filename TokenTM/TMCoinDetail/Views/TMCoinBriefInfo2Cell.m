//
//  TMCoinBriefInfoCell.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinBriefInfo2Cell.h"

@interface TMCoinBriefInfo2Cell ()

@property (nonatomic,strong) UILabel* leftLbl;
@property (nonatomic,strong) UILabel* rightLbl;

@end

@implementation TMCoinBriefInfo2Cell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.leftLbl];
    [self addSubview:self.rightLbl];
}

-(void)setMainModel:(TMBriefModel *)mainModel{
    _mainModel = mainModel;
    
    self.leftLbl.text = _mainModel.connectsName;
    //self.rightLbl.text = mainModel.connects;
    
    [self.leftLbl sizeToFit];
    [self.rightLbl sizeToFit];
    
    self.leftLbl.frame = CGRectMake(20, 0, self.leftLbl.frame.size.width, 53);
    //self.rightLbl.frame = CGRectMake(Screen_Width-self.rightLbl.frame.size.width-20, 0, self.rightLbl.frame.size.width, self.rightLbl.frame.size.height);
}


-(UILabel *)leftLbl{
    if (!_leftLbl) {
        _leftLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 24, 53)];
    }
    return _leftLbl;
}

-(UILabel *)rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 24, 53)];
    }
    return _rightLbl;
}

@end
