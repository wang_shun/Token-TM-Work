//
//  TMHomeHeadInfoView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBriefModel.h"
#import "TMCoinPriceModel.h"
@interface TMCoinDetailHeadView : UIView

@property (nonatomic,strong) TMCoinPriceModel *mainModel;

@end
