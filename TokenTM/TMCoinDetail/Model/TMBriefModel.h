//
//  TMBriefModel.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMBriefModel : NSObject



@property (nonatomic,strong) NSString *coinChName;
@property (nonatomic,strong) NSString *connectsName;
@property (nonatomic,strong) NSArray *connects;

@property (nonatomic,strong) NSString *briefName;
@property (nonatomic,strong) NSString *brief;

@property (nonatomic,strong) NSArray *datas;

+ (TMBriefModel *)briefModelFromDictionary:(NSDictionary *)dic;

@end
