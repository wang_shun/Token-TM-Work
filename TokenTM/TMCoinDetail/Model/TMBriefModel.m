//
//  TMBriefModel.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBriefModel.h"

@implementation TMBriefModel

+ (TMBriefModel *)briefModelFromDictionary:(NSDictionary *)dic{
    
    TMBriefModel *model = [[TMBriefModel alloc]init];
    
    model.coinChName = dic[@"coinChName"] ?:@"";
    
    model.connectsName = @"链接";
    
    NSArray *connects = dic[@"coinConnect"];
    if (connects && [connects isKindOfClass:NSArray.class]) {
        model.connects = connects;
    }
    
    model.briefName = @"简介";
    model.brief = dic[@"coinDescription"] ?:@"";
    
//    model.briefName = @"简介";
//    model.brief = dic[@"coinConnect"] ?:@"";
    
    model.datas = @[@{@"流通量":dic[@"coinAvailableSupply"] ?:@""},
                    @{@"发行总量":dic[@"coinTotalSupply"] ?:@""},
                    @{@"发行最大量":dic[@"maxSupply"] ?:@""},
                    @{@"兑换比例":dic[@"rmbPrice"] ?:@""}
                    ];

    return model;
}


@end
