//
//  TMCoinBreifController.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinBreifController.h"
#import "TMCoinBriefInfoCell.h"
#import "TMBriefModel.h"
#import "TMCoinLineTitleView.h"
#import "TMCoinBriefInfo1Cell.h"
#import "TMCoinBriefInfo2Cell.h"

@interface TMCoinBreifController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *mainTable;
@property(nonatomic,strong) TMBriefModel *mainModel;


@end

@implementation TMCoinBreifController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mainTable.frame = self.view.bounds;
    [self.view addSubview:self.mainTable];
    
    __weak typeof(self) weakSelf = self;
    
    NSString* url = [NSString stringWithFormat:@"%@/info/%@/brief.json?openId=%@",TMDomain,self.coinName,@"oT-Ok5Eh283dXEcnbkflXSBvdk2o"];
    //NSString *url = @"https://dj.gl/tokentm/v1/info/btc/brief.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o"
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"status"];
            if (status.integerValue == 200) {
                NSDictionary* dataDic = [responseObject objectForKey:@"data"];

                weakSelf.mainModel = [TMBriefModel briefModelFromDictionary:dataDic];
                [weakSelf.mainTable reloadData];
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.mainTable.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if (self.mainModel.brief.length) {
            return ([self.mainModel.brief boundingRectWithSize:CGSizeMake(Screen_Width-20*2, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height)+75;
        }
        
        return 0;
    }else if (indexPath.row == 1) {
        return 67;
    }else if (indexPath.row == 5) {
        return 55;
    }

    return 53;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.mainModel == nil) {
        return 0;
    }
//    if ([[[self.mainModel.datas lastObject] allKeys][0] isEqualToString:@"兑换量"]) {
//
//    }
    return self.mainModel.datas.count + 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        NSString* cell_Identifier = @"TMCoinBriefInfo1Cell";
        TMCoinBriefInfo1Cell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
        if (cell == nil) {
            cell = [[TMCoinBriefInfo1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.brief = self.mainModel.brief;
        return cell;
    }else if (indexPath.row == 1) {
        NSString* cell_Identifier = @"TMCoinBriefInfo2Cell";
        TMCoinBriefInfo2Cell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
        if (cell == nil) {
            cell = [[TMCoinBriefInfo2Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.mainModel = self.mainModel;
        return cell;
    }else if (indexPath.row == 5) {
        NSString* cell_Identifier = @"TMCoinTitleUITableViewCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = GRAY(0xf4);
            cell.contentView.backgroundColor = GRAY(0xf4);
            
            TMCoinLineTitleView *titleView = [[TMCoinLineTitleView alloc]initWithFrame:CGRectMake(0, 5, Screen_Width, 50)];
            titleView.backgroundColor = [UIColor whiteColor];
            titleView.tag = 111;
            titleView.title = @"公募";
            [cell.contentView addSubview:titleView];
        }
        return cell;
    }else{
        NSString* cell_Identifier = @"TMCoinBriefInfoCell";
        TMCoinBriefInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
        if (cell == nil) {
            cell = [[TMCoinBriefInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row == 6) {
            cell.data = [self.mainModel.datas lastObject];
        }else{
            cell.data = self.mainModel.datas[indexPath.row-2];
        }
        
        return cell;
    }

//    cell.backgroundColor = [UIColor clearColor];
//    cell.contentView.backgroundColor = [UIColor clearColor];
    //cell.priceModel = _dataArray[indexPath.row];
    return nil;
}

-(UITableView *)mainTable{
    if (!_mainTable) {
        _mainTable = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _mainTable.delegate = self;
        _mainTable.dataSource = self;
        _mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _mainTable.estimatedRowHeight = 0;
        _mainTable.estimatedSectionHeaderHeight = 0;
        _mainTable.estimatedSectionFooterHeight = 0;
        //_mainTable.rowHeight = 53;
        _mainTable.backgroundColor = HEXCOLOR(0xebdabb);
    }
    return _mainTable;
}

//-(void)

@end
