//
//  TMCoinDetailController.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseViewController.h"
#import "TMCoinPriceModel.h"


@interface TMCoinDetailController : TMBaseViewController

@property (nonatomic,strong) TMCoinPriceModel *priceModel;

@end


//https://dj.gl/tokentm/v1/info/btc/progress.json?page=1&pageSize=6&openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o

//https://dj.gl/tokentm/v1/info/btc/brief.json?openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o

//https://dj.gl/tokentm/v1/info/btc/notice.json?page=1&pageSize=6&openId=oT-Ok5Eh283dXEcnbkflXSBvdk2o
