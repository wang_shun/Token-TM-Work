//
//  TMCoinBreifController.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMCoinBreifController : UIViewController

@property (nonatomic,strong) NSString *coinName;

@end


//{
//    "data": {
//        "coinAvailableSupply": 17097962,
//        "coinChName": "比特币",
//        "coinConnect": {
//            "website": "https://bitcoin.org/",
//            "twitter": "https://twitter.com/BTCTN",
//            "whitepaper": "https://bitcoin.org/bitcoin.pdf",
//            "facebook": "https://www.facebook.com/buy.bitcoin.news",
//            "explorer": "http://blockchain.info"
//        },
//        "coinDescription": "Bitcoin 比特币的概念最初由中本聪在2009年提出，是点对点的基于 SHA-256 算法的一种 P2P 形式的数字货币，点对点的传输意味着一个去中心化的支付系统，到2140年之前达到固定发行总额2100万。",
//        "coinTotalSupply": 17055312,
//        "maxSupply": 21000000,
//        "rmbPrice": "￥42438.54",
//        "updateTime": 1527306737000
//    },
//    "status": 200
//}

