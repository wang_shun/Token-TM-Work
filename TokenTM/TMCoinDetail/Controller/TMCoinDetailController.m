//
//  TMCoinDetailController.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinDetailController.h"
#import "TMCoinProgressController.h"
#import "TMCoinNoticeController.h"
#import "TMCoinBreifController.h"
#import "TMCoinDetailHeadView.h"
@interface TMCoinDetailController ()<UIScrollViewDelegate>

@property (nonatomic,strong)TMCoinDetailHeadView *headView;
@property (nonatomic,strong)UIView *titleView;
@property (nonatomic,strong)UIButton *leftBtn;
@property (nonatomic,strong)UIButton *midBtn;
@property (nonatomic,strong)UIButton *rightBtn;
@property (nonatomic,strong)UIScrollView *mainScroll;
@property (nonatomic,strong)TMCoinProgressController *progressController;
@property (nonatomic,strong)TMCoinNoticeController *noticeController;
@property (nonatomic,strong)TMCoinBreifController *briefController;

@end

@implementation TMCoinDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNavigationBar];
    [self initViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initViews {

    self.headView = [[TMCoinDetailHeadView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), Screen_Width, 100)];
    self.headView.mainModel =self.priceModel;
    [self.view addSubview:self.headView];
    
    self.titleView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headView.frame), Screen_Width, 40)];
    self.titleView.backgroundColor = GRAY(0xf4);
    [self.titleView addSubview:self.leftBtn];
    [self.titleView addSubview:self.midBtn];
    [self.titleView addSubview:self.rightBtn];
    self.leftBtn.selected = YES;
    [self.view addSubview:self.titleView];
    
    self.mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleView.frame), Screen_Width, Screen_Height-CGRectGetMaxY(self.titleView.frame))];
    
    [self.view addSubview:self.mainScroll];
    
    self.progressController = [[TMCoinProgressController alloc]init];
    self.noticeController = [[TMCoinNoticeController alloc]init];
    self.briefController = [[TMCoinBreifController alloc]init];
    self.briefController.coinName = self.priceModel.coinName;
    
    [self.mainScroll addSubview:self.progressController.view];
    [self.mainScroll addSubview:self.noticeController.view];
    [self.mainScroll addSubview:self.briefController.view];
    self.mainScroll.pagingEnabled = YES;
    self.mainScroll.delegate = self;
    self.mainScroll.contentSize = CGSizeMake(Screen_Width * 3, self.mainScroll.frame.size.height);
    
    [self addChildViewController:self.briefController];
    [self addChildViewController:self.noticeController];
    [self addChildViewController:self.progressController];
    
    self.briefController.view.frame = CGRectMake(0, 0, Screen_Width, self.mainScroll.frame.size.height);
    self.noticeController.view.frame = CGRectMake(Screen_Width, 0, Screen_Width, self.mainScroll.frame.size.height);
    self.progressController.view.frame = CGRectMake(Screen_Width*2, 0, Screen_Width, self.mainScroll.frame.size.height);
}

-(void)leftBtnTapped:(UIButton *)sender{
    if (sender.selected == YES) {
        return;
    }
    sender.selected = !sender.selected;
    self.midBtn.selected = NO;
    self.rightBtn.selected = NO;
    self.mainScroll.contentOffset = CGPointMake(0, 0);
}

-(void)midBtnTapped:(UIButton *)sender{
    if (sender.selected == YES) {
        return;
    }
    sender.selected = !sender.selected;
    self.leftBtn.selected = NO;
    self.rightBtn.selected = NO;
    self.mainScroll.contentOffset = CGPointMake(Screen_Width, 0);
}

-(void)rightBtnTapped:(UIButton *)sender{
    if (sender.selected == YES) {
        return;
    }
    sender.selected = YES;
    self.midBtn.selected = NO;
    self.leftBtn.selected = NO;
    self.mainScroll.contentOffset = CGPointMake(Screen_Width*2, 0);
}

- (UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.backgroundColor = [UIColor clearColor];
        [_leftBtn addTarget:self action:@selector(leftBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        _leftBtn.frame = CGRectMake(0, 0, Screen_Width/3, self.titleView.frame.size.height);
        [_leftBtn setTitle:@"概况" forState:0];
        [_leftBtn setTitleColor:GRAY(0x99) forState:0];
        [_leftBtn setTitleColor:Global_Brown_Color forState:UIControlStateSelected];
    }
    return _leftBtn;
}

- (UIButton *)midBtn{
    if (!_midBtn) {
        _midBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _midBtn.backgroundColor = [UIColor clearColor];
        [_midBtn addTarget:self action:@selector(midBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        _midBtn.frame = CGRectMake(Screen_Width/3, 0, Screen_Width/3, self.titleView.frame.size.height);
        [_midBtn setTitle:@"公告" forState:0];
        [_midBtn setTitleColor:GRAY(0x99) forState:0];
        [_midBtn setTitleColor:Global_Brown_Color forState:UIControlStateSelected];
    }
    return _midBtn;
}

- (UIButton *)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.backgroundColor = [UIColor clearColor];
        [_rightBtn addTarget:self action:@selector(rightBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        _rightBtn.frame = CGRectMake(Screen_Width/3*2, 0, Screen_Width/3, self.titleView.frame.size.height);
        [_rightBtn setTitle:@"进度" forState:0];
        [_rightBtn setTitleColor:GRAY(0x99) forState:0];
        [_rightBtn setTitleColor:Global_Brown_Color forState:UIControlStateSelected];
    }
    return _rightBtn;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x >= Screen_Width && scrollView.contentOffset.x < Screen_Width*2) {
        self.leftBtn.selected = NO;
        self.midBtn.selected = YES;
        self.rightBtn.selected = NO;
    }else if (scrollView.contentOffset.x >= Screen_Width * 2){
        self.leftBtn.selected = NO;
        self.midBtn.selected = NO;
        self.rightBtn.selected = YES;
    }else if (scrollView.contentOffset.x < Screen_Width / 2){
        self.leftBtn.selected = YES;
        self.midBtn.selected = NO;
        self.rightBtn.selected = NO;
    }
}


@end
