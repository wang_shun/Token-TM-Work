//
//  HomeTitleView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTitleView : UIView

@property (nonatomic,strong) UILabel* title_label;

-(instancetype)initWithFrame:(CGRect)frame;

@end
