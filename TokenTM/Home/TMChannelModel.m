//
//  TMChannelModel.m
//  TokenTM
//
//  Created by wang shun on 2018/5/13.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelModel.h"

@implementation TMChannelModel

- (instancetype)init{
    if (self = [super init]) {
        self.recomChannels = [[NSMutableArray alloc] initWithCapacity:0];
        self.userChannels = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)analysisData:(NSDictionary*)resp{
    NSDictionary* data = [resp objectForKey:@"data"];
    NSArray* recom_s = [data objectForKey:@"recomChannels"];
    NSArray* user_s = [data objectForKey:@"userChannels"];
    [self removeAll];
    [self.recomChannels addObjectsFromArray:recom_s];
//    [self.userChannels addObject:@{@""}];
    [self.userChannels addObject:@{@"channelName":@"index",@"nickName":@"综合",@"type":@"index"}];
    [self.userChannels addObjectsFromArray:user_s];
}

- (void)removeAll{
    [self.recomChannels removeAllObjects];
    [self.userChannels removeAllObjects];
}

- (NSInteger)getIndexForData:(NSDictionary *)info{
    if (self.userChannels.count>0) {
        NSInteger return_Integer = 0;
        for (int i=0; i<self.userChannels.count; i++) {
            NSDictionary* d = [self.userChannels objectAtIndex:i];
            NSString* nickName = [d objectForKey:@"nickName"];
            NSString* s_nick = [info objectForKey:@"nickName"];
            if ([s_nick isEqualToString:nickName]) {
                return_Integer = i;
            }
        }
        return return_Integer;
    }
    return -1;
}

- (void)save{
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"ws_TMChannelModel"];
    
    NSMutableData *data = [NSMutableData data];
    //2:创建归档对象
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    //3:开始归档
    [archiver encodeObject:self forKey:@"TMChannelModel"];
    
    [archiver finishEncoding];
    //5:写入文件当中
    BOOL result = [data writeToFile:path atomically:YES];
    if (result) {
        NSLog(@"归档成功:%@",path);
    }else
    {
        NSLog(@"归档不成功!!!");
    }
}

+ (TMChannelModel*)read{
    TMChannelModel* archiver = nil;
    
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"ws_TMChannelModel"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path]){
        NSData *myData = [NSData dataWithContentsOfFile:path];
        //创建反归档对象
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:myData];
        //反归档
        archiver = [unarchiver decodeObjectForKey:@"TMChannelModel"];
        //完成反归档
        [unarchiver finishDecoding];
    }
    
    return archiver;
}

+ (void)removeAll{
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"ws_TMChannelModel"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path]){
        [manager removeItemAtPath:path error:nil];
    }
}



//反归档(反序列化)
//对person对象进行反归档时,该方法执行
//创建一个新的person对象,所有属性都是通过反序列化得到
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.recomChannels = [aDecoder decodeObjectForKey:@"recomChannels"];
        self.userChannels = [aDecoder decodeObjectForKey:@"userChannels"];
        self.resp = [aDecoder decodeObjectForKey:@"resp"];
    }
    
    return self;
}

//归档(序列化)
//对person对象进行归档时,此方法执行
//对person中想要进行归档的所有属性,进行序列化操作
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.recomChannels forKey:@"recomChannels"];
    [aCoder encodeObject:self.userChannels forKey:@"userChannels"];
    [aCoder encodeObject:self.resp forKey:@"resp"];
}




@end
