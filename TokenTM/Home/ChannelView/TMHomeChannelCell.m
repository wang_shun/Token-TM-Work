//
//  TMHomeChannelCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMHomeChannelCell.h"

@interface TMHomeChannelCell ()

@property (nonatomic,strong) UILabel* title_label;
@property (nonatomic,strong) UIButton* title_Button;

@property (nonatomic,strong) NSDictionary* info;

@end

@implementation TMHomeChannelCell

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self createTitleLabel];
        [self createButton];
    }
    return self;
}

- (void)createTitleLabel{
    self.title_label = [[UILabel alloc] initWithFrame:self.bounds];
    [self.title_label setText:@"综合"];
    self.title_label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.title_label];
}

- (void)createButton{
    self.title_Button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.title_Button setFrame:self.bounds];
    [self addSubview:self.title_Button];
    [self.title_Button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)click:(UIButton*)b{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickChannel:WithIndex:)]) {
        [self.delegate clickChannel:self.info WithIndex:self.cur_Index];
    }
}

-(void)updateData:(id)sender{
    if (sender && [sender isKindOfClass:[NSDictionary class]]) {
        self.info = sender;
        NSString* nickName = [self.info objectForKey:@"nickName"];
        [self.title_label setText:nickName];
    }
}

+ (NSDictionary*)attriDicText{
    return @{NSFontAttributeName:[UIFont systemFontOfSize:[UIFont systemFontSize]]};
}

@end
