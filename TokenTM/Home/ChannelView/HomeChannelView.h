//
//  HomeChannelView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMHomeData.h"

@protocol HomeChannelViewDelegate;
@protocol HomeChannelViewDataScource;
@interface HomeChannelView : UIView

@property (nonatomic,weak) id <HomeChannelViewDelegate> delegate;
@property (nonatomic,weak) id <HomeChannelViewDataScource> dataSource;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)reloadData:(id)model;

- (void)mainCollectionViewDidScrollView:(UIScrollView*)scrollView;

- (void)scrollToIndexPath:(NSIndexPath*)indexPath;

@end

@protocol HomeChannelViewDelegate<NSObject>

- (void)channelManagerBtnClick:(UIButton*)b;
- (void)channelNameClick:(NSDictionary*)selectDic WithIndex:(NSIndexPath *)index;

@end

@protocol HomeChannelViewDataScource<NSObject>

- (TMHomeData*)getData;

@end
