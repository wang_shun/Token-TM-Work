//
//  HomeChannelView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "HomeChannelView.h"

#import "TMHomeChannelCell.h"

@interface HomeChannelView ()<UICollectionViewDelegate,UICollectionViewDataSource,TMHomeChannelCellDelegate>

@property (nonatomic,strong) UIButton* channelPageBtn;

@property (nonatomic,strong) UICollectionView* channelCollection;
@property (nonatomic,strong) UIView* sliderView;

@end

@implementation HomeChannelView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createChannelBtn];
        [self createChannelCollectionView];
        [self createslider];
    }
    return self;
}

- (void)createChannelBtn{
    _channelPageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_channelPageBtn setFrame:CGRectMake(self.bounds.size.width-36, 0, 36, self.bounds.size.height)];
    [self.channelPageBtn setImage:[UIImage imageNamed:@"bannerButton.png"] forState:UIControlStateNormal];
    [self.channelPageBtn setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_channelPageBtn];
    [self.channelPageBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)click:(UIButton*)b{
    if (self.delegate && [self.delegate respondsToSelector:@selector(channelManagerBtnClick:)]) {
        [self.delegate channelManagerBtnClick:b];
    }
}

- (void)createslider{
    
    self.sliderView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height-2, (self.channelCollection.bounds.size.width/5.0), 2)];
    self.sliderView.backgroundColor = [UIColor clearColor];//
    [self.channelCollection addSubview:self.sliderView];
    
    CGFloat w = [UIFont systemFontSize]*2+6;
    CGFloat x = (CGRectGetWidth(self.sliderView.frame)-w)/2.0;
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(x, 0, w, 2)];
    [view setBackgroundColor: RGBACOLOR(187, 160, 114, 1)];
    [self.sliderView addSubview:view];
}

- (CGFloat)getTextWidth:(NSString*)str{
    CGRect rect = [str boundingRectWithSize:CGSizeMake(MAXFRAG, self.bounds.size.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{} context:nil];
    return rect.size.width;
}

- (void)createChannelCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    //设置CollectionView的属性
    self.channelCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width-CGRectGetWidth(self.channelPageBtn.frame), self.bounds.size.height) collectionViewLayout:flowLayout];
    self.channelCollection.backgroundColor = [UIColor clearColor];
    
    self.channelCollection.delegate = self;
    self.channelCollection.dataSource = self;
    self.channelCollection.scrollEnabled = YES;
    self.channelCollection.scrollsToTop = NO;
    self.channelCollection.showsHorizontalScrollIndicator = NO;
    
    [self addSubview:self.channelCollection];
    
    //注册Cell
    [self.channelCollection registerClass:[TMHomeChannelCell class] forCellWithReuseIdentifier:@"TMHomeChannelCell"];
}


#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    TMHomeData* home = [self getHomeData];
    if (home) {
        return [home channelNum];
    }
    else{
       return 0;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"TMHomeChannelCell";
    TMHomeChannelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.cur_Index = indexPath;
    cell.delegate = self;
    TMHomeData* home = [self getHomeData];
    NSDictionary* dic = [home eachChannelData:indexPath];
    [cell updateData:dic];
    return cell;
}

#pragma mark  定义每个UICollectionView的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((self.channelCollection.bounds.size.width/5.0),self.bounds.size.height);
}

#pragma mark  定义每个UICollectionView的横向间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark  定义每个UICollectionView的纵向间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - UICollectionViewDelegate


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


- (void)mainCollectionViewDidScrollView:(UIScrollView *)scrollView{
//    NSLog(@"main scrollView:%f",scrollView.contentOffset.x);
    CGFloat x = scrollView.contentOffset.x;
    CGFloat s_w = scrollView.frame.size.width;
    CGFloat b = x/s_w;
    CGFloat slider_x = b*self.sliderView.frame.size.width;

    int n = ((int)slider_x/(int)self.sliderView.frame.size.width);

    [self.sliderView setFrame:CGRectMake(slider_x, self.sliderView.frame.origin.y, self.sliderView.bounds.size.width, self.sliderView.frame.size.height)];
    
    if (n>=2) {
        slider_x = self.sliderView.frame.origin.x;
        CGFloat later3_x = (self.channelCollection.contentSize.width-(3*self.sliderView.frame.size.width));
        
        if (slider_x>=later3_x) {//到最后就不要动了
            
            CGFloat later5_x = (self.channelCollection.contentSize.width-(5*self.sliderView.frame.size.width));
            [self.channelCollection setContentOffset:CGPointMake(later5_x, 0) animated:YES];
        }
        else{
            [self.channelCollection setContentOffset:CGPointMake((n-2)*(self.sliderView.frame.size.width), 0) animated:YES];
        }
    }
}


-(void)reloadData:(id)sender{
    [self.channelCollection reloadData];
}

- (void)clickChannel:(id)sender WithIndex:(NSIndexPath *)index{
    if (self.delegate && [self.delegate respondsToSelector:@selector(channelNameClick:WithIndex:)]) {
        [self.delegate channelNameClick:sender WithIndex:index];
    }
}

- (TMHomeData*)getHomeData{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(getData)]) {
        TMHomeData* homeData = [self.dataSource getData];
        return homeData;
    }
    else{
        return nil;
    }
}

- (void)scrollToIndexPath:(NSIndexPath *)indexPath{
    [self.channelCollection setContentOffset:CGPointMake(indexPath.row * CGRectGetWidth(_channelCollection.frame),0) animated:NO];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
