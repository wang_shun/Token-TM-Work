//
//  TMHomeChannelCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMHomeChannelCellDelegate;
@interface TMHomeChannelCell : UICollectionViewCell

@property (nonatomic,weak) id <TMHomeChannelCellDelegate> delegate;
@property (nonatomic,strong) NSIndexPath* cur_Index;

- (id)initWithFrame:(CGRect)frame;

- (void)updateData:(id)sender;

@end

@protocol TMHomeChannelCellDelegate<NSObject>

- (void)clickChannel:(id)sender WithIndex:(NSIndexPath*)index;

@end
