//
//  HomeViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "HomeViewController.h"

#import "ChannelManagerController.h"
#import "TMNewsDetailViewController.h"
#import "TMCollectViewController.h"
#import "TMSendViewController.h"

#import "HomeTitleView.h"
#import "HomeChannelView.h"
#import "TMMainCollectionView.h"

#import "TMEditView.h"

#import "TMHomeData.h"

#import "TMUserInfo.h"
#import "WeiXinObject.h"

@interface HomeViewController ()<HomeChannelViewDelegate,HomeChannelViewDataScource,TMMainCollectionViewDelegate,TMMainCollectionViewDataScource,TMHomeDataDelegate,TMEditViewDelegate,ChannelManagerControllerDelegate>

@property (nonatomic,strong) HomeTitleView* titleView;
@property (nonatomic,strong) HomeChannelView* channelView;
@property (nonatomic,strong) TMMainCollectionView* mainView;

@property (nonatomic,strong) TMEditView* editView;

@property (nonatomic,strong) TMHomeData* homeData;

@property (nonatomic,strong) NSDictionary* curChannelData;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createTitleView];
    [self createChannelView];

    [self createTable];

    [self createEdit];
    
    self.homeData = [[TMHomeData alloc] init];
    self.homeData.delegate = self;
    
    if (![TMUserInfo isLogin]) {
        
        [TMUserInfo login:nil Completion:^(NSDictionary *result) {
            [self.homeData getChannelData:nil];
        }];
    }
    else{
        [self.homeData getChannelData:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)createEdit{
    
    CGFloat x = self.view.bounds.size.width-(290/2.0);
    CGFloat y = self.view.bounds.size.height-(290/2.0);
    CGFloat w = 290/2.0;
    CGFloat h = 290/2.0;
    
    self.editView = [[TMEditView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    self.editView.delegate = self;
    [self.view addSubview:self.editView];
}

- (void)createTitleView{
    CGFloat h = 0;
    if (IS_IPHONE_X) {
        h = 24;//电池栏+24
    }
    self.titleView = [[HomeTitleView alloc] initWithFrame:CGRectMake(0, h, self.view.bounds.size.width, TMNavigationBarHeight)];
    [self.view addSubview:self.titleView];
}

- (void)createChannelView{
    self.channelView = [[HomeChannelView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleView.frame), self.view.bounds.size.width, 36)];
    [self.view addSubview:self.channelView];
    self.channelView.delegate = self;
    self.channelView.dataSource = self;
}

- (void)createTable{
    self.mainView = [[TMMainCollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.channelView.frame), self.view.bounds.size.width, self.view.bounds.size.height-CGRectGetMaxY(self.channelView.frame))];
    [self.view addSubview:self.mainView];
    self.mainView.delegate = self;
    self.mainView.dataSource = self;
}

#pragma mark - HomeChannelViewDelegate

//进频道管理页面
- (void)channelManagerBtnClick:(UIButton *)b{

    TMChannelModel* channel_M_data = [self.homeData getChannelManagerData:nil];
    ChannelManagerController* cm_vc = [[ChannelManagerController alloc] initWthChannelData:channel_M_data];
    cm_vc.delegate = self;
    [self.navigationController pushViewController:cm_vc animated:YES];
}

- (void)channelItemClick:(NSDictionary *)dic{
    NSInteger index = [self.homeData getIndexForData:dic];
    if (index != -1) {
        NSIndexPath* indexpath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.mainView seletecPage:indexpath];
    }
}

- (void)refreshHomeChannelList:(id)sender{
    [self.homeData getChannelData:nil];
    
    [self.homeData resort];
    
    [self.mainView reloadData:nil];
}


- (void)channelNameClick:(NSDictionary*)selectDic WithIndex:(NSIndexPath *)index{
    NSLog(@"clickChannel:%@",selectDic);
    
    [self.mainView seletecPage:index];
    
    if (self.channelView) {
        [self.channelView mainCollectionViewDidScrollView:self.mainView.collectionView];
    }
}


#pragma mark - HomeChannelViewDataSource

#pragma mark - TMMainCollectionViewDataScource & HomeChannelViewDataSource

-(TMHomeData *)getData{
    return self.homeData;
}

#pragma mark - TMMainCollectionViewDelegate

- (void)clickCell:(NSDictionary *)info{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.channelView) {
        [self.channelView mainCollectionViewDidScrollView:scrollView];
    }
    
    if (scrollView.contentSize.width>self.view.bounds.size.width) {
        if (scrollView.contentOffset.x >=self.view.bounds.size.width) {
            [self.editView setSendCommentEnable:YES];
        }
        else{
            [self.editView setSendCommentEnable:NO];
        }
    }
    else{
        [self.editView setSendCommentEnable:NO];
    }
}

-(void)willDisplayCell:(id)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////



#pragma mark - HomeData Delegate

- (void)netFinishedChannel:(id)sender{
    if (sender) {
        [self.channelView reloadData:nil];
        [self.mainView reloadData:nil];
    }
}

-(void)enterNewsDetailPage:(TMListModel *)model{
    TMNewsDetailViewController* newsvc = [[TMNewsDetailViewController alloc] initWithTMListModel:model];
    [self.navigationController pushViewController:newsvc animated:YES];
}


#pragma mark - TMEidtDelgate
//分享
- (void)enterShare:(id)sender{
    NSString* shareUrl = @"http://www.token.tm/index";
    
    NSMutableDictionary* shareDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [shareDic setObject:shareUrl forKey:@"shareUrl"];
    [shareDic setObject:@"区块链资讯与社群平台，全球链情，为你而聚" forKey:@"shareTitle"];
    [shareDic setObject:@"" forKey:@"sharePic"];
    
    [WeiXinObject share:shareDic Completion:^(NSDictionary *resp) {
        
    }];
}

//收藏
- (void)enterCollectionPage:(id)sender{
    TMCollectViewController* c_vc = [[TMCollectViewController alloc] init];
    [self.navigationController pushViewController:c_vc animated:YES];
}

//发文
-(void)enterSendPage:(id)sender{
    
    NSDictionary* dic =[self.homeData getChannelInfoForIndex:self.mainView.curIndex];
    
    TMSendViewController* s_vc = [[TMSendViewController alloc] initWithData:dic];
    [self.navigationController pushViewController:s_vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
