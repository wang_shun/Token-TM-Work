//
//  TMChannelModel.h
//  TokenTM
//
//  Created by wang shun on 2018/5/13.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMChannelModel : NSObject

@property (nonatomic,strong) NSMutableDictionary* resp;//原数据

@property (nonatomic,strong) NSMutableArray* recomChannels;
@property (nonatomic,strong) NSMutableArray* userChannels;

- (instancetype)init;

- (void)analysisData:(NSDictionary*)resp;

- (NSInteger)getIndexForData:(NSDictionary *)info;

- (void)save;

+ (void)removeAll;
+ (TMChannelModel*)read;

@end
