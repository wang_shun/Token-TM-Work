//
//  TMCoinPriceModel.m
//  TokenTM
//
//  Created by qz on 2018/5/25.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCoinPriceModel.h"

@implementation TMCoinPriceModel

+(TMCoinPriceModel *)priceModelFromDictionary:(NSDictionary *)dic{
    
    TMCoinPriceModel *model = [[TMCoinPriceModel alloc]init];
    
    
    //model.alertSwitch = dic[@""]
    
    model.coinName = dic[@"coinName"] ?: @"";
    model.fullName = dic[@"fullName"] ?: @"";
    model.daySupply = dic[@"daySupply"] ?: @"";
    model.hourRatio = dic[@"hourRatio"] ?: @"";
    model.dayRatio = dic[@"dayRatio"] ?: @"";
    model.weekRatio = dic[@"weekRatio"] ?: @"";
    model.rmbPrice = dic[@"rmbPrice"] ?: @"";
    
    
    return model;
}

@end


//"data": {
//    "alertSwitch": true,
//    "coinName": "btc",
//    "dayRatio": "-1.83%",
//    "daySupply": "￥4,201,588万",
//    "fullName": "Bitcoin",
//    "hourRatio": "+0.16%",
//    "rmbPrice": "￥48267.88",
//    "weekRatio": "-8.66%"
//},
//"status": 200
