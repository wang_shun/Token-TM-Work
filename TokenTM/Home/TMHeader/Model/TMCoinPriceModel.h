//
//  TMCoinPriceModel.h
//  TokenTM
//
//  Created by qz on 2018/5/25.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCoinPriceModel : NSObject

@property (nonatomic,assign) BOOL alertSwitch;
@property (nonatomic,copy) NSString *coinName;
@property (nonatomic,copy) NSString *fullName;

@property (nonatomic,copy) NSString *daySupply;

@property (nonatomic,copy) NSString *hourRatio;
@property (nonatomic,copy) NSString *dayRatio;
@property (nonatomic,copy) NSString *weekRatio;
@property (nonatomic,copy) NSString *rmbPrice;

+(TMCoinPriceModel *)priceModelFromDictionary:(NSDictionary *)dic;

@end



