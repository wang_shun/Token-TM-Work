//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMHomeHeadInfoView.h"
#import "TMHomeHeadInfoCell.h"

@interface TMHomeHeadInfoView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *mainTable;
@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMHomeHeadInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.mainTable];
}


-(void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    
    if (dataArray && dataArray.count) {
        [self.mainTable reloadData];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count > 2 ? 2 :_dataArray.count;
}

- (TMHomeHeadInfoCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* cell_Identifier = @"TMMYChannelCell";
    TMHomeHeadInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
    if (cell == nil) {
        cell = [[TMHomeHeadInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.priceModel = _dataArray[indexPath.row];
    return cell;
}

-(UITableView *)mainTable{
    if (!_mainTable) {
        _mainTable = [[UITableView alloc]initWithFrame:self.viewRect style:UITableViewStylePlain];
        _mainTable.delegate = self;
        _mainTable.dataSource = self;
        _mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _mainTable.estimatedRowHeight = 0;
        _mainTable.estimatedSectionHeaderHeight = 0;
        _mainTable.estimatedSectionFooterHeight = 0;
        _mainTable.rowHeight = 32;
        _mainTable.backgroundColor = HEXCOLOR(0xebdabb);
    }
    return _mainTable;
}

@end
