//
//  TMHomeHeadInfoView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMCoinHeadInfoView.h"
#import "TMCoinDetailController.h"
#import "TMAlertController.h"

@interface TMCoinHeadInfoView ()

@property (nonatomic,strong) UIView* leftBgView;
@property (nonatomic,strong) UIImageView* coinImageView;

@property (nonatomic,strong) UILabel* detailLbl;

@property (nonatomic,strong) UIScrollView* mainScroll;

@property (nonatomic,strong) UILabel* priceLbl;
@property (nonatomic,strong) UILabel* volumeLbl;
@property (nonatomic,strong) UILabel* changePercentHour;
@property (nonatomic,strong) UILabel* changePercentDay;
@property (nonatomic,strong) UILabel* changePercentWeek;

@property (nonatomic,strong) UIButton* alertBtn;

@property (nonatomic,strong) UIButton* clearBtn;

@end

@implementation TMCoinHeadInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    
    [self addSubview:self.leftBgView];
    
    [self.leftBgView addSubview:self.coinImageView];
    [self.leftBgView addSubview:self.detailLbl];
    [self.leftBgView addSubview:self.clearBtn];
    
    [self addSubview:self.mainScroll];
    [self.mainScroll addSubview:self.priceLbl];
    [self.mainScroll addSubview:self.volumeLbl];
    [self.mainScroll addSubview:self.changePercentHour];
    [self.mainScroll addSubview:self.changePercentDay];
    [self.mainScroll addSubview:self.changePercentWeek];
    
    [self addSubview:self.alertBtn];
}

-(NSAttributedString *)priceAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:Global_Black_Color,NSFontAttributeName:[UIFont systemFontOfSize:16]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:16]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"价格" attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.rmbPrice attributes:dic1]];
    return maString;
}

-(NSAttributedString *)volumeAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:Global_Black_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"成交量24h    " attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.rmbPrice attributes:dic1]];
    return maString;
}

-(NSAttributedString *)hourAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:[self upOrDownColor:_priceModel.hourRatio],NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"1h" attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.hourRatio attributes:dic1]];
    return maString;
}

-(NSAttributedString *)dayAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:[self upOrDownColor:_priceModel.dayRatio],NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"24h " attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.dayRatio attributes:dic1]];
    return maString;
}

-(NSAttributedString *)weekAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:[self upOrDownColor:_priceModel.weekRatio],NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"7d " attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.weekRatio attributes:dic1]];
    return maString;
}

-(UIColor *)upOrDownColor:(NSString *)string{
    if ([string hasPrefix:@"-"]) {
        return Coin_Plummet_Color;
    }
        
    return Coin_Rise_Color;
}

-(void)setPriceModel:(TMCoinPriceModel *)priceModel{
    
    _priceModel = priceModel;
    
    _priceLbl.attributedText = [self priceAttriString];
    _volumeLbl.attributedText = [self volumeAttriString];
    _changePercentHour.attributedText = [self hourAttriString];
    _changePercentDay.attributedText = [self dayAttriString];
    _changePercentWeek.attributedText = [self weekAttriString];
    
    [self refreshUIFrame];
}

-(void)refreshUIFrame{
    _leftBgView.frame = CGRectMake(0, 0, 50, self.frame.size.height);
    _coinImageView.frame = CGRectMake(10, 10, 29, 29);
    _detailLbl.frame = CGRectMake(0, CGRectGetMaxY(_coinImageView.frame)+8, CGRectGetWidth(_leftBgView.frame), 19);
    
    _clearBtn.frame = _leftBgView.bounds;
    
    _alertBtn.frame = CGRectMake(Screen_Width-12-25, 10, 25, 25);
    
    _mainScroll.frame = CGRectMake(CGRectGetMaxX(_leftBgView.frame), 0, Screen_Width-CGRectGetMaxX(_leftBgView.frame), self.frame.size.height);
    
    [_priceLbl sizeToFit];
    _priceLbl.frame = CGRectMake(13, 14, CGRectGetWidth(_priceLbl.frame), CGRectGetHeight(_priceLbl.frame));
    
    [_volumeLbl sizeToFit];
    _volumeLbl.frame = CGRectMake(CGRectGetMinX(_priceLbl.frame), CGRectGetMaxY(_priceLbl.frame)+11, CGRectGetWidth(_volumeLbl.frame), CGRectGetHeight(_volumeLbl.frame));
    
    [_changePercentHour sizeToFit];
    _changePercentHour.frame = CGRectMake(CGRectGetMaxX(_priceLbl.frame)+30, CGRectGetMinY(_priceLbl.frame), CGRectGetWidth(_changePercentHour.frame), CGRectGetHeight(_changePercentHour.frame));
    
    [_changePercentDay sizeToFit];
    _changePercentDay.frame = CGRectMake(CGRectGetMinX(_changePercentHour.frame), CGRectGetMinY(_volumeLbl.frame), CGRectGetWidth(_changePercentDay.frame), CGRectGetHeight(_changePercentDay.frame));
    
    [_changePercentWeek sizeToFit];
    _changePercentWeek.frame = CGRectMake(CGRectGetMaxX(_changePercentDay.frame)+5, CGRectGetMinY(_volumeLbl.frame), CGRectGetWidth(_changePercentWeek.frame), CGRectGetHeight(_changePercentWeek.frame));
}

-(void)clearBtnTapped:(UIButton *)sender{
    if(self.nav){
        TMCoinDetailController *detail = [[TMCoinDetailController alloc]init];
        detail.priceModel = self.priceModel;
        [self.nav pushViewController:detail animated:YES];
    }
}

-(void)alertBtnTapped:(UIButton *)sender{
    if(self.nav){
        TMAlertController *detail = [[TMAlertController alloc]init];
        detail.priceModel = self.priceModel;
        [self.nav pushViewController:detail animated:YES];
    }
}

-(UILabel *)priceLbl{
    if (!_priceLbl) {
        _priceLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    }
    return _priceLbl;
}

-(UILabel *)volumeLbl{
    if (!_volumeLbl) {
        _volumeLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    }
    return _volumeLbl;
}

-(UILabel *)changePercentHour{
    if (!_changePercentHour) {
        _changePercentHour = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    }
    return _changePercentHour;
}

-(UILabel *)changePercentDay{
    if (!_changePercentDay) {
        _changePercentDay = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    }
    return _changePercentDay;
}

-(UILabel *)changePercentWeek{
    if (!_changePercentWeek) {
        _changePercentWeek = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
    }
    return _changePercentWeek;
}

-(UIScrollView *)mainScroll{
    if (!_mainScroll) {
        _mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leftBgView.frame), 0, Screen_Width-CGRectGetMaxX(_leftBgView.frame), self.frame.size.height)];
        _mainScroll.backgroundColor = HEXCOLOR(0xebdabb);
    }
    return _mainScroll;
}

-(UIView *)leftBgView{
    if (!_leftBgView) {
        _leftBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, self.frame.size.height)];
        _leftBgView.backgroundColor = Global_Brown_Color;
    }
    return _leftBgView;
}

-(UILabel *)detailLbl{
    if (!_detailLbl) {
        _detailLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 15)];
        _detailLbl.text = @"项目详情";
        _detailLbl.backgroundColor = [UIColor clearColor];
        _detailLbl.textAlignment = NSTextAlignmentCenter;
        _detailLbl.textColor = [UIColor whiteColor];
        _detailLbl.font = [UIFont systemFontOfSize:8];
    }
    return _detailLbl;
}

-(UIImageView *)coinImageView{
    if (!_coinImageView) {
        _coinImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 29, 29)];
        _coinImageView.image = [UIImage imageNamed:@"cell_header_detail_icon"];
    }
    return _coinImageView;
}

-(UIButton *)clearBtn{
    if (!_clearBtn) {
        _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _clearBtn.backgroundColor = [UIColor clearColor];
        [_clearBtn addTarget:self action:@selector(clearBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clearBtn;
}

-(UIButton *)alertBtn{
    if (!_alertBtn) {
        _alertBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _alertBtn.backgroundColor = Global_Brown_Color;
        _alertBtn.layer.masksToBounds = YES;
        _alertBtn.layer.cornerRadius = 12.5;
        [_alertBtn setImage:[UIImage imageNamed:@"cell_header_alert_button"] forState:0];
        [_alertBtn addTarget:self action:@selector(alertBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _alertBtn;
}


@end
