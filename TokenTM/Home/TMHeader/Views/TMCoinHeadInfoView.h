//
//  TMHomeHeadInfoView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMCoinPriceModel.h"

@interface TMCoinHeadInfoView : UIView

@property (nonatomic,strong) TMCoinPriceModel *priceModel;
@property (nonatomic,weak) UINavigationController *nav;

-(instancetype)initWithFrame:(CGRect)frame;

@end
