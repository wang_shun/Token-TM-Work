//
//  TMHomeHeadInfoCell.h
//  TokenTM
//
//  Created by qz on 2018/5/23.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMCoinPriceModel.h"

@interface TMHomeHeadInfoCell : UITableViewCell

@property (nonatomic,strong) TMCoinPriceModel*priceModel;
@end
