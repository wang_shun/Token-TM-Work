//
//  TMHomeHeadInfoCell.m
//  TokenTM
//
//  Created by qz on 2018/5/23.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMHomeHeadInfoCell.h"

@interface TMHomeHeadInfoCell ()

@property (nonatomic,strong) UILabel* rmbLbl;
@property (nonatomic,strong) UILabel* hourRadioLbl;
@property (nonatomic,strong) UILabel* dayRadioLbl;

@end

@implementation TMHomeHeadInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.rmbLbl];
    [self addSubview:self.hourRadioLbl];
    [self addSubview:self.dayRadioLbl];
}

-(NSAttributedString *)rmbAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:Global_Black_Color,NSFontAttributeName:[UIFont systemFontOfSize:14]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:14]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",[_priceModel.coinName uppercaseString]]  attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.rmbPrice attributes:dic1]];
    return maString;
}

-(NSAttributedString *)hourAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:[self upOrDownColor:_priceModel.hourRatio],NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"1h " attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.hourRatio attributes:dic1]];
    return maString;
}

-(NSAttributedString *)dayAttriString{
    if (_priceModel == nil)   return nil;
    
    NSMutableAttributedString *maString = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *dic1 = @{NSForegroundColorAttributeName:[self upOrDownColor:_priceModel.dayRatio],NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSDictionary *dic2 = @{NSForegroundColorAttributeName:Global_Brown_Color,NSFontAttributeName:[UIFont systemFontOfSize:12]};
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:@"24h " attributes:dic2]];
    [maString appendAttributedString:[[NSAttributedString alloc] initWithString:_priceModel.dayRatio attributes:dic1]];
    return maString;
}

-(UIColor *)upOrDownColor:(NSString *)string{
    if ([string hasPrefix:@"-"]) {
        return Coin_Plummet_Color;
    }
    
    return Coin_Rise_Color;
}

-(void)setPriceModel:(TMCoinPriceModel *)priceModel{
    
    _priceModel = priceModel;
    
    _rmbLbl.attributedText = [self rmbAttriString];
    _hourRadioLbl.attributedText = [self hourAttriString];
    _dayRadioLbl.attributedText = [self dayAttriString];

    [self refreshUIFrame];
}

-(void)refreshUIFrame{
    
    [_rmbLbl sizeToFit];
    _rmbLbl.frame = CGRectMake(13, 10, CGRectGetWidth(_rmbLbl.frame), CGRectGetHeight(_rmbLbl.frame));
    
    [_hourRadioLbl sizeToFit];
    _hourRadioLbl.frame = CGRectMake(Screen_Width/2-CGRectGetWidth(_hourRadioLbl.frame)/2, CGRectGetMinY(_rmbLbl.frame), CGRectGetWidth(_hourRadioLbl.frame), CGRectGetHeight(_hourRadioLbl.frame));
    
    [_dayRadioLbl sizeToFit];
    _dayRadioLbl.frame = CGRectMake(Screen_Width-CGRectGetWidth(_dayRadioLbl.frame)-13, CGRectGetMinY(_rmbLbl.frame), CGRectGetWidth(_dayRadioLbl.frame), CGRectGetHeight(_dayRadioLbl.frame));
}


-(UILabel *)rmbLbl{
    if (!_rmbLbl) {
        _rmbLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 24, 20)];
    }
    return _rmbLbl;
}

-(UILabel *)hourRadioLbl{
    if (!_hourRadioLbl) {
        _hourRadioLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 24, 20)];
    }
    return _hourRadioLbl;
}

-(UILabel *)dayRadioLbl{
    if (!_dayRadioLbl) {
        _dayRadioLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 24, 20)];
    }
    return _dayRadioLbl;
}

@end
