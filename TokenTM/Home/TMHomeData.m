//
//  TMHomeData.m
//  TokenTM
//
//  Created by wang shun on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMHomeData.h"

#import "TMChannelListData.h"

@interface TMHomeData ()
{
    BOOL isLoading;
}
@property (nonatomic,strong) NSMutableDictionary* channelList;//横滚数组 这里面key = indexPath

@property (nonatomic,strong) TMChannelModel* cur_Channelmodel;

@property (nonatomic,strong) NSMutableDictionary* tables_Dic;


@end

@implementation TMHomeData

-(instancetype)init{
    if (self = [super init]) {
        _channelList = [[NSMutableDictionary alloc] initWithCapacity:0];
        _tables_Dic = [[NSMutableDictionary alloc] initWithCapacity:0];
        isLoading = NO;
    }
    return self;
}

- (void)resort{
    NSArray* arr = self.cur_Channelmodel.userChannels;
    
    NSMutableDictionary* resortDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    for (int i = 0; i<arr.count ; i++) {
        NSDictionary* dic = [arr objectAtIndex:i];
        NSString* channelName = [dic objectForKey:@"channelName"];
        
        TMChannelListData* resort_listData  = [[TMChannelListData alloc] init];
        resort_listData.channelName = channelName;
        
        NSLog(@"resort:%@",dic);
        NSArray* keyArr = self.channelList.allKeys;
        for (int j = 0; j<keyArr.count; j++) {
            NSIndexPath* index = [keyArr objectAtIndex:j];
            TMChannelListData* listData = [self.channelList objectForKey:index];
            if ([listData.channelName isEqualToString:channelName]) {
                if (listData.mArr.count>0) {
                    resort_listData.mArr = listData.mArr;
                    resort_listData.curIndex = listData.curIndex;
                    resort_listData.resp = listData.resp;
                }
            }
        }
        NSIndexPath* re_index = [NSIndexPath indexPathForRow:i inSection:0];
        [resortDic setObject:resort_listData forKey:re_index];
    }
    
    self.channelList = resortDic;
}

- (BOOL)getIsHaveDataWithCurIndexPath:(NSIndexPath*)indexPath{
  
    TMChannelListData* listData = [self.channelList objectForKey:indexPath];
    NSArray* arr = listData.mArr;
    if (arr && arr.count>0) {
        return YES;
    }
    return NO;
}

-(void)getChannelData:(id)sender{
    
    if ([TMChannelModel read]) {
        self.cur_Channelmodel = [TMChannelModel read];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(netFinishedChannel:)]) {
                [self.delegate netFinishedChannel:self];
            }
        });
        return;
    }
    
    NSString* url = [NSString stringWithFormat:@"%@/channel/info.json?openId=%@",TMDomain,[TMUserInfo getOpenID]];
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"getChannelData responseObject::%@",responseObject);
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"status"];
            if (status.integerValue == 200) {
                TMChannelModel* model = [[TMChannelModel alloc] init];
                model.resp = responseObject;
                [model analysisData:responseObject];
                
                if (model.resp) {
                    self.cur_Channelmodel = model;
                    [self.cur_Channelmodel save];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.delegate && [self.delegate respondsToSelector:@selector(netFinishedChannel:)]) {
                            [self.delegate netFinishedChannel:self];
                        }
                    });
                }
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}


-(NSInteger)channelNum{
    return self.cur_Channelmodel.userChannels.count;
}

- (NSDictionary*)eachChannelData:(NSIndexPath*)indexPath{
    NSDictionary* dic = [self.cur_Channelmodel.userChannels objectAtIndex:indexPath.row];
    return dic;
}

- (NSInteger)mainViewsNum{
    return self.cur_Channelmodel.userChannels.count;
}


- (NSInteger)eachTalbleNum:(NSIndexPath*)indexPath{
    TMChannelListData* listData = [self.channelList objectForKey:indexPath];
    NSArray* arr = listData.mArr;
    if (arr.count) {
        return arr.count;
    }
    else{
        return 0;
    }
}

- (TMListModel*)eachTalbleContentData:(NSIndexPath*)indexPath WithCurTabIndex:(NSIndexPath*)cindex{
    TMChannelListData* listData = [self.channelList objectForKey:cindex];
    TMListModel* m = [listData.mArr objectAtIndex:indexPath.row];
    return m;
}


- (void)getEachChannelTableDataIndex:(NSIndexPath*)indexPath WithPage:(NSInteger)page PullDirection:(NSString*)direction Completion:(void (^)(NSArray*result))method{
    //    NSLog(@"getEachChannelTableDataIndex index::::%@",indexPath);
    //    feed
    if (isLoading) {
        return;
    }
    isLoading = YES;
    
    NSDictionary* dic = [self.cur_Channelmodel.userChannels objectAtIndex:indexPath.row];
    NSString* channelName = [dic objectForKey:@"channelName"];

    NSString* pageStr = [NSString stringWithFormat:@"%d",(int)page];
    
    TMChannelListData* listData = [self.channelList objectForKey:indexPath];
    if (!listData) {
        listData = [[TMChannelListData alloc] init];
        [self.channelList setObject:listData forKey:indexPath];
        listData.curIndex = indexPath;
        listData.channelName = channelName;
    }

    [listData getCurIndexDataChannelName:channelName page:pageStr Direction:direction Completion:^(NSArray *result) {
        isLoading = NO;
        if (method) {
            method(result);
        }
    }];
}

- (NSString*)getChannelName:(NSInteger)index{
    NSDictionary* dic = [self.cur_Channelmodel.userChannels objectAtIndex:index];
    if (dic) {
        NSString* channelName = [dic objectForKey:@"channelName"];
        return channelName;
    }
    return @"";
}

- (void)getHeaderDataWith:(NSString *)coinName Completion:(void (^)(TMCoinPriceModel* model))method{
    
    NSString* url = [NSString stringWithFormat:@"%@/price/%@.json?openId=%@",TMDomain,coinName,[TMUserInfo getOpenID]];
    

    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"status"];
            if (status.integerValue == 200) {
                NSDictionary* dic = [responseObject objectForKey:@"data"];
                
                TMCoinPriceModel* model = [TMCoinPriceModel priceModelFromDictionary:dic];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (method) {
                        method(model);
                    }
                });
            }
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
    
}


#pragma mark - table selected

- (void)didSelectedTableCellDataIndex:(NSIndexPath*)indexPath WithCIndex:(NSIndexPath*)cindex{
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterNewsDetailPage:)]) {
        TMChannelListData* listData = [self.channelList objectForKey:cindex];
        TMListModel* model = [listData.mArr objectAtIndex:indexPath.row];
        [self.delegate enterNewsDetailPage:model];
    }
}


#pragma mark - get Channel Manager

- (TMChannelModel*)getChannelManagerData:(NSDictionary*)dic{
    if (self.cur_Channelmodel) {
        return self.cur_Channelmodel;
    }
    return nil;
}

-(NSInteger)getIndexForData:(NSDictionary *)info{
    NSInteger n = [self.cur_Channelmodel getIndexForData:info];
    return n;
}

- (NSDictionary*)getChannelInfoForIndex:(NSIndexPath*)indexPath{
    NSDictionary* dic = nil;
    if (self.cur_Channelmodel.userChannels.count>=(indexPath.row+1)) {
        dic = [self.cur_Channelmodel.userChannels objectAtIndex:indexPath.row];
    }
    return dic;
}




@end
