//
//  TMChannelListData.h
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMChannelListData : NSObject

@property (nonatomic,strong) NSDictionary* resp;//源数据

@property (nonatomic,strong) NSIndexPath* curIndex;
@property (nonatomic,strong) NSString* channelName;

@property (nonatomic,strong) NSMutableArray* mArr;

- (instancetype)init;

- (void)getCurIndexDataChannelName:(NSString*)channelName page:(NSString*)page Direction:(NSString*)direction Completion:(void (^)(NSArray *result))method;

- (void)addListBeforeAllData:(NSArray*)arr;
- (void)addListLaterAllData:(NSArray*)arr;

@end
