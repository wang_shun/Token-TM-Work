//
//  TMRedView.h
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMRedViewDelegate;

@interface TMRedView : UIView

@property (nonatomic,weak) id <TMRedViewDelegate> delegate;

@property (nonatomic,strong) UIImageView* bgView;

@property (nonatomic,strong) UIView* cover_View;

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* selected_name;

@property (nonatomic,assign) BOOL isEnable;

@property (nonatomic,assign) BOOL isSelected;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setBgColor:(UIColor *)bgColor;

- (void)setlayCornerRadius:(CGFloat)f;

- (void)setImage:(NSString*)imageName;
- (void)setSelectedImage:(NSString*)imageName;

- (void)setEnableClick:(BOOL)b;

@end

@protocol TMRedViewDelegate<NSObject>

- (void)click:(id)sender;

@end
