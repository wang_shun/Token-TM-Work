//
//  TMRedView.m
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMRedView.h"

@implementation TMRedView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        
        [self setBackgroundColor:RGBACOLOR(204, 51, 51, 1)];
        
        self.layer.cornerRadius = frame.size.width/2.0;
        
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowOpacity = 0.6;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        
        [self createView];
        
        [self setEnableClick:YES];
    }
    return self;
}

- (void)setBgColor:(UIColor *)bgColor{
    if (bgColor) {
        [self setBackgroundColor:bgColor];
    }
}

- (void)setlayCornerRadius:(CGFloat)f{
    self.layer.cornerRadius = f;
    self.cover_View.layer.cornerRadius = f;
    [self.cover_View removeFromSuperview];
}

- (void)createView{
    self.bgView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.bgView];
    self.bgView.center = CGPointMake(self.bounds.size.width/2.0, self.bounds.size.height/2.0);
    
    self.cover_View = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:self.cover_View];
    self.cover_View.backgroundColor = [UIColor blackColor];
    [self.cover_View setAlpha:0.3];
    self.cover_View.hidden = YES;
    self.cover_View.layer.cornerRadius = self.bounds.size.width/2.0;
    self.cover_View.layer.masksToBounds = YES;
}

- (void)setImage:(NSString*)imageName{
    if (imageName) {
        self.name = imageName;
    }
    [self.bgView setImage:[UIImage imageNamed:imageName]];
}

- (void)setSelectedImage:(NSString*)imageName{
    if (imageName) {
        self.selected_name = imageName;
    }
}

-(void)setEnableClick:(BOOL)b{
    self.isEnable = b;
    if (b) {
        self.cover_View.hidden = YES;
    }
    else{
        self.cover_View.hidden = NO;
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.isEnable) {
        return;
    }
    
    self.cover_View.hidden = NO;
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.isEnable) {
        return;
    }
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.isEnable) {
        return;
    }
    
    self.cover_View.hidden = YES;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!self.isEnable) {
        return;
    }
    
    self.cover_View.hidden = YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(click:)]) {
        [self.delegate click:self];
    }
}


- (void)setIsSelected:(BOOL)isSelected{
    _isSelected = isSelected;
    
    if (_isSelected == YES) {
        [self.bgView setImage:[UIImage imageNamed:self.selected_name]];
    }
    else{
        [self.bgView setImage:[UIImage imageNamed:self.name]];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
