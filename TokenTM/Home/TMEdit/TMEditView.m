//
//  TMEditView.m
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMEditView.h"

#import "TMRedView.h"

@interface TMEditView ()<TMRedViewDelegate>
{
    BOOL isAnimation;
    BOOL isOpen;
}
@property (nonatomic,strong) TMRedView* addView;

@property (nonatomic,strong) TMRedView* shareView;
@property (nonatomic,strong) TMRedView* commentView;
@property (nonatomic,strong) TMRedView* collectView;

@end

@implementation TMEditView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

- (void)createView{
    CGFloat w = 108/2.0;
    CGFloat h = 108/2.0;
    CGFloat x = self.bounds.size.width-w-14;
    CGFloat y = self.bounds.size.height-h-16;
    
    self.addView = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [self.addView setImage:@"edit_add.png"];
    [self addSubview:self.addView];
    self.addView.delegate = self;
    
    w = 90/2.0;
    h = 90/2.0;
    x = self.bounds.size.width-w-14;
    y = self.bounds.size.height-h-16;
    
    self.shareView = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [self.shareView setImage:@"edit_share.png"];
    [self addSubview:self.shareView];
    self.shareView.center = self.addView.center;
    self.shareView.delegate = self;
    
    w = 90/2.0;
    h = 90/2.0;
    x = self.bounds.size.width-w-14;
    y = self.bounds.size.height-h-16;
    
    self.commentView = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [self.commentView setImage:@"edit_comment.png"];
    [self addSubview:self.commentView];
    self.commentView.center = self.addView.center;
    self.commentView.delegate = self;
    
    w = 90/2.0;
    h = 90/2.0;
    x = self.bounds.size.width-w-14;
    y = self.bounds.size.height-h-16;
    
    self.collectView = [[TMRedView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    [self.collectView setImage:@"edit_collect.png"];
    [self addSubview:self.collectView];
    self.collectView.center = self.addView.center;
    self.collectView.delegate = self;
    
    self.shareView.hidden = YES;
    self.commentView.hidden = YES;
    self.collectView.hidden = YES;
    isOpen = NO;
    
    [self bringSubviewToFront:self.addView];
    [self setSendCommentEnable:NO];
}

- (void)click:(id)sender{
    if (sender && [sender isKindOfClass:[TMRedView class]]) {
        TMRedView* tv = (TMRedView*)sender;
        if ([tv.name isEqualToString:@"edit_add.png"]) {
            if (isOpen == NO) {
                [self open];
            }
            else{
                [self close];
            }
            
        }
        else if ([tv.name isEqualToString:@"edit_share.png"]){
            if (self.delegate && [self.delegate respondsToSelector:@selector(enterShare:)]) {
                [self.delegate enterShare:nil];
            }
        }
        else if ([tv.name isEqualToString:@"edit_comment.png"]){
            if (self.delegate && [self.delegate respondsToSelector:@selector(enterSendPage:)]) {
                [self.delegate enterSendPage:nil];
            }
        }
        else if ([tv.name isEqualToString:@"edit_collect.png"]){
            if (self.delegate && [self.delegate respondsToSelector:@selector(enterCollectionPage:)]) {
                [self.delegate enterCollectionPage:nil];
            }
        }
    }
}


- (void)open{
    
    if (isAnimation) {
        return;
    }
    isAnimation = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        CGAffineTransform transform = CGAffineTransformMakeRotation((135) * M_PI/180.0);
        
        [self.addView setTransform:transform];
        
        self.shareView.hidden = NO;
        self.commentView.hidden = NO;
        self.collectView.hidden = NO;
        self.shareView.center = CGPointMake(self.addView.center.x+10, self.addView.center.y-75);
        self.commentView.center = CGPointMake(self.addView.center.x-50, self.addView.center.y-50);
        self.collectView.center = CGPointMake(self.addView.center.x-75, self.addView.center.y+10);
        
    } completion:^(BOOL finished) {
        isAnimation = NO;
        isOpen = YES;
    }];
}

- (void)close{
    
    if (isAnimation) {
        return;
    }
    isAnimation = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        CGAffineTransform transform = CGAffineTransformIdentity;
        
        [self.addView setTransform:transform];
        
        self.shareView.center = CGPointMake(self.addView.center.x, self.addView.center.y);
        self.commentView.center = CGPointMake(self.addView.center.x, self.addView.center.y);
        self.collectView.center = CGPointMake(self.addView.center.x, self.addView.center.y);
        
    } completion:^(BOOL finished) {
        self.shareView.hidden = YES;
        self.commentView.hidden = YES;
        self.collectView.hidden = YES;
        
        isAnimation = NO;
        isOpen = NO;
    }];
}


- (void)setSendCommentEnable:(BOOL)b{
    [self.commentView setEnableClick:b];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
