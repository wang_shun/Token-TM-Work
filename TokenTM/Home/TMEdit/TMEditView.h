//
//  TMEditView.h
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMEditViewDelegate;
@interface TMEditView : UIView

@property (nonatomic,weak) id <TMEditViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)setSendCommentEnable:(BOOL)b;

@end

@protocol TMEditViewDelegate <NSObject>

- (void)enterCollectionPage:(id)sender;
- (void)enterShare:(id)sender;
- (void)enterSendPage:(id)sender;


@end
