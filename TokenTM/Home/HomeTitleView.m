//
//  HomeTitleView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "HomeTitleView.h"

@implementation HomeTitleView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createTitleLabel];
    }
    return self;
}

- (void)createTitleLabel{
    _title_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.bounds.size.width, self.bounds.size.height-20)];
    [_title_label setText:@"Token.TM"];
    [_title_label setFont:[UIFont boldSystemFontOfSize:21]];
    [_title_label setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_title_label];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
