//
//  TMChannelListData.m
//  TokenTM
//
//  Created by wang shun on 2018/7/15.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelListData.h"

#import "TMListModel.h"

@interface TMChannelListData ()
{
    BOOL isloading;
}
@end

@implementation TMChannelListData


- (void)getCurIndexDataChannelName:(NSString*)channelName page:(NSString *)page Direction:(NSString*)direction Completion:(void (^)(NSArray *result))method{
    
    if (isloading) {
        return;
    }
    isloading = YES;
    
    NSString* url = [NSString stringWithFormat:@"%@/newsfeed/%@.json?openId=%@&from=channel&gesture=%@&page=%@&pageSize=%@",TMDomain,channelName,[TMUserInfo getOpenID],direction,page,@"6"];
    
    [TMNetService GET:url success:^(NSURLSessionDataTask *task, id responseObject) {
        
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSNumber* status = [responseObject objectForKey:@"statusCode"];
            if (status.integerValue == 200) {
                NSArray* dataArr = [responseObject objectForKey:@"data"];
                NSMutableArray* tmp_arr = [[NSMutableArray alloc] initWithCapacity:0];
                
                for (NSDictionary* edic in dataArr) {
                    TMListModel* model = [TMListModel getTMListModel:edic];
                    [tmp_arr addObject:model];
                }
                
                NSArray* resultArr = tmp_arr;
                if ([direction isEqualToString:@"pull-down"]) {
                    [self addListBeforeAllData:tmp_arr];
                }
                else{
                    [self addListLaterAllData:tmp_arr];
                }
                
                if (method) {
                    method(resultArr);
                }
                isloading = NO;
                
                return;
            }
        }
        
        if (method) {
            method(nil);
        }
        isloading = NO;
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (method) {
            method(nil);
        }
        isloading = NO;
    }];
}


-(void)addListBeforeAllData:(NSArray *)arr{
    if (self.mArr.count>0) {
        NSRange range = NSMakeRange(1, [arr count]);
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
        
        [self.mArr insertObjects:arr atIndexes:indexSet];
    }
    else{
        [self.mArr addObjectsFromArray:arr];
    }
    
}

- (void)addListLaterAllData:(NSArray *)arr{
    [self.mArr addObjectsFromArray:arr];
}

- (instancetype)init{
    if (self = [super init]) {
        self.mArr = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

@end

