//
//  TMHomeData.h
//  TokenTM
//
//  Created by wang shun on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChannelModel.h"
#import "TMListModel.h"
#import "TMCoinPriceModel.h"

@protocol TMHomeDataDelegate;
@interface TMHomeData : NSObject

@property (nonatomic,weak) id <TMHomeDataDelegate> delegate;

- (instancetype)init;

- (BOOL)getIsHaveDataWithCurIndexPath:(NSIndexPath*)indexPath;

- (void)getChannelData:(id)sender;

//channel bar
- (NSInteger)channelNum;
- (NSDictionary*)eachChannelData:(NSIndexPath*)indexPath;


//tables
- (NSInteger)mainViewsNum;

- (NSInteger)eachTalbleNum:(NSIndexPath*)indexPath;
- (TMListModel*)eachTalbleContentData:(NSIndexPath*)indexPath WithCurTabIndex:(NSIndexPath*)cindex;

- (void)getEachChannelTableDataIndex:(NSIndexPath*)indexPath WithPage:(NSInteger)page PullDirection:(NSString*)direction Completion:(void (^)(NSArray*result))method;
- (void)didSelectedTableCellDataIndex:(NSIndexPath*)indexPath WithCIndex:(NSIndexPath*)cindex;

- (TMChannelModel*)getChannelManagerData:(NSDictionary*)dic;
- (NSString*)getChannelName:(NSInteger)index;

- (void)getHeaderDataWith:(NSString *)coinName Completion:(void (^)(TMCoinPriceModel* model))method;

- (NSInteger)getIndexForData:(NSDictionary*)info;

- (NSDictionary*)getChannelInfoForIndex:(NSIndexPath*)indexPath;

- (void)resort;

@end


@protocol TMHomeDataDelegate <NSObject>

- (void)netFinishedChannel:(id)sender;

- (void)enterNewsDetailPage:(TMListModel*)model;

@end
