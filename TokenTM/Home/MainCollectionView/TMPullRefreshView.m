//
//  TMPullRefreshView.m
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMPullRefreshView.h"

@interface TMPullRefreshView ()
{
    BOOL isPullloading;
    BOOL isPullUploading;
    BOOL isDraged;//拉过
    NSDate* startRefreshLoadingDate;
    
    void (^reloadTable)(void);
}
@property (nonatomic,strong) UILabel* pulllabel;

@end

@implementation TMPullRefreshView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.page = 0;
        [self createView];
    }
    return self;
}

- (void)createView{
    _pulllabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.bounds.size.width, 30)];
    [_pulllabel setTextAlignment:NSTextAlignmentCenter];
    [_pulllabel setText:@"下拉刷新"];
    [_pulllabel setFont:[UIFont systemFontOfSize:13]];
    [_pulllabel setTextColor:Global_Brown_Color];
    [self addSubview:_pulllabel];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"scrollView:contentOffset:::%@",NSStringFromCGPoint(scrollView.contentOffset));
    
    if (scrollView.dragging) {
        [_pulllabel setText:@"松手可刷新"];
        isDraged = YES;
    }
    else{
        if (isPullloading) {
            return;
        }
        
        if (isPullUploading) {
            return;
        }
        
        if (isDraged == YES) {
            if (scrollView.contentOffset.y<=-65) {//下拉刷新
                isPullloading = YES;
                if (self.delegate && [self.delegate respondsToSelector:@selector(pullDownRefresh)]) {
                    [self.delegate pullDownRefresh];
                }
            }
        }
        
        if (scrollView.contentSize.height>scrollView.frame.size.height) {
            if (scrollView.contentOffset.y>=(scrollView.contentSize.height+0-scrollView.bounds.size.height)) {
                
                isPullUploading = YES;
                if (self.delegate && [self.delegate respondsToSelector:@selector(pullUpRefresh)]) {
                    [self.delegate pullUpRefresh];
                }
            }
        }
    }
}

- (void)reset{
    isPullloading = NO;
    isPullUploading = NO;
    [_pulllabel setText:@"下拉刷新"];
}
- (void)start{
    [_pulllabel setText:@"正在刷新..."];
}

- (void)startRefresh{
    [self.table_View setScrollEnabled:NO];
    [self start];
    [self.table_View setContentOffset:CGPointMake(0, -65) animated:YES];
    startRefreshLoadingDate = [NSDate date];
    [self performSelector:@selector(endRefresh:) withObject:nil afterDelay:1.5];
}

- (void)endRefresh:(void (^)(void))method{
    if (method) {
        reloadTable = method;
    }
    CGFloat f = [TMPullRefreshView getDateInterval:startRefreshLoadingDate Date2:[NSDate date]];
    if (f<1.5) {
        return;
    }
    
    [self.table_View setContentOffset:CGPointMake(0, 0) animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.table_View setScrollEnabled:YES];
        [self reset];
        
        if (reloadTable) {
            reloadTable();
            reloadTable = nil;
        }
    });
}

- (void)endPullUpRefresh:(void (^)(void))method{
    if (method) {
        reloadTable = method;
    }
    
    if (reloadTable) {
        reloadTable();
        reloadTable = nil;
    }
}



//获取时间间隔
+ (double)getDateInterval:(NSDate*)date1 Date2:(NSDate*)date2{
    // 时间1
    NSTimeZone *zone1 = [NSTimeZone systemTimeZone];
    NSInteger interval1 = [zone1 secondsFromGMTForDate:date1];
    NSDate *localDate1 = [date1 dateByAddingTimeInterval:interval1];
    
    // 时间2
    NSTimeZone *zone2 = [NSTimeZone systemTimeZone];
    NSInteger interval2 = [zone2 secondsFromGMTForDate:date2];
    NSDate *localDate2 = [date2 dateByAddingTimeInterval:interval2];
    
    // 时间2与时间1之间的时间差（秒）
    double intervalTime = [localDate2 timeIntervalSinceReferenceDate] - [localDate1 timeIntervalSinceReferenceDate];
    return intervalTime;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
