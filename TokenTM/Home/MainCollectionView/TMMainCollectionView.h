//
//  TMMainCollectionView.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMHomeData.h"

@protocol TMMainCollectionViewDelegate;
@protocol TMMainCollectionViewDataScource;

@interface TMMainCollectionView : UIView

@property (nonatomic,weak) id <TMMainCollectionViewDelegate> delegate;
@property (nonatomic,weak) id <TMMainCollectionViewDataScource> dataSource;

@property (nonatomic,strong) UICollectionView* collectionView;
@property (nonatomic,strong) NSIndexPath* curIndex;

-(instancetype)initWithFrame:(CGRect)frame;

- (void)reloadData:(id)sender;

- (void)seletecPage:(NSIndexPath*)indexPath;

@end

@protocol TMMainCollectionViewDelegate<NSObject>

- (void)clickCell:(NSDictionary*)info;

- (void)scrollViewDidScroll:(UIScrollView*)scrollView;

- (void)willDisplayCell:(id)cell forItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@protocol TMMainCollectionViewDataScource<NSObject>

- (TMHomeData*)getData;

@end
