//
//  TMPullRefreshView.h
//  TokenTM
//
//  Created by wang shun on 2018/6/9.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMPullRefreshViewDelegate;
@interface TMPullRefreshView : UIView

@property (nonatomic,weak) id <TMPullRefreshViewDelegate> delegate;

@property (nonatomic,assign) NSInteger page;
@property (nonatomic,weak) UITableView* table_View;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;

- (void)reset;
- (void)start;

- (void)startRefresh;
- (void)endRefresh:(void (^)(void))method;

- (void)endPullUpRefresh:(void (^)(void))method;

//获取时间间隔
+ (double)getDateInterval:(NSDate*)date1 Date2:(NSDate*)date2;

@end


@protocol TMPullRefreshViewDelegate<NSObject>

- (void)pullDownRefresh;

- (void)pullUpRefresh;

@end
