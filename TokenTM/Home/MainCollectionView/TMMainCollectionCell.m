//
//  TMMainCollectionCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/11.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMMainCollectionCell.h"
#import "TMListModel.h"
#import "TMBaseCell.h"
#import "TMTitleTextCell.h"
#import "TMCellManager.h"
#import "TMHomeHeadInfoView.h"
#import "TMCoinHeadInfoView.h"
#import "TMPullRefreshView.h"
#import "HomeViewController.h"

@interface TMMainCollectionCell ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,TMListCellDelegate,TMPullRefreshViewDelegate>
{
    BOOL isPullDownLoading;
}
@property (nonatomic,strong) UIView* bannerView;

@property (nonatomic,strong) UITableView* table_View;

@property (nonatomic,strong) NSMutableArray* arr;

@property (nonatomic,strong) NSMutableArray* headerArr;

@property (nonatomic,strong) TMHomeHeadInfoView* homeHeadView;
@property (nonatomic,strong) TMCoinHeadInfoView* coinHeadView;

@property (nonatomic,strong) TMPullRefreshView* pull_refreshView;

@end

@implementation TMMainCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
       
        TMHomeHeadInfoView * hview = [[TMHomeHeadInfoView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width,64)];
        hview.hidden = YES;
        [self addSubview:hview];
        self.homeHeadView = hview;
    
        TMCoinHeadInfoView * cview = [[TMCoinHeadInfoView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width,64)];
        cview.hidden = YES;
        [self addSubview:cview];
        self.coinHeadView = cview;
        self.bannerView = self.homeHeadView;

        [self createTable];
        
        isPullDownLoading = NO;
    }
    return self;
}

- (BOOL)isFirstCollectionCell{
    if (self.cur_indexPath.row == 0) {
        return YES;
    }
    return NO;
}

- (NSString *)currentCoinName{
    
    TMHomeData* home = [self getHomeData];
    if (home && self.cur_indexPath) {
        return [home getChannelName:self.cur_indexPath.row];
    }
    
    return @"";
}

- (void)createTable{
    _table_View = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bannerView.frame), self.bounds.size.width, self.bounds.size.height-CGRectGetMaxY(self.bannerView.frame)) style:UITableViewStylePlain];
    _table_View.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table_View.delegate = self;
    _table_View.dataSource = self;
    
    
    if (@available(iOS 11.0, *)) {
        self.table_View.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.table_View.estimatedRowHeight = 0;
        self.table_View.estimatedSectionFooterHeight = 0;
        self.table_View.estimatedSectionHeaderHeight = 0;
    } else {
        // Fallback on earlier versions
    }
    
    [self addSubview:_table_View];
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
    _table_View.tableFooterView = view;
    
    TMPullRefreshView* refreshView = [[TMPullRefreshView alloc] initWithFrame:CGRectMake(00, -50, self.bounds.size.width, 50)];
    [_table_View addSubview:refreshView];
    refreshView.delegate = self;
    refreshView.table_View = _table_View;
    self.pull_refreshView = refreshView;

}

#pragma mark - UITableViewDelegate,UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    TMHomeData* home = [self getHomeData];
    if (home) {
        return [home eachTalbleNum:self.cur_indexPath];
    }
    else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TMHomeData* home = [self getHomeData];
    TMListModel* model = [home eachTalbleContentData:indexPath WithCurTabIndex:self.cur_indexPath];
    
    if (model.is_fullText) {
        return model.cellFullTextHeight;
    }
    return model.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TMHomeData* home = [self getHomeData];
    TMListModel* model = nil;
    NSString *classNameStr = @"";
    if (home) {
        model = [home eachTalbleContentData:indexPath WithCurTabIndex:self.cur_indexPath];
        classNameStr = [TMListModel getCellClassName:model.templateType.integerValue];
        if (!classNameStr || [classNameStr isEqualToString:@""]) {
            NSLog(@"wangshun no have cell class");
            NSLog(@"TMListModel Cell class name::%@",classNameStr);
            exit(0);
        }
    }

    Class cellClass = NSClassFromString(classNameStr);
    
    TMBaseCell* cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@Identifier",classNameStr]];
    if (cell == nil) {
        cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%@Identifier",classNameStr]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    //NSLog(@"model.title:::%@",model.title);
    cell.delegate = self;
    [cell reloadDataWithModel:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TMHomeData* home = [self getHomeData];
    if (home) {
        [home didSelectedTableCellDataIndex:indexPath WithCIndex:self.cur_indexPath];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    TMHomeData* home = [self getHomeData];
    
    if ([home getIsHaveDataWithCurIndexPath:self.cur_indexPath]) {
        if (self.pull_refreshView) {
            [self.pull_refreshView scrollViewDidScroll:scrollView];
        }
    }
}


- (void)TMCellClickFullTextOrHold:(TMListModel *)model{
    if (!model || !_table_View) {
        return;
    }
    
    [self.table_View reloadData];
}
#pragma mark -

- (void)updateData:(id)sender{
    self.homeHeadView.hidden = NO;
    if ([self isFirstCollectionCell]) {
        self.bannerView = self.homeHeadView;
        self.homeHeadView.hidden = NO;
        self.coinHeadView.hidden = YES;
    }else{
        self.bannerView = self.coinHeadView;
        self.coinHeadView.hidden = NO;
        self.homeHeadView.hidden = YES;
    }
   
    [self.table_View reloadData];
}

-(void)willDisplayCell:(NSIndexPath *)index WithData:(id)sender{
    TMHomeData* home = [self.dataSource getData];
    
    if ([home eachTalbleNum:self.cur_indexPath] == 0) {
        [self pullDownRefresh];
    }
    
    [self getCoinData];
}

- (TMHomeData*)getHomeData{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(getData)]) {
        TMHomeData* homeData = [self.dataSource getData];
        return homeData;
    }
    else{
        return nil;
    }
}

-(NSMutableArray *)headerArr{
    if (_headerArr == nil) {
        _headerArr = [NSMutableArray array];
    }
    return _headerArr;
}

- (void)getCoinData{
    TMHomeData* home = [self getHomeData];
    
    if (home) {
        __weak typeof(self) weakSelf = self;
        if ([self isFirstCollectionCell]) {
            [home getHeaderDataWith:@"btc" Completion:^(TMCoinPriceModel* model){
                [weakSelf.headerArr addObject:model];
                if (weakSelf.headerArr.count >= 2) {
                    ((TMHomeHeadInfoView*)weakSelf.bannerView).dataArray = weakSelf.headerArr;
                }
            }];
        
            [home getHeaderDataWith:@"eth" Completion:^(TMCoinPriceModel* model){
                [weakSelf.headerArr addObject:model];
                if (weakSelf.headerArr.count >= 2) {
                    ((TMHomeHeadInfoView*)weakSelf.bannerView).dataArray = weakSelf.headerArr;
                }
            }];
        }else{
            NSLog(@"coin:::%@",[self currentCoinName]);
            [home getHeaderDataWith:[self currentCoinName] Completion:^(TMCoinPriceModel* model){
                TMHomeData* home = [self getHomeData];
                ((TMCoinHeadInfoView*)weakSelf.bannerView).priceModel = model;
                if (home.delegate && [home.delegate isKindOfClass:HomeViewController.class]) {
                    self.coinHeadView.nav = ((HomeViewController*)home.delegate).navigationController;
                }
            }];
        }
    }
}


- (void)pullDownRefresh {
    
    TMHomeData* home = [self getHomeData];
    __weak typeof(self) weakSelf = self;
    if (home) {
        
        [self.pull_refreshView startRefresh];
        //频道流数据
        NSInteger p = self.pull_refreshView.page?(self.pull_refreshView.page+1):1;//默认1
        
        [home getEachChannelTableDataIndex:self.cur_indexPath WithPage:p PullDirection:@"pull-down" Completion:^(NSArray *result) {
            
            if(result && [result isKindOfClass:[NSArray class]]){
                NSArray* arr = (NSArray*)result;
                if (arr.count>0) {
                    weakSelf.pull_refreshView.page = weakSelf.pull_refreshView.page +1;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.pull_refreshView endRefresh:^{
                    [weakSelf.table_View reloadData];
                }];
            });
        }];
    }
}

- (void)pullUpRefresh{
    TMHomeData* home = [self getHomeData];
    __weak typeof(self) weakSelf = self;
    if (home) {
        
        BOOL b = [home getIsHaveDataWithCurIndexPath:self.cur_indexPath];
        if (b == NO) {
            return;
        }
        //频道流数据
        NSInteger p = self.pull_refreshView.page?(self.pull_refreshView.page+1):1;//默认1
        
        [home getEachChannelTableDataIndex:self.cur_indexPath WithPage:p PullDirection:@"pull-up" Completion:^(NSArray *result) {
            
            if(result && [result isKindOfClass:[NSArray class]]){
                NSArray* arr = (NSArray*)result;
                if (arr.count>0) {
                   weakSelf.pull_refreshView.page = weakSelf.pull_refreshView.page +1;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.pull_refreshView endPullUpRefresh:^{
                     [weakSelf.table_View reloadData];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [weakSelf.pull_refreshView reset];
                    });
                }];
            });
        }];
    }
}




@end
