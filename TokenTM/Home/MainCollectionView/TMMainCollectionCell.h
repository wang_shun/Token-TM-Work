//
//  TMMainCollectionCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/11.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMHomeData.h"
@protocol TMMainCollectionCellDelegate;
@protocol TMMainCollectionCellDataScource;
@interface TMMainCollectionCell : UICollectionViewCell

@property (nonatomic,weak) id <TMMainCollectionCellDelegate> delegate;
@property (nonatomic,weak) id <TMMainCollectionCellDataScource> dataSource;

@property (nonatomic,strong) NSIndexPath* cur_indexPath;//当前值


- (id)initWithFrame:(CGRect)frame;

- (void)updateData:(id)sender;

- (void)willDisplayCell:(NSIndexPath*)index WithData:(id)sender;

@end

@protocol TMMainCollectionCellDelegate<NSObject>

- (void)clickCell:(NSDictionary*)dic;


@end

@protocol TMMainCollectionCellDataScource<NSObject>

- (TMHomeData*)getData;

@end
