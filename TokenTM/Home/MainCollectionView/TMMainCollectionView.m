//
//  TMMainCollectionView.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMMainCollectionView.h"
#import "TMMainCollectionCell.h"

@interface TMMainCollectionView ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,TMMainCollectionCellDelegate,TMMainCollectionCellDataScource>
@end

@implementation TMMainCollectionView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createCollectionView];
    }
    return self;
}

- (void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //设置CollectionView的属性
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = YES;
    self.collectionView.scrollsToTop = NO;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    [self addSubview:self.collectionView];
    
    //注册Cell
    [self.collectionView registerClass:[TMMainCollectionCell class] forCellWithReuseIdentifier:@"TMMainCollectionCell"];
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    TMHomeData* home = [self getHomeData];
    if (home) {
        return [home mainViewsNum];
    }
    else{
        return 0;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"TMMainCollectionCell";
    TMMainCollectionCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.cur_indexPath = indexPath;
    cell.delegate = self;
    cell.dataSource = self;
    [cell updateData:nil];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(TMMainCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    self.curIndex = indexPath;
    [cell willDisplayCell:indexPath WithData:nil];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {

}


#pragma mark  定义每个UICollectionView的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake(self.bounds.size.width,self.bounds.size.height);
}

#pragma mark  定义每个UICollectionView的横向间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark  定义每个UICollectionView的纵向间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - UICollectionViewDelegate

-(void)clickCell:(NSDictionary *)dic{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickCell:)]) {
        [self.delegate clickCell:dic];
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate scrollViewDidScroll:scrollView];
    }
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

-(void)seletecPage:(NSIndexPath *)indexPath{
//    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
     [_collectionView setContentOffset:CGPointMake(indexPath.row * CGRectGetWidth(_collectionView.frame),0) animated:NO];
}

#pragma mark - Data

- (void)reloadData:(id)sender{
    [self.collectionView reloadData];
}

- (TMHomeData*)getHomeData{
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(getData)]) {
        TMHomeData* homeData = [self.dataSource getData];
        return homeData;
    }
    else{
        return nil;
    }
}

- (TMHomeData *)getData{
    return [self getHomeData];
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
