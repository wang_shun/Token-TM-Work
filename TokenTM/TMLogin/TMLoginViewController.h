//
//  TMLoginViewController.h
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBaseViewController.h"

@interface TMLoginViewController : TMBaseViewController

@property (nonatomic,copy) void (^resultMethod)(NSDictionary* result);

- (instancetype)init;

@end
