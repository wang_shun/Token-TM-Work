//
//  TMUserInfo.m
//  TokenTM
//
//  Created by wang shun on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMUserInfo.h"
#import "AppDelegate.h"
#import "TMLoginViewController.h"

static TMUserInfo* _instance = nil;

@interface TMUserInfo ()

@property (nonatomic,strong) NSString* openid;

@property (nonatomic,strong) NSString* access_token;
@property (nonatomic,strong) NSString* expires_in;
@property (nonatomic,strong) NSString* refresh_token;
@property (nonatomic,strong) NSString* scope;
@property (nonatomic,strong) NSString* unionid;

@end

@implementation TMUserInfo

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

+ (void)setUserInfo:(NSDictionary*)u_Dic{
    TMUserInfo* userInfo = [TMUserInfo sharedInstanceTool];
    NSString* access_token = [u_Dic objectForKey:@"access_token"];
    NSString* refresh_token = [u_Dic objectForKey:@"refresh_token"];
    NSString* openid  = [u_Dic objectForKey:@"openid"];
    NSString* scope  = [u_Dic objectForKey:@"scope"];
    NSString* unionid  = [u_Dic objectForKey:@"unionid"];
    
    userInfo.access_token = access_token;
    userInfo.refresh_token = refresh_token;
    userInfo.openid = openid;
    userInfo.scope = scope;
    userInfo.unionid = unionid;
    
    [userInfo save];
}

- (void)save{
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"TM_UserInfo"];
    
    NSMutableData *data = [NSMutableData data];
    //2:创建归档对象
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    //3:开始归档
    [archiver encodeObject:self forKey:@"TMUserInfo"];
    
    [archiver finishEncoding];
    //5:写入文件当中
    BOOL result = [data writeToFile:path atomically:YES];
    if (result) {
        NSLog(@"归档成功:%@",path);
    }else
    {
        NSLog(@"归档不成功!!!");
    }
}

+ (TMUserInfo*)read{
    TMUserInfo* archiver = nil;
    
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"TM_UserInfo"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path]){
        NSData *myData = [NSData dataWithContentsOfFile:path];
        //创建反归档对象
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:myData];
        //反归档
        archiver = [unarchiver decodeObjectForKey:@"TMUserInfo"];
        //完成反归档
        [unarchiver finishDecoding];
    }
    return archiver;
}

+ (void)login:(id)sender Completion:(void (^)(NSDictionary* result))method{
    TMLoginViewController* vc = [[TMLoginViewController alloc] init];
    if (method) {
        vc.resultMethod = method;
    }
    AppDelegate* app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIViewController* root_vc = app.window.rootViewController;
    [root_vc presentViewController:vc animated:YES completion:^{
        
    }];
}

+ (BOOL)isLogin{
    
#if TARGET_IPHONE_SIMULATOR//模拟器
    return YES;
#elif TARGET_OS_IPHONE//真机
    TMUserInfo* userInfo = [TMUserInfo sharedInstanceTool];
    if (userInfo.openid && userInfo.openid.length>0) {
        return YES;
    }
    return NO;
#endif

}


+ (NSString*)getOpenID{
    
    

#if TARGET_IPHONE_SIMULATOR//模拟器
    return @"oemvi1CHzBETTkNpw4CGtnASDkYA";
#elif TARGET_OS_IPHONE//真机
    TMUserInfo* userInfo = [TMUserInfo sharedInstanceTool];
    if (userInfo.openid) {
        return userInfo.openid;
    }
    return @"";
#endif

}

+ (instancetype)sharedInstanceTool{
    @synchronized(self){
        if(_instance == nil){
            TMUserInfo* archiver = [TMUserInfo read];
            if (archiver) {
                _instance = archiver;
            }
            else{
                _instance = [[self alloc] init];
            }
        }
    }
    return _instance;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.access_token = [aDecoder decodeObjectForKey:@"access_token"];
        self.refresh_token = [aDecoder decodeObjectForKey:@"refresh_token"];
        self.openid = [aDecoder decodeObjectForKey:@"openid"];
        self.scope = [aDecoder decodeObjectForKey:@"scope"];
        self.unionid = [aDecoder decodeObjectForKey:@"unionid"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.access_token forKey:@"access_token"];
    [aCoder encodeObject:self.refresh_token forKey:@"refresh_token"];
    [aCoder encodeObject:self.openid forKey:@"openid"];
    [aCoder encodeObject:self.scope forKey:@"scope"];
    [aCoder encodeObject:self.unionid forKey:@"unionid"];
}

+ (void)removeAll{
    NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",@"TM_UserInfo"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if([manager fileExistsAtPath:path]){
        [manager removeItemAtPath:path error:nil];
    }
}

@end
