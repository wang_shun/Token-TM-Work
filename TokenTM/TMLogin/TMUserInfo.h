//
//  TMUserInfo.h
//  TokenTM
//
//  Created by wang shun on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMUserInfo : NSObject<NSCoding>

- (instancetype)init;

+ (void)setUserInfo:(NSDictionary*)userinfo;

+ (void)login:(id)sender Completion:(void (^)(NSDictionary* result))method;

+ (NSString*)getOpenID;

+ (BOOL)isLogin;

+ (TMUserInfo*)read;

+ (instancetype)sharedInstanceTool;

+ (void)removeAll;

@end
