//
//  TMLoginViewController.m
//  TokenTM
//
//  Created by wang shun on 2018/6/30.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMLoginViewController.h"
#import "WeiXinObject.h"

@interface TMLoginViewController ()

@property (nonatomic,strong) UIImageView* loginImage;
@property (nonatomic,strong) UIButton* weixinLoginBtn;

@end

@implementation TMLoginViewController

-(instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.loginImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width-80)/2.0, 90, 80, 80)];
    [self.loginImage setImage:[UIImage imageNamed:@"TMLogo80"]];
    [self.view addSubview:self.loginImage];
    self.loginImage.layer.cornerRadius = 40;
    self.loginImage.layer.masksToBounds = YES;
    
    self.weixinLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.weixinLoginBtn];
    
    [self.weixinLoginBtn setFrame:CGRectMake((self.view.bounds.size.width-80)/2.0, self.view.bounds.size.height*0.6, 80, 80)];

    [self.weixinLoginBtn setImage:[UIImage imageNamed:@"icon64_wx_logo"] forState:UIControlStateNormal];
    [self.weixinLoginBtn addTarget:self action:@selector(weixinLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.weixinLoginBtn setTitle:@"微信登录" forState:UIControlStateNormal];
    [self.weixinLoginBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.weixinLoginBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.weixinLoginBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.weixinLoginBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 20, 10)];
    
    [self.weixinLoginBtn setTitleEdgeInsets:UIEdgeInsetsMake(60, -60, 0, 0)];
    
}

- (void)weixinLoginClick:(UIButton*)b{
    [WeiXinObject login:nil Completion:^(NSDictionary *result) {
        if ([TMUserInfo isLogin]) {
            if (self.resultMethod) {
                self.resultMethod(nil);
            }
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
