//
//  TMListModel.m
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMListModel.h"
#import "TMCellManager.h"

@implementation TMListModel


+(TMListModel *)fakeModel{
    TMListModel *model = [[TMListModel alloc]init];
    model.newsId = @"172199102";
    model.newsType = @"23";
    model.templateType = @"2";
    
    model.summary = @"wangshun 推荐";
    model.title = @"【干货】币圈交易所交易费率实质与各家对比";
    model.content = @"币圈交易所交易费率实质与各家对比";
    model.translation = @"翻译内容";
    
    model.likeValue = @"2";
    model.recomTime = @"4";
    model.readTimes = @"7";
    model.shareNum = @"6";
    model.commentNum = @"5";

    model.useful = @"1234";
    model.useless = @"544";
    return model;
}

+(TMListModel *)getTMListModel:(NSDictionary *)dic{
    
    TMListModel *model = [[TMListModel alloc] init];
    
    
    model.newsId = [dic objectForKey:@"newsId"];
    model.newsType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"newsType"]];
    model.templateType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"templateType"]];
    
    model.summary = [dic objectForKey:@"summary"]?:@"";
    model.title = [dic objectForKey:@"title"]?:@"";
    model.content = [dic objectForKey:@"content"]?:@"";
    
    //翻译内容
    model.translation = [dic objectForKey:@"translation"]?:@"";
    model.zhTitle = [dic objectForKey:@"zhTitle"]?:@"";
    
    model.pics = [dic objectForKey:@"pics"];
    model.likeValue = @"2";
    model.recomTime = @"4";
    model.readTimes = @"7";
    model.shareNum = [dic objectForKey:@"shareNum"]?[NSString stringWithFormat:@"%@",[dic objectForKey:@"shareNum"]]:@"0";
    model.commentNum = [dic objectForKey:@"commentNum"]?[NSString stringWithFormat:@"%@",[dic objectForKey:@"commentNum"]]:@"0";
    
    model.shareImage = [dic objectForKey:@"shareImage"]?[NSString stringWithFormat:@"%@",[dic objectForKey:@"shareImage"]]:@"";
    
    model.docId = [dic objectForKey:@"docId"];
    model.userInfo = [dic objectForKey:@"userInfo"];
    model.useful = [dic objectForKey:@"useful"]?[NSString stringWithFormat:@"%@",[dic objectForKey:@"useful"]]:@"0";
    model.useless = [dic objectForKey:@"useless"]?[NSString stringWithFormat:@"%@",[dic objectForKey:@"useless"]]:@"0";
    
    [[TMCellManager shareInstance]  configHeightForCellWithData:model];
    
    return model;
}

+ (NSString *)getCellClassName:(NSInteger)templateNum{
    NSString* classNameStr = @"";
    
    switch (templateNum) {
        case TMCellStyleTitleAndRightImage:
            classNameStr = @"TMTitleImageCell";
            break;
        case TMCellStyleContentAndDownImage:
            classNameStr = @"TMTitleDownImageCell";
            break;
        case TMCellStyleTitleAndText:
            classNameStr = @"TMTitleTextCell";
            break;
        default:
            
            break;
    }
    
    return classNameStr;
}

/*
 
 {
 commentNum = 0;
 docId = "ff94b78a1dd9edad-a78df19dff20b399";
 history = 0;
 likeValue = 0;
 newsId = 2147460970572677121;
 newsType = 0;
 pics =             (
 "http://img.dj.gl/CE6BC42A3B83197A305CCABE78559D7F.jpg?x-oss-process=image/resize,m_lfit,h_160,w_250",
 "http://img.dj.gl/7AC05D1F0FB3FA28431323EFA40C45FD.jpg?x-oss-process=image/resize,m_lfit,h_160,w_250",
 "http://img.dj.gl/E8EE8CBE83E1AB43C8BCF5F5D70EFD12.jpg?x-oss-process=image/resize,m_lfit,h_160,w_250"
 );
 publishTime = 1526172937000;
 readTimes = 0;
 recomTime = 0;
 shareImage = "http://img.dj.gl/CE6BC42A3B83197A305CCABE78559D7F.jpg";
 shareNum = 0;
 templateType = 1;
 time = "2018-05-13 08:55";
 title = "\U300c\U6280\U672f\U5206\U6790\U300dEOS - LTC - XLM - TRX - IOTA / USD";
 useful = 0;
 useless = 0;
 userInfo =             {
 avatarUrl = "http://img.dj.gl/token_default_icon.jpg";
 gender = 0;
 nickName = "Token.TM";
 type = 0;
 };
 }

 
 */


@end
