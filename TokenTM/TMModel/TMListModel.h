//
//  TMListModel.h
//  TokenTM
//
//  Created by qz on 2018/5/12.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TMListModel : NSObject

@property (nonatomic,copy) NSString *newsId;
@property (nonatomic,copy) NSString *summary;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *translation;

@property (nonatomic,copy) NSString *newsType;
@property (nonatomic,copy) NSString *templateType;

@property (nonatomic,copy) NSString *publishTime;//发布时间

@property (nonatomic,copy) NSString *likeValue;//点赞数
@property (nonatomic,copy) NSString *recomTime;//推荐次数
@property (nonatomic,copy) NSString *readTimes;//阅读次数
@property (nonatomic,copy) NSString *shareNum;//分享次数
@property (nonatomic,copy) NSString *commentNum;//评论数

@property (nonatomic,copy) NSString *useful;//有用数
@property (nonatomic,copy) NSString *useless;//无用数

@property (nonatomic,copy) NSString *shareImage;//分享图片链接
@property (nonatomic,strong) NSDictionary *userInfo;
@property (nonatomic,strong) NSArray *pics;

@property (nonatomic,copy) NSString *zhTitle;//翻译后的内容
@property (nonatomic,copy) NSString *media;//不知道啥意思
@property (nonatomic,assign) BOOL history;//不知道啥意思
@property (nonatomic,copy) NSString *docId;//不知道啥意思
@property (nonatomic,copy) NSString *time;//不知道啥意思

//UI 层
@property (nonatomic,assign) BOOL is_fullText;
@property (nonatomic,assign) BOOL can_fullText;
@property (nonatomic,assign) CGFloat cellHeight;//不展开时候的cell高度
@property (nonatomic,assign) CGFloat cellFullTextHeight;//展开时候的cell高度
@property (nonatomic,assign) CGFloat titleHeight;//标题高度
@property (nonatomic,assign) CGFloat fullTextHeight;//全文text高度
@property (nonatomic,assign) CGFloat translationHeight;//全文text高度
@property (nonatomic,assign) CGFloat picHeight;//全文text高度

+(TMListModel *)fakeModel;

+ (TMListModel *)getTMListModel:(NSDictionary*)dic;

+ (NSString*)getCellClassName:(NSInteger)templateNum;

@end


//{
//    "commentNum": 0,
//    "content": "string",
//    "docId": "string",
//    "history": true,
//    "likeValue": 0,
//    "media": "string",
//    "newsId": "string",
//    "newsType": 0,
//    "pics": [
//             "string"
//             ],
//    "publishTime": 0,
//    "readTimes": 0,
//    "recomTime": 0,
//    "shareImage": "string",
//    "shareNum": 0,
//    "summary": "string",
//    "templateType": 0,
//    "time": "string",
//    "title": "string",
//    "translation": "string",
//    "useful": 0,
//    "useless": 0,
//    "userInfo": {
//        "avatarUrl": "string",
//        "gender": 0,
//        "nickName": "string",
//        "openId": "string",
//        "type": 0
//    },
//    "zhTitle": "string"
//}

