//
//  TMChannelSelectedBtn.h
//  TokenTM
//
//  Created by wang shun on 2018/5/19.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMChannelSelectedBtnDelegate;
@interface TMChannelSelectedBtn : UIView

@property (nonatomic,weak) id <TMChannelSelectedBtnDelegate> delegate;

@property (nonatomic,strong) NSDictionary* info;


@property (nonatomic,assign) BOOL notSupportingSort;
@property (nonatomic,assign) BOOL isLongPress;
@property (nonatomic,assign) CGPoint beginCenter;
@property (nonatomic,assign) NSTimeInterval lastBeiginTouch;

@property (nonatomic,assign) BOOL isMyChannel;


- (void)setBtnInfo:(NSDictionary*)dic;

@end

@protocol TMChannelSelectedBtnDelegate <NSObject>

- (void)lockScrollView;
- (void)unLockScrollView;

- (void)startMove:(TMChannelSelectedBtn*)view;
- (void)didMove:(TMChannelSelectedBtn*)view;
- (void)endMove:(TMChannelSelectedBtn*)view;

- (BOOL)isAllowDrag;


- (void)clickItem:(NSDictionary*)info;

@end
