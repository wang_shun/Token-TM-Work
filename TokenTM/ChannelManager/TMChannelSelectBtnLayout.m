//
//  TMChannelSelectBtnLayout.m
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelSelectBtnLayout.h"

@implementation TMChannelSelectBtnLayout

- (instancetype)init{
    if (self = [super init]) {
        self.mArr = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

-(void)removeAll{
    [self.mArr removeAllObjects];
}

- (void)addObjectsFromArray:(NSArray *)arr{
    if (arr.count>0) {
        for (int i = 0 ; i<arr.count ; i++) {
            TMChannelSelectedBtnLayoutItem* item = [[TMChannelSelectedBtnLayoutItem alloc] init];
            [self.mArr addObject:item];
        }
        
        [self calculateAllGuestViews];
    }
}

- (void)addObject:(TMChannelSelectedBtnLayoutItem*)item{
    [self.mArr addObject:item];
}

-(void)layoutAnnimation:(void (^)(void))completion{
    [self calculateAllGuestViews];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutAllGuestViews];
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
}

- (void)layoutAllGuestViews {
    for (TMChannelSelectedBtnLayoutItem *hd in self.mArr) {
        [hd letitgo];
    }
}

- (void)removeCurMove:(TMChannelSelectedBtn *)view{
    
    TMChannelSelectedBtnLayoutItem* remove_Item = nil;
    for (TMChannelSelectedBtnLayoutItem* item in self.mArr) {
        if (item.channelView == view) {
            remove_Item = item;
        }
    }
    
    if (remove_Item) {
        self.movingItem = remove_Item;
        [self.mArr removeObject:remove_Item];
    }
}

- (void)calculateAllGuestViews {
    CGFloat x = 25;
    CGFloat y = 50;
    CGFloat space = 10;
    CGFloat w = ([UIScreen mainScreen].bounds.size.width-x-x - space*3)/4.0;
    CGFloat h = 30;
    
    
    for (int i = 0; i< self.mArr.count; i++) {
        TMChannelSelectedBtnLayoutItem* item = [self.mArr objectAtIndex:i];

        x = (i%4)*(w+space)+25;
        y = (i/4)*(h+space)+50;
        CGRect rect = CGRectMake(x,y, w, h);
        item.rectValue = [NSValue valueWithCGRect:rect];
        
        CGPoint point = CGPointMake(x + w/2.0, y+ h/2.0);
        item.centerValue = [NSValue valueWithCGPoint:point];
    }
}

- (void)receiveHitAtPointChannelView:(TMChannelSelectedBtn *)view {
    if (view && [view isKindOfClass:[TMChannelSelectedBtn class]]) {
        NSInteger index = [self indexForPoint:view.center];
        
        for (NSInteger _index = index; _index < self.mArr.count; _index++) {
            //获取不支持排序的Index
            TMChannelSelectedBtnLayoutItem *item = self.mArr[_index];
            if (!item.channelView.notSupportingSort) {
                index = _index;
                break;
            }
        }
        
        if (self.movingItem) {
            if (self.movingItem.channelView == view) {
                if (index >= self.mArr.count) {
                    [self.mArr addObject:self.movingItem];
                } else {
                    [self.mArr insertObject:self.movingItem atIndex:index];
                }
                self.movingItem = nil;
            }
        }
    }
}

#pragma mark - private

- (int)indexForPoint:(CGPoint)pos {
    
    int index = -1;
    int xIndex = 0;
    int yIndex = 0;
    BOOL isLineTail = NO;
    
    CGFloat x = 25;
//    CGFloat y = 50;
    CGFloat space = 10;
    CGFloat w = ([UIScreen mainScreen].bounds.size.width-x-x - space*3)/4.0;
    CGFloat h = 30;

    CGFloat dx = w ;
    CGFloat dy = h + space;
    CGFloat originY =  space+ space + h;

    CGFloat originX = x;
    CGFloat xOffset = pos.x - originX;
    CGFloat yOffset = pos.y - originY;

    if (xOffset < dx) {
        xIndex = 0;
    }
    else if (xOffset < (originX + 4 * dx)) {
        xIndex = (int)xOffset / (int)dx;
    }
    else {
        xIndex = 0;
        isLineTail = YES;
    }

    if (yOffset < dy) {
        yIndex = 0;
    }
    else {
        yIndex = (int)yOffset / (int)dy;
    }

    if (isLineTail) {
        yIndex++;
    }

    index = yIndex * 4 + xIndex;
    
    return index;
}



-(NSArray *)finishedArr{
    NSMutableArray* fArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i<self.mArr.count; i++) {
        TMChannelSelectedBtnLayoutItem* item = [self.mArr objectAtIndex:i];
        NSDictionary* dic = item.channelView.info;
        [fArr addObject:dic];
    }
//    NSLog(@"finishedArr:::%@",fArr);
    return fArr;
}

@end
