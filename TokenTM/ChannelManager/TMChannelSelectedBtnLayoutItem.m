//
//  TMChannelSelectedBtnLayoutItem.m
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelSelectedBtnLayoutItem.h"

@implementation TMChannelSelectedBtnLayoutItem


- (void)letitgo{
    CGPoint center = [self.centerValue CGPointValue];
    self.channelView.center = center;
}


@end
