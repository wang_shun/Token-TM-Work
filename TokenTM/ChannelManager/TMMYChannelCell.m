//
//  TMMYChannelCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMMYChannelCell.h"

#import "TMChannelCellTitle.h"
#import "TMChannelEditBtn.h"
#import "TMChannelSelectedBtn.h"
#import "TMChannelSelectBtnLayout.h"

@interface TMMYChannelCell ()<TMChannelSelectedBtnDelegate,TMChannelEditBtnDelegate>

@property (nonatomic,strong) TMChannelSelectBtnLayout* layout;

@property (nonatomic,strong) TMChannelEditBtn* editBtn;

@end

@implementation TMMYChannelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        _users = [[NSMutableArray alloc] initWithCapacity:0];
        self.layout = [[TMChannelSelectBtnLayout alloc] init];
        self.isADDed = NO;
        [self createTitle];
        [self createEdit];
    }
    return self;
}


- (void)createTitle{
    TMChannelCellTitle* titleView = [[TMChannelCellTitle alloc] initWithFrame:CGRectMake(0, 0, self.cell_Frame.size.width, 40)];
    [titleView setTitle:@"已选频道"];
    [titleView setsubTitle:@"长按排序或删除"];
    titleView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:titleView];
}

- (void)createEdit{
    CGFloat x = self.cell_Frame.size.width-85;
    CGFloat y = (40-30)/2.0;
    self.editBtn = [[TMChannelEditBtn alloc] initWithFrame:CGRectMake(x, y, 85, 30)];
    [self.contentView addSubview:self.editBtn];
    self.editBtn.delegate = self;
}

- (void)editPress:(id)sender{
    if (self.mychannelDelegate && [self.mychannelDelegate respondsToSelector:@selector(selectEdit:WithUsers:)]) {
        if (sender && [sender isKindOfClass:[UIButton class]]) {
            UIButton* b = sender;
            if (b.selected == NO) {
                NSArray* usersArr = [self.layout finishedArr];
                [self.mychannelDelegate selectEdit:sender WithUsers:usersArr];
            }
        }
    }
}

- (BOOL)isEditingStatus{
    return [self.editBtn isEditStatus];
}

- (void)createChannels{
    
    CGFloat x = 25;
    CGFloat y = 50;
    CGFloat space = 10;
    CGFloat w = (self.cell_Frame.size.width-25-25 - space*3)/4.0;
    CGFloat h = 30;
    
    for (int i = 0; i<_users.count; i++) {
        x = (i%4)*(w+space)+25;
        y = (i/4)*(h+space)+50;
        TMChannelSelectedBtn* eachBtn = [[TMChannelSelectedBtn alloc] initWithFrame:CGRectMake(x, y, w, h)];
        NSDictionary* info = [_users objectAtIndex:i];
        [eachBtn setBtnInfo:info];
        eachBtn.isMyChannel = YES;
        eachBtn.delegate = self;
        [self.contentView addSubview:eachBtn];
        TMChannelSelectedBtnLayoutItem* item = [[TMChannelSelectedBtnLayoutItem alloc] init];
        item.channelView = eachBtn;
        item.rectValue = [NSValue valueWithCGRect:eachBtn.frame];
        item.centerValue = [NSValue valueWithCGPoint:eachBtn.center];
        [self.layout addObject:item];
        
    }
    self.isADDed = YES;
}

-(void)setCellInfo:(NSDictionary *)cell_info{
    if (cell_info != self.info) {
        self.info = cell_info;
        if (self.isADDed == YES) {
            return;
        }
        else{
            for (UIView* v in self.contentView.subviews) {
                if ([v isKindOfClass:[TMChannelSelectedBtn class]]) {
                    [v removeFromSuperview];
                }
            }
            [self.layout removeAll];
        }
        NSArray* arr = [self.info objectForKey:@"userChannels"];
        if (arr && arr.count>0) {
            _users = [NSArray arrayWithArray:arr];
            [self createChannels];
        }
    }
}

+ (CGFloat)getHeight:(NSInteger)num{
    int n = 0;
    if ((int)(num%4)>0) {
        n = 1;
    }
        
    int line = (int)((int)(num/4)+n);
        
    CGFloat f = line*(30+10)+50;
    return f;
}

- (void)clickItem:(NSDictionary *)info{
    if (![self isAllowDrag]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickChannelItem:)]) {
            [self.delegate clickChannelItem:info];
        }
    }
    else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(removeChannelItem:)]) {
            [self.delegate removeChannelItem:info];
        }
    }
}

-(void)lockScrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(lockScrollView)]) {
        [self.delegate lockScrollView];
    }
}

-(void)unLockScrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(unLockScrollView)]) {
        [self.delegate unLockScrollView];
    }
}


-(void)startMove:(TMChannelSelectedBtn *)view{
    if ([view isDescendantOfView:self]) {
        [self.contentView bringSubviewToFront:view];
        [self.layout removeCurMove:view];
    }
}

-(void)didMove:(TMChannelSelectedBtn *)view{
    if ([view isDescendantOfView:self]) {
//        NSLog(@"didMove:");
        [self.layout layoutAnnimation:nil];
    }
}

-(void)endMove:(TMChannelSelectedBtn *)view{
    if ([view isDescendantOfView:self]) {
//        NSLog(@"endMove:");
        [self.layout receiveHitAtPointChannelView:view];
        [self.layout layoutAnnimation:nil];
    }
}

-(BOOL)isAllowDrag{
    return [self isEditingStatus];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
