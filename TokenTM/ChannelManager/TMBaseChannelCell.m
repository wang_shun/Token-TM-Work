//
//  TMBaseChannelCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMBaseChannelCell.h"

@implementation TMBaseChannelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.cell_Frame = rect;
    }
    return self;
}

- (void)setCellInfo:(NSDictionary *)cell_info{
    if (cell_info != self.info) {
        self.info = cell_info;
    }
}

+ (CGFloat)getHeight:(NSInteger)num{
    return 50;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
