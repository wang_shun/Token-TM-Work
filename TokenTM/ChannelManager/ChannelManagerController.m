//
//  ChannelManagerController.m
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "ChannelManagerController.h"
#import "TMBaseChannelCell.h"
#import "TMMYChannelCell.h"
#import "TMAddChannelCell.h"
#import "TMChannelModel.h"

@interface ChannelManagerController ()<UITableViewDelegate,UITableViewDataSource,TMBaseChannelCellDelegate,TMMYChannelCellDelegate>

@property (nonatomic,strong) UITableView* table_View;

@property (nonatomic,strong) TMChannelModel* channelMData;

@property (nonatomic,strong) NSMutableArray* recomChannels;
@property (nonatomic,strong) NSMutableArray* userChannels;

@end

@implementation ChannelManagerController

- (instancetype)initWthChannelData:(id)data{
    if (self = [super init]) {
        if (data && [data isKindOfClass:[TMChannelModel class]]) {
            self.channelMData = (TMChannelModel*)data;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"主题定制";
    
    [self createTable];
    
    [self createNavigationBar];
}

- (void)createTable{
    
    _table_View = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationBar.frame), self.view.bounds.size.width, self.view.bounds.size.height-CGRectGetMaxY(self.navigationBar.frame)) style:UITableViewStylePlain];
    
    _table_View.delegate = self;
    _table_View.dataSource = self;
    
    _table_View.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_table_View];
    
    UIView* v = [[UIView alloc] initWithFrame:CGRectZero];
    self.table_View.tableFooterView = v;
}


#pragma mark - UITableViewDelegate && UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (TMBaseChannelCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* cell_Identifier = @"TMMYChannelCell";
    if (indexPath.row == 1) {
        cell_Identifier = @"TMAddChannelCell";
    }
    
    TMBaseChannelCell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
    if (cell == nil) {
        Class class= NSClassFromString(cell_Identifier);
        cell = [[class alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier Frame:CGRectMake(0, 0, self.view.bounds.size.width, 0)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    
    NSDictionary* dic = nil;
    if ([cell_Identifier isEqualToString:@"TMMYChannelCell"]) {
        if (self.channelMData.userChannels && self.channelMData.userChannels.count>0) {
            dic = @{@"userChannels":self.channelMData.userChannels};
        }
        TMMYChannelCell* mcell = (TMMYChannelCell*)cell;
        mcell.mychannelDelegate = self;
    }
    else if ([cell_Identifier isEqualToString:@"TMAddChannelCell"]){
        if (self.channelMData.recomChannels && self.channelMData.recomChannels.count>0) {
            dic = @{@"recomChannels":self.channelMData.recomChannels};
        }
    }
    
    [cell setCellInfo:dic];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row ==0) {
        CGFloat height =[TMMYChannelCell getHeight:self.channelMData.userChannels.count];
        return height;
    }
    else{
        CGFloat height =[TMAddChannelCell getHeight:self.channelMData.recomChannels.count];
        return height;
    }
}


-(void)clickChannelItem:(NSDictionary *)info{
    NSLog(@"channel vc :selected::%@", info);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(channelItemClick:)]) {
        [self.delegate channelItemClick:info];
    }
    
    [self close];
}

-(void)close{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)lockScrollView{
    _table_View.scrollEnabled = NO;
}

-(void)unLockScrollView{
    _table_View.scrollEnabled = YES;
}

#pragma mark - didReceiveMemoryWarning

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TMBaseChannelCellDelegate

- (void)addChannelItem:(NSDictionary *)dic{
    NSLog(@"add channel item dic:::%@",dic);
    
    [self.channelMData.userChannels addObject:dic];
    
    NSInteger remove_Index = -1;
    for (int i=0; i<self.channelMData.recomChannels.count ;i++) {
        NSDictionary* re_dic = [self.channelMData.recomChannels objectAtIndex:i];
        NSString* channelName = [re_dic objectForKey:@"channelName"];
        NSString* channelName_add = [dic objectForKey:@"channelName"];
        if ([channelName isEqualToString:channelName_add]) {
            remove_Index = i;
        }
    }
    
    [self.channelMData.recomChannels removeObjectAtIndex:remove_Index];
    
    TMBaseChannelCell* cell = [self.table_View cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.isADDed = NO;
    cell = [self.table_View cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    cell.isADDed = NO;
    
    [_table_View reloadData];
    
    [self saveData];
}

-(void)removeChannelItem:(NSDictionary *)dic{
    NSLog(@"removeChannelItem channel item dic:::%@",dic);
    
    [self.channelMData.recomChannels addObject:dic];
    
    NSInteger remove_Index = -1;
    for (int i=0; i<self.channelMData.userChannels.count ;i++) {
        NSDictionary* re_dic = [self.channelMData.userChannels objectAtIndex:i];
        NSString* channelName = [re_dic objectForKey:@"channelName"];
        NSString* channelName_add = [dic objectForKey:@"channelName"];
        if ([channelName isEqualToString:channelName_add]) {
            remove_Index = i;
        }
    }
    
    [self.channelMData.userChannels removeObjectAtIndex:remove_Index];
    
    TMBaseChannelCell* cell = [self.table_View cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.isADDed = NO;
    cell = [self.table_View cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    cell.isADDed = NO;
    
    [_table_View reloadData];
    
    [self saveData];
}

-(void)selectEdit:(id)sender WithUsers:(NSArray *)arr{
    if (sender && [sender isKindOfClass:[UIButton class]]) {
        UIButton* b = sender;
        if (b.selected == YES) {
            
        }
        else{
            self.channelMData.userChannels = [[NSMutableArray alloc] initWithArray:arr];
            [self saveData];
        }
    }
}

- (void)saveData{
    [self.channelMData save];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(refreshHomeChannelList:)]) {
        [self.delegate refreshHomeChannelList:nil];
    }
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
