//
//  TMBaseChannelCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMBaseChannelCellDelegate;
@interface TMBaseChannelCell : UITableViewCell

@property (nonatomic,assign) CGRect cell_Frame;
@property (nonatomic,assign) BOOL isADDed;
@property (nonatomic,strong) NSDictionary* info;

@property (nonatomic,weak) id <TMBaseChannelCellDelegate> delegate;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect) rect;

- (void)setCellInfo:(NSDictionary*)cell_info;

+ (CGFloat)getHeight:(NSInteger)num;

@end

@protocol TMBaseChannelCellDelegate <NSObject>

- (void)clickChannelItem:(NSDictionary*)info;

- (void)removeChannelItem:(NSDictionary*)dic;
- (void)addChannelItem:(NSDictionary*)dic;

- (void)lockScrollView;
- (void)unLockScrollView;

@end
