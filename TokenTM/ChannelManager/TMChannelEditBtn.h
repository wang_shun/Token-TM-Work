//
//  TMChannelEditBtn.h
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TMChannelEditBtnDelegate;
@interface TMChannelEditBtn : UIView

@property (nonatomic,weak) id <TMChannelEditBtnDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame;

- (BOOL)isEditStatus;
@end

@protocol TMChannelEditBtnDelegate <NSObject>

- (void)editPress:(id)sender;

@end
