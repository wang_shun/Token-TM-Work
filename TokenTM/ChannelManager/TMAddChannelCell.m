//
//  TMAddChannelCell.m
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMAddChannelCell.h"
#import "TMChannelCellTitle.h"
#import "TMChannelSelectedBtn.h"

@interface TMAddChannelCell ()<TMChannelSelectedBtnDelegate,TMBaseChannelCellDelegate>

@end

@implementation TMAddChannelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Frame:(CGRect)rect{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier Frame:rect]) {
        self.isADDed = NO;
        [self createTitle];
    }
    return self;
}

- (void)createTitle{
    TMChannelCellTitle* titleView = [[TMChannelCellTitle alloc] initWithFrame:CGRectMake(0, 0, self.cell_Frame.size.width, 40)];
    [titleView setTitle:@"添加频道"];
    [titleView setsubTitle:@"点击即可快速添加"];
    titleView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:titleView];
}

- (void)createChannels{
    CGFloat x = 25;
    CGFloat y = 50;
    CGFloat space = 10;
    CGFloat w = (self.cell_Frame.size.width-25-25 - space*3)/4.0;
    CGFloat h = 30;
    
    for (int i = 0; i<_recommonds.count; i++) {
        x = (i%4)*(w+space)+25;
        y = (i/4)*(h+space)+50;
        TMChannelSelectedBtn* eachBtn = [[TMChannelSelectedBtn alloc] initWithFrame:CGRectMake(x, y, w, h)];
        NSDictionary* info = [_recommonds objectAtIndex:i];
        [eachBtn setBtnInfo:info];
        eachBtn.delegate = self;
        [self.contentView addSubview:eachBtn];
    }
    self.isADDed = YES;
}

- (void)clickItem:(NSDictionary *)info{
    if (self.delegate && [self.delegate respondsToSelector:@selector(addChannelItem:)]) {
        [self.delegate addChannelItem:info];
    }
}

- (void)setCellInfo:(NSDictionary *)cell_info{
    if (cell_info != self.info) {
        self.info = cell_info;
        if (self.isADDed == YES) {
            return;
        }
        else{
            for (UIView* v in self.contentView.subviews) {
                if ([v isKindOfClass:[TMChannelSelectedBtn class]]) {
                    [v removeFromSuperview];
                }
            }
        }
        
        NSArray* arr = [self.info objectForKey:@"recomChannels"];
        if (arr && arr.count>0) {
            _recommonds = [NSArray arrayWithArray:arr];
            [self createChannels];
        }
    }
}


+ (CGFloat)getHeight:(NSInteger)num{
    int n = 0;
    if ((int)(num%4)>0) {
        n = 1;
    }
    
    int line = (int)((int)(num/4)+n);
    
    CGFloat f = line*(30+10)+50;
    return f;
}

- (void)clickChannelItem:(NSDictionary *)info{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickChannelItem:)]) {
        [self.delegate clickChannelItem:info];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
