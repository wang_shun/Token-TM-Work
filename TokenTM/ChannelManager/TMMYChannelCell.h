//
//  TMMYChannelCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseChannelCell.h"
@protocol TMMYChannelCellDelegate;
@interface TMMYChannelCell : TMBaseChannelCell

@property (nonatomic,strong) NSArray* users;
@property (nonatomic,weak) id <TMMYChannelCellDelegate> mychannelDelegate;

@end

@protocol TMMYChannelCellDelegate<NSObject>

- (void)selectEdit:(id)sender WithUsers:(NSArray*)arr;

@end
