//
//  TMChannelCellTitle.m
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelCellTitle.h"

@interface TMChannelCellTitle ()

@property (nonatomic,strong) UILabel* title_Label;
@property (nonatomic,strong) UILabel* sub_title_Label;

@end

@implementation TMChannelCellTitle

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _title_Label = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 65, self.bounds.size.height)];
        _title_Label.textColor = RGBACOLOR(187, 160, 113, 1);
        _title_Label.font = [UIFont systemFontOfSize:15];
        [self addSubview:_title_Label];
        
        
        CGFloat x = CGRectGetMaxX(_title_Label.frame)+10;
        CGFloat h = 16;
        CGFloat y = (self.bounds.size.height-h)/2.0+2;
        _sub_title_Label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, self.bounds.size.width-(x), 16)];
        _sub_title_Label.textColor = RGBACOLOR(142, 142, 142, 1);
        _sub_title_Label.font = [UIFont systemFontOfSize:13];
        _sub_title_Label.backgroundColor = [UIColor clearColor];
        [self addSubview:_sub_title_Label];
        
    }
    return self;
}

- (void)setTitle:(NSString*)title{
    [_title_Label setText:title];
}

- (void)setsubTitle:(NSString*)subtitle{
    [_sub_title_Label setText:subtitle];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
