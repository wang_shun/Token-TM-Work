//
//  TMChannelSelectedBtnLayoutItem.h
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TMChannelSelectedBtn.h"

@interface TMChannelSelectedBtnLayoutItem : NSObject

@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSValue* rectValue;
@property (nonatomic,strong) NSValue* centerValue;

//@property (nonatomic,strong) SNVideoChannelModel* videoChannelModel;

@property (nonatomic,strong) TMChannelSelectedBtn* channelView;

- (void)letitgo;

@end
