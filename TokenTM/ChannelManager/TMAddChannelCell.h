//
//  TMAddChannelCell.h
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMBaseChannelCell.h"

@interface TMAddChannelCell : TMBaseChannelCell

@property (nonatomic,strong) NSArray* recommonds;


-(void)setCellInfo:(NSDictionary *)cell_info;

@end
