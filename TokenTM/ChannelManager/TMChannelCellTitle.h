//
//  TMChannelCellTitle.h
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMChannelCellTitle : UIView

- (void)setTitle:(NSString*)title;
- (void)setsubTitle:(NSString*)subtitle;

@end
