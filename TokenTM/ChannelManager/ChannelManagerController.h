//
//  ChannelManagerController.h
//  TokenTM
//
//  Created by wang shun on 2018/5/10.
//  Copyright © 2018年 wang shun. All rights reserved.
//

#import "TMBaseViewController.h"
@protocol ChannelManagerControllerDelegate;
@interface ChannelManagerController : TMBaseViewController

@property (nonatomic,weak) id <ChannelManagerControllerDelegate> delegate;

- (instancetype)initWthChannelData:(id)data;

@end

@protocol ChannelManagerControllerDelegate <NSObject>

- (void)channelItemClick:(NSDictionary*)dic;
- (void)refreshHomeChannelList:(id)sender;

@end
