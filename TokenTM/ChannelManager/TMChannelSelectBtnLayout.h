//
//  TMChannelSelectBtnLayout.h
//  TokenTM
//
//  Created by wang shun on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMChannelSelectedBtnLayoutItem.h"
#import "TMChannelSelectedBtn.h"

@interface TMChannelSelectBtnLayout : NSObject

@property (nonatomic,strong) NSMutableArray <id > *mArr;

@property (nonatomic,strong) TMChannelSelectedBtnLayoutItem* movingItem;

- (instancetype)init;

- (void)removeAll;

- (void)addObject:(TMChannelSelectedBtnLayoutItem*)item;

- (void)layoutAnnimation:(void (^)(void))completion;

//移除当前已经提出来的
- (void)removeCurMove:(TMChannelSelectedBtn *)view;

- (void)receiveHitAtPointChannelView:(TMChannelSelectedBtn*)view;

//完成时顺序
- (NSArray*)finishedArr;

@end
