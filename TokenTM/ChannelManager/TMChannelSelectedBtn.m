//
//  TMChannelSelectedBtn.m
//  TokenTM
//
//  Created by wang shun on 2018/5/19.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelSelectedBtn.h"

@interface TMChannelSelectedBtn ()

@property (nonatomic,strong) UIView* bgView;
@property (nonatomic,strong) UILabel* title_label;

@property (nonatomic,strong) UIView* cover_View;

@property (nonatomic,strong) UIButton* btn;

@end

@implementation TMChannelSelectedBtn

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

- (void)createView{
    self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    [self.bgView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.bgView];
    
    self.bgView.layer.cornerRadius = self.bounds.size.height/2.0;
    self.bgView.layer.borderColor = RGBACOLOR(187, 160, 113, 1).CGColor;
    self.bgView.layer.borderWidth = TMOnePixel;
    
    self.title_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    self.title_label.text = @"";
    self.title_label.textAlignment = NSTextAlignmentCenter;
    self.title_label.backgroundColor = [UIColor clearColor];
    self.title_label.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.title_label.textColor = RGBACOLOR(187, 160, 113, 1);
    [self addSubview:self.title_label];
    
//    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.btn setFrame:self.bounds];
//    [self.btn setBackgroundColor:[UIColor clearColor]];
//    [self.btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:self.btn];
    
    
    self.cover_View = [[UIView alloc] initWithFrame:self.bounds];
    self.cover_View.backgroundColor = [UIColor whiteColor];
    self.cover_View.alpha = 0.5;
    [self addSubview:self.cover_View];
    self.cover_View.userInteractionEnabled = YES;
    self.cover_View.hidden = YES;
}

- (void)click:(UIButton*)b{
//    NSLog(@"channel item click:%@",self.info);
    
    if (self.isMyChannel == YES) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickItem:)]) {
            [self.delegate clickItem:self.info];
        }
    }
}

-(void)setBtnInfo:(NSDictionary *)dic{
    if (dic) {
        self.info = dic;
        NSString* title = [self.info objectForKey:@"nickName"];
        [self.title_label setText:title];
        
        NSString* type = [self.info objectForKey:@"type"];
        if ([type isEqualToString:@"index"]) {
            self.cover_View.hidden = NO;
        }
        else{
            self.cover_View.hidden = YES;
        }
        
        if (self.cover_View.hidden == NO) {
            self.notSupportingSort = YES;
        }
        else{
            self.notSupportingSort = NO;
        }
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    if (![self isAllow]) {
        return;
    }
    
    CGFloat delayTime = 0.25;
    [self performSelector:@selector(activeLongPress:) withObject:nil afterDelay:delayTime];
    
    [self lock];
    
    UITouch *aTouch = [touches anyObject];
    self.beginCenter = [aTouch locationInView:self.superview];
    self.lastBeiginTouch = CFAbsoluteTimeGetCurrent();
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(startMove:)]) {
        [self.delegate startMove:self];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_notSupportingSort) {
        return;
    }

    [super touchesEnded:touches withEvent:event];
    
    [self unLock];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(endMove:)]) {
        [self.delegate endMove:self];
    }
    
    if (self.isLongPress) {
        NSLog(@"isLongPress YES");
    } else {
        NSLog(@"isLongPress NO");
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickItem:)]) {
            [self.delegate clickItem:self.info];
        }
        if (![self isAllow]) {
//            if (self.delegate && [self.delegate respondsToSelector:@selector(clickChannelItem:)]) {
//                if (self.isMyChannel == YES) {
//                    [self.delegate clickChannelItem:self.info];
//                }
//                else{
//
//                }
//            }
        }
//        else{
//            if (self.delegate && [self.delegate respondsToSelector:@selector(clickChannelItem:)]){
//                if (self.isMyChannel == YES) {
//
//                }
//                else{
//
//                }
//            }
//        }
    }
    self.isLongPress = NO;
    [self enlarge:NO];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_notSupportingSort) {
        return;
    }

    [super touchesMoved:touches withEvent:event];
    
    if (![self isAllow]) {
        return;
    }
    
    UITouch *aTouch = [touches anyObject];
    CGPoint movingPt = [aTouch locationInView:self.superview];
    CGFloat dX = movingPt.x - self.beginCenter.x;
    CGFloat dY = movingPt.y - self.beginCenter.y;
    
    CGPoint newCenter = CGPointMake(self.beginCenter.x + dX, self.beginCenter.y + dY);
    self.center = newCenter;
    
    if (ABS(dX) > self.frame.size.width/2 || ABS(dY) > self.frame.size.height/2) {
        //需要排序了
//        NSLog(@"ABS:");
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didMove:)]) {
        [self.delegate didMove:self];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (![self isAllow]) {
        return;
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(activeLongPress:) object:nil];
 
    [super touchesCancelled:touches withEvent:event];
    

    
    if (self.delegate && [self.delegate respondsToSelector:@selector(endMove:)]) {
        [self.delegate endMove:self];
    }
    
    [self unLock];
    
    self.isLongPress = NO;
    [self enlarge:NO];
}

- (void)activeLongPress:(id)sender{
    if (_notSupportingSort) {
        return;
    }
    self.isLongPress = YES;
    [self enlarge:YES];
}

- (void)lock {
    if (self.delegate &&
        [self.delegate respondsToSelector:@selector(lockScrollView)]) {
        [self.delegate lockScrollView];
    }
}

- (void)unLock {
    if (self.delegate && [self.delegate respondsToSelector:@selector(unLockScrollView)]) {
        [self.delegate unLockScrollView];
    }
}

- (BOOL)isAllow{
    if (self.delegate && [self.delegate respondsToSelector:@selector(isAllowDrag)]) {
        return [self.delegate isAllowDrag];
    }
    return NO;
}

- (void)enlarge:(BOOL)b {
    if (!b) {
        self.transform = CGAffineTransformIdentity;
        self.isLongPress = NO;
        return;
    }
    
    self.isLongPress = YES;
    [UIView animateWithDuration:0.25 animations:^{
        if (b) {
            CGFloat f = 1.2;
            self.transform = CGAffineTransformMakeScale(f, f);
        }
        else {
            self.transform = CGAffineTransformIdentity;
        }
    } completion:^(BOOL finished) {
    }];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
