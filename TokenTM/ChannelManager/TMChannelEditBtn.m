//
//  TMChannelEditBtn.m
//  TokenTM
//
//  Created by wang shun on 2018/5/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMChannelEditBtn.h"

@interface TMChannelEditBtn ()

@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UIView* cornerRadiusView;

@property (nonatomic,strong) UIButton* selectedBtn;

@end

@implementation TMChannelEditBtn

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self createTitleLabel];
        [self createCornerRadiusView];
        [self createBtn];
    }
    return self;
}

- (void)createTitleLabel{
    self.titleLabel = [[UILabel alloc] init];
    [self.titleLabel setText:@"编辑"];
    [self.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [self.titleLabel setTextColor:RGBACOLOR(187, 160, 113, 1)];
    [self.titleLabel sizeToFit];
    
    CGFloat x = (self.bounds.size.width-self.titleLabel.bounds.size.width)/2.0;
    CGFloat y = (self.bounds.size.height-self.titleLabel.bounds.size.height)/2.0;
    [self.titleLabel setFrame:CGRectMake(x, y, self.titleLabel.bounds.size.width, self.titleLabel.bounds.size.height)];
    [self addSubview:self.titleLabel];
}

- (void)createCornerRadiusView{
    self.cornerRadiusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width-15 -25, self.bounds.size.height-5)];
    self.cornerRadiusView.center = self.titleLabel.center;
    self.cornerRadiusView.layer.cornerRadius = 10;
    self.cornerRadiusView.layer.borderColor = RGBACOLOR(187, 160, 113, 1).CGColor;
    self.cornerRadiusView.layer.borderWidth = 1;
    [self addSubview:self.cornerRadiusView];
}

- (void)createBtn{
    self.selectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.selectedBtn setFrame:self.bounds];
    [self addSubview:self.selectedBtn];
    if ([self.titleLabel.text isEqualToString:@"编辑"]) {
        self.selectedBtn.selected = NO;
    }
    
    [self.selectedBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)click:(UIButton*)b{
    b.selected = !b.selected;
    
    if (b.selected == YES) {
        [self.titleLabel setText:@"完成"];
    }
    else{
        [self.titleLabel setText:@"编辑"];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(editPress:)]) {
        [self.delegate editPress:b];
    }
}

- (BOOL)isEditStatus{
    return self.selectedBtn.selected;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
