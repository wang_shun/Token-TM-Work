//
//  TMCommentModel.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCommentModel.h"

@implementation TMCommentModel

+ (TMCommentModel *)commentModelFromDictionary:(NSDictionary *)dic{
    
    TMCommentModel *model = [[TMCommentModel alloc]init];
    
    model.action = dic[@"action"] ? [NSString stringWithFormat:@"%@",dic[@"action"]] : @"";
    model.commentId = dic[@"commentId"] ? [NSString stringWithFormat:@"%@",dic[@"commentId"]] : @"";
    model.content = dic[@"content"] ? [NSString stringWithFormat:@"%@",dic[@"content"]] : @"";
    model.ct = dic[@"ct"] ? [NSString stringWithFormat:@"%@",dic[@"ct"]] : @"";
    
    id isLike = [NSString stringWithFormat:@"%@",dic[@"isLike"]?:@""];
    model.isLike = ([isLike integerValue] == 1);
    model.likeCount = dic[@"likeCount"] ? [NSString stringWithFormat:@"%@",dic[@"likeCount"]] : @"";
    
    NSDictionary *commentUserInfo = [dic objectForKey:@"commentUserInfo"];
    if (commentUserInfo && [commentUserInfo isKindOfClass:NSDictionary.class]) {
        model.avatarUrl = commentUserInfo[@"avatarUrl"] ? [NSString stringWithFormat:@"%@",commentUserInfo[@"avatarUrl"]] : @"";
        model.nickName = commentUserInfo[@"nickName"] ? [NSString stringWithFormat:@"%@",commentUserInfo[@"nickName"]] : @"";
    }
    model.timeStr = dic[@"timeStr"] ? [NSString stringWithFormat:@"%@",dic[@"timeStr"]] : @"";

    if (model.content.length) {
        CGFloat contentHeight = ceil([model.content boundingRectWithSize:CGSizeMake(Screen_Width-60-30, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.height);
        model.cellHeight = contentHeight + 57 + 10;
    }else{
        model.cellHeight = 57 + 10;
    }

    return model;
}


@end
