//
//  TMBriefModel.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMCommentModel : NSObject

@property (nonatomic,strong) NSString *action;
@property (nonatomic,strong) NSString *commentId;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *ct;
@property (nonatomic,assign) BOOL isLike;
@property (nonatomic,strong) NSString *likeCount;
@property (nonatomic,strong) NSString *timeStr;
@property (nonatomic,strong) NSString *avatarUrl;
@property (nonatomic,strong) NSString *nickName;
@property (nonatomic,assign) CGFloat cellHeight;

+ (TMCommentModel *)commentModelFromDictionary:(NSDictionary *)dic;

@end

//{
//    "data": {
//        "list": [{
//            "action": 1,
//            "commentId": 136,
//            "commentUserInfo": {
//                "avatarUrl": "https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo9tPzj5NMfqWVssBVwPriazXCD4rdxMQuqniaNITF9Rwq7uDtq0Wuxsbew6V1QbQvAeRcjtNP31XGQ/132",
//                "gender": 1,
//                "nickName": "QZ",
//                "openId": "oT-Ok5Eh283dXEcnbkflXSBvdk2o",
//                "type": 0
//            },
//            "content": "今晚开始卡面",
//            "ct": 1531021297954,
//            "isLike": 0,
//            "likeCount": 0,
//            "replyCount": 0,
//            "replys": [],
//            "status": 1,
//            "timeStr": "12秒前"
//        }, {
//            "action": 1,
//            "commentId": 135,
//            "commentUserInfo": {
//                "avatarUrl": "https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo9tPzj5NMfqWVssBVwPriazXCD4rdxMQuqniaNITF9Rwq7uDtq0Wuxsbew6V1QbQvAeRcjtNP31XGQ/132",
//                "gender": 1,
//                "nickName": "QZ",
//                "openId": "oT-Ok5Eh283dXEcnbkflXSBvdk2o",
//                "type": 0
//            },
//            "content": "不是几十年",
//            "ct": 1531021283360,
//            "isLike": 0,
//            "likeCount": 0,
//            "replyCount": 0,
//            "replys": [],
//            "status": 1,
//            "timeStr": "26秒前"
//        }, {
//            "action": 1,
//            "commentId": 134,
//            "commentUserInfo": {
//                "avatarUrl": "https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo9tPzj5NMfqWVssBVwPriazXCD4rdxMQuqniaNITF9Rwq7uDtq0Wuxsbew6V1QbQvAeRcjtNP31XGQ/132",
//                "gender": 1,
//                "nickName": "QZ",
//                "openId": "oT-Ok5Eh283dXEcnbkflXSBvdk2o",
//                "type": 0
//            },
//            "content": "不上课市面上",
//            "ct": 1531021192075,
//            "isLike": 0,
//            "likeCount": 0,
//            "replyCount": 0,
//            "replys": [],
//            "status": 1,
//            "timeStr": "2分钟前"
//        }],
//        "commentCount": 3
//    },
//    "statusCode": 200
//}

