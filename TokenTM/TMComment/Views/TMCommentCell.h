//
//  TMCoinBriefInfoCell.h
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMCommentModel.h"

@interface TMCommentCell : UITableViewCell

@property(nonatomic,strong) TMCommentModel *mainModel;

@end
