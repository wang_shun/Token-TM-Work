//
//  TMCommentConetentView.h
//  TokenTM
//
//  Created by qz on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TMCommentHeaderViewDelegate <NSObject>

- (void)TMCommentHeaderViewTapToIndex:(NSInteger)index;

@end

@interface TMCommentHeaderView : UIView

@property(nonatomic,weak)id<TMCommentHeaderViewDelegate>delegate;

@property(nonatomic,assign) NSInteger commentCount;

-(void)tapIndex:(NSInteger)index;

@end
