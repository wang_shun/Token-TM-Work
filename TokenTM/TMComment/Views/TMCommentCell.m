//
//  TMCoinBriefInfoCell.m
//  TokenTM
//
//  Created by qz on 2018/6/17.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCommentCell.h"
#import "UIImageView+WebCache.h"

@interface TMCommentCell ()

@property (nonatomic,strong) UIImageView* headerImageView;
@property (nonatomic,strong) UILabel* nickLbl;
@property (nonatomic,strong) UILabel* timeLbl;
@property (nonatomic,strong) UILabel* likeLbl;
@property (nonatomic,strong) UILabel* commentLbl;

@property (nonatomic,strong) UIButton* likeBtn;


@end

@implementation TMCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews{
    
    [self addSubview:self.headerImageView];
    
    [self addSubview:self.nickLbl];
    [self addSubview:self.timeLbl];
    [self addSubview:self.likeLbl];
    [self addSubview:self.commentLbl];
}

-(void)setMainModel:(TMCommentModel *)mainModel{
    _mainModel = mainModel;

    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:mainModel.avatarUrl]
                            placeholderImage:[UIImage imageNamed:@"place_holder_72"]
                                   completed:nil];
    
    self.nickLbl.text = _mainModel.nickName;
    self.timeLbl.text = _mainModel.timeStr;
    if (_mainModel.likeCount.integerValue) {
        self.likeLbl.hidden = NO;
        self.likeLbl.text = _mainModel.likeCount;
        [self.likeLbl sizeToFit];
    }else{
        self.likeLbl.hidden = YES;
    }
    
    self.commentLbl.text = _mainModel.content;
    
    [self.nickLbl sizeToFit];
    [self.timeLbl sizeToFit];
    
    [self.commentLbl sizeToFit];
    
    [self refreshUIFrame];
}

-(void)refreshUIFrame{
    _nickLbl.frame = CGRectMake(60, 10, _nickLbl.frame.size.width, _nickLbl.frame.size.height);
    _timeLbl.frame = CGRectMake(60, CGRectGetMaxY(_nickLbl.frame)+1, _timeLbl.frame.size.width, _timeLbl.frame.size.height);
    _likeLbl.frame = CGRectMake(Screen_Width -30 - _likeLbl.frame.size.width, CGRectGetMinY(_nickLbl.frame), _likeLbl.frame.size.width, _likeLbl.frame.size.height);
    _commentLbl.frame = CGRectMake(60, CGRectGetMaxY(_timeLbl.frame)+15, _commentLbl.frame.size.width, _commentLbl.frame.size.height);
}

- (UIImageView *)headerImageView{
    if (!_headerImageView) {
        _headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 6, 36, 36)];
        _headerImageView.layer.masksToBounds = YES;
        _headerImageView.layer.cornerRadius = 18;
    }
    return _headerImageView;
}


-(UILabel *)nickLbl{
    if (!_nickLbl) {
        _nickLbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, Screen_Width, 20)];
        _nickLbl.textColor = GRAY(0x33);
        _nickLbl.font = [UIFont systemFontOfSize:12];
    }
    return _nickLbl;
}

-(UILabel *)timeLbl{
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, Screen_Width, 20)];
        _timeLbl.textColor = GRAY(0x66);
        _timeLbl.font = [UIFont systemFontOfSize:12];
    }
    return _timeLbl;
}

-(UILabel *)likeLbl{
    if (!_likeLbl) {
        _likeLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _likeLbl.textColor = Global_Brown_Color;
        _likeLbl.font = [UIFont systemFontOfSize:12];
    }
    return _likeLbl;
}

-(UILabel *)commentLbl{
    if (!_commentLbl) {
        _commentLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-24, 50)];
        _commentLbl.textColor = GRAY(0x99);
        _commentLbl.font = [UIFont systemFontOfSize:14];
        _commentLbl.numberOfLines = 0;
    }
    return _commentLbl;
}

-(UIButton *)likeBtn{
    if (!_likeBtn) {
        _likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _likeBtn.frame = CGRectMake(0, 0, Screen_Width-24, 50);
        [_likeBtn setImage:[UIImage imageNamed:@""] forState:0];
        [_likeBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateSelected];
    }
    return _likeBtn;
}


@end
