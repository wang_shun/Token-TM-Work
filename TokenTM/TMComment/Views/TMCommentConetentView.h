//
//  TMCommentConetentView.h
//  TokenTM
//
//  Created by qz on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TMCommentConetentViewDelegate <NSObject>
/**
 *
 *
 *
 *
 */
- (void)TMCommentConetentViewScrollToIndex:(NSInteger)index;

@end

@interface TMCommentConetentView : UIView


@property (nonatomic,strong) id<TMCommentConetentViewDelegate> delegate;
@property (nonatomic,strong) NSArray *commentsArray;
@property (nonatomic,strong) NSArray *hotCommentsArray;

-(void)selectIndex:(NSInteger)index;

@end
