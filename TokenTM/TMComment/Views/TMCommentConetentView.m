//
//  TMCommentConetentView.m
//  TokenTM
//
//  Created by qz on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCommentConetentView.h"
#import "TMCommentCell.h"
@interface TMCommentConetentView ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UIScrollView *mainScroll;
@property (nonatomic,strong) UITableView *leftTable;
@property (nonatomic,strong) UITableView *rightTable;
@property(nonatomic,assign) CGRect viewRect;
@property(nonatomic,assign) NSInteger currentIndex;


@end

@implementation TMCommentConetentView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    self.currentIndex = 1;
    [self addSubview:self.mainScroll];
    [self.mainScroll addSubview:self.leftTable];
    [self.mainScroll addSubview:self.rightTable];
}


-(void)setHotCommentsArray:(NSArray *)hotCommentsArray{
    _hotCommentsArray = hotCommentsArray;
    if (hotCommentsArray.count) {
        [self.rightTable reloadData];
    }
}

-(void)setCommentsArray:(NSArray *)commentsArray{
    _commentsArray = commentsArray;
    if (commentsArray.count) {
        [self.leftTable reloadData];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.delegate) {
        NSInteger currentIndex = 1;
        if (scrollView.contentOffset.x >= Screen_Width && scrollView.contentOffset.x < Screen_Width*2) {
            currentIndex = 2;
        }else if (scrollView.contentOffset.x < Screen_Width){
            currentIndex = 1;
        }
        if (currentIndex != self.currentIndex) {
            self.currentIndex = currentIndex;
            [self.delegate TMCommentConetentViewScrollToIndex:currentIndex];
        }
    }
}

-(void)selectIndex:(NSInteger)index{
    if (index == 1) {
        [self.mainScroll setContentOffset:CGPointMake(0, 0) animated:YES];
    }else{
        [self.mainScroll setContentOffset:CGPointMake(Screen_Width, 0) animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TMCommentModel *mainModel = nil;
    if (tableView == self.leftTable) {
        mainModel = [self.commentsArray objectAtIndex:indexPath.row];
    }else if (tableView == self.rightTable) {
        mainModel = [self.hotCommentsArray objectAtIndex:indexPath.row];
    }
    if (mainModel) {
        return mainModel.cellHeight;
    }
    return 53;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.leftTable) {
        return self.commentsArray.count;
    }else if (tableView == self.leftTable) {
        return self.hotCommentsArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TMCommentModel *mainModel = nil;
    if (tableView == self.leftTable) {
        mainModel = [self.commentsArray objectAtIndex:indexPath.row];
    }else if (tableView == self.rightTable) {
        mainModel = [self.hotCommentsArray objectAtIndex:indexPath.row];
    }
    NSString* cell_Identifier = @"TMCoinBriefInfo2Cell";
    TMCommentCell* cell = [tableView dequeueReusableCellWithIdentifier:cell_Identifier];
    if (cell == nil) {
        cell = [[TMCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if(mainModel){
        cell.mainModel = mainModel;
    }
    return cell;
}


-(UIScrollView *)mainScroll{
    if (!_mainScroll) {
        _mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, self.viewRect.size.height)];
        _mainScroll.delegate = self;
        _mainScroll.backgroundColor = RGBACOLOR(249, 245, 239, 1);
        _mainScroll.pagingEnabled = YES;
        _mainScroll.delegate = self;
    }
    return _mainScroll;
}

-(UITableView *)rightTable{
    if (!_rightTable) {
        _rightTable = [[UITableView alloc] initWithFrame:CGRectMake(Screen_Width, 0, Screen_Width, self.viewRect.size.height) style:UITableViewStylePlain];
        _rightTable.delegate = self;
        _rightTable.dataSource = self;
        _rightTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTable.estimatedRowHeight = 0;
        _rightTable.estimatedSectionHeaderHeight = 0;
        _rightTable.estimatedSectionFooterHeight = 0;
        _rightTable.backgroundColor = [UIColor clearColor];
    }
    return _rightTable;
}

-(UITableView *)leftTable{
    if (!_leftTable) {
        _leftTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, self.viewRect.size.height) style:UITableViewStylePlain];
        _leftTable.delegate = self;
        _leftTable.dataSource = self;
        _leftTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTable.estimatedRowHeight = 0;
        _leftTable.estimatedSectionHeaderHeight = 0;
        _leftTable.estimatedSectionFooterHeight = 0;
        _leftTable.backgroundColor = [UIColor clearColor];
    }
    return _leftTable;
}


@end
