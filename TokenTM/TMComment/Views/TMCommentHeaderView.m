//
//  TMCommentConetentView.m
//  TokenTM
//
//  Created by qz on 2018/7/8.
//  Copyright © 2018年 TTM. All rights reserved.
//

#import "TMCommentHeaderView.h"

@interface TMCommentHeaderView ()

@property (nonatomic,strong)UILabel *headerLbl;
@property (nonatomic,strong)UIView *headerLine;
@property (nonatomic,strong)UIButton *leftBtn;
@property (nonatomic,strong)UIButton *rightBtn;

@property(nonatomic,assign) CGRect viewRect;

@end

@implementation TMCommentHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.viewRect = frame;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    [self addSubview:self.headerLbl];
    [self addSubview:self.headerLine];
    [self addSubview:self.leftBtn];
    [self addSubview:self.rightBtn];
}

-(void)tapIndex:(NSInteger)index{
    if (index == 1) {
        [self leftBtnTapped:self.leftBtn];
    }else{
        [self rightBtnTapped:self.rightBtn];
    }
}

-(void)setCommentCount:(NSInteger)commentCount{
    _commentCount = commentCount;
    _headerLbl.text = [NSString stringWithFormat:@"评论(%lu)",commentCount];
    [_headerLbl sizeToFit];
    _headerLbl.frame = CGRectMake(13, 18, _headerLbl.frame.size.width, _headerLbl.frame.size.height);


}

-(void)leftBtnTapped:(UIButton *)sender{
    if (sender.selected == YES) {
        return;
    }
    sender.selected = YES;
    self.rightBtn.selected = NO;
    if (_delegate) {
        [_delegate TMCommentHeaderViewTapToIndex:1];
    }
}

-(void)rightBtnTapped:(UIButton *)sender{
    if (sender.selected == YES) {
        return;
    }
    sender.selected = YES;
    self.leftBtn.selected = NO;
    if (_delegate) {
        [_delegate TMCommentHeaderViewTapToIndex:2];
    }
}

- (UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.frame = CGRectMake(0, 45, Screen_Width/2, 40);
        
        _leftBtn.backgroundColor = [UIColor clearColor];
        [_leftBtn addTarget:self action:@selector(leftBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_leftBtn setTitle:@"最新评论" forState:0];
        [_leftBtn setTitle:@"最新评论" forState:UIControlStateSelected];
        [_leftBtn setTitleColor:GRAY(0x99) forState:0];
        [_leftBtn setTitleColor:Global_Brown_Color forState:UIControlStateSelected];
    }
    return _leftBtn;
}

-(UILabel *)headerLbl{
    if (!_headerLbl) {
        _headerLbl = [[UILabel alloc]initWithFrame:CGRectMake(13, 0, Screen_Width-24, 50)];
        _headerLbl.textColor = Global_Brown_Color;
        _headerLbl.font = [UIFont systemFontOfSize:14];
    }
    return _headerLbl;
}

- (UIButton *)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.frame = CGRectMake(Screen_Width/2, 45, Screen_Width/2, 40);
        _rightBtn.backgroundColor = [UIColor clearColor];
        [_rightBtn addTarget:self action:@selector(rightBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_rightBtn setTitle:@"热门评论" forState:0];
        [_rightBtn setTitle:@"热门评论" forState:UIControlStateSelected];
        [_rightBtn setTitleColor:GRAY(0x99) forState:0];
        [_rightBtn setTitleColor:Global_Brown_Color forState:UIControlStateSelected];
    }
    return _rightBtn;
}


- (UIView *)headerLine{
    if (!_headerLine) {
        _headerLine = [[UIView alloc] init];
        _headerLine.frame = CGRectMake(77, 24, Screen_Width-77-10, TMOnePixel);
        [_headerLine setBackgroundColor:Global_Brown_Color];
    }
    return _headerLine;
}


@end
